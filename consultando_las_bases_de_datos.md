# Consultando las Bases Datos

Pasos realizados para realizar consultas sobre las bases de datos.
- Requiere tener la infraestructura instalada (sólo el backend), también python instalado localmente (para la generación de consultas) y conocimientos de SQL.
- Tomamos como ejemplo el análisis de perfiles, pero estos pasos sirven para conocer la infraestructura, realizar todo tipo de consultas y aprender SQL en un nivel más avanzado.

## Levantar contenedor

Una vez instalada la infraestructura y las bases de datos, levantar el backend.
Abrir una terminal de consola (en git bash para Windows) y levantar el contenedor desde el menu:

```bash
cd lid-bases-de-datos
lid-bases-de-datos$ ./menu.sh
------------------------------------------------------------
Ingresar opción: 
1) Levantar  contenedor: docker-compose up
2) Detener   contenedor: docker-compose stop
3) Reiniciar contenedor: docker-compose up --build
4) Eliminar  contenedor: docker-compose down (OJO !, se eliminarán las bases de datos)
5) Importar bases de datos desde dump (es necesario que el contenedor esté levantado)
6) Borrar bases de datos que estén en dump (es necesario que el contenedor esté levantado)
7) Consultas SQL Elecciones
8) Consultas SQL Establecimientos
9) Consultas SQL Censo Provisorio
10) Consultas SQL Perfiles
0) Salir
------------------------------------------------------------
1
- xxx xxx xx xx:xx:xx -xx 2024 - Levantando contenedores (Ctrl+C para detener)
WARNING: Some networks were defined but are not used by any service: common_network
Starting web                ... done
Starting contenedor_pg      ... done
Starting contenedor_adminer ... done
Attaching to web, contenedor_adminer, contenedor_pg
contenedor_adminer | [xxx xxx xx xx:xx:xx 2024] PHP 7.4.30 Development Server (http://[::]:8080) started
contenedor_pg | 
contenedor_pg | PostgreSQL Database directory appears to contain a database; Skipping initialization
contenedor_pg | 
contenedor_pg | xxxx-xx-xx xx:xx:xx:x083 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
contenedor_pg | xxxx-xx-xx xx:xx:xx:x083 UTC [1] LOG:  listening on IPv6 address "::", port 5432
contenedor_pg | xxxx-xx-xx xx:xx:xx:x155 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
contenedor_pg | xxxx-xx-xx xx:xx:xx:x466 UTC [26] LOG:  database system was shut down at ...
contenedor_pg | xxxx-xx-xx xx:xx:xx:x925 UTC [1] LOG:  database system is ready to accept connections
web        | INFO:     Will watch for changes in these directories: ['/code']
web        | INFO:     Uvicorn running on http://0.0.0.0:8001 (Press CTRL+C to quit)
web        | INFO:     Started reloader process [1] using WatchFiles
web        | INFO:     Started server process [8]
web        | INFO:     Waiting for application startup.
web        | INFO:     Application startup complete.
...
```

Una vez levantado el contenedor, verificar acceso a las base de datos desde el navegador.
- Ingresando a http://localhost:8080 se mostrará una pantalla de un cliente de base de datos (Adminer)
- **Seleccionar base de datos:** PostGreSQL
- **Usuario:** postgres **Contraseña:** lid

Se deberían mostrar las bases de datos disponibles, para poder seleccionar y realizar consultas SQL.

## Generar consultas SQL a las bases de datos

Para este paso es necesario tener instalado python localmente.
Abrir otra terminal y volver a ejecutar el menu:

```bash
lid-bases-de-datos$ ./menu.sh
------------------------------------------------------------
Ingresar opción: 
1) Levantar  contenedor: docker-compose up
2) Detener   contenedor: docker-compose stop
3) Reiniciar contenedor: docker-compose up --build
4) Eliminar  contenedor: docker-compose down (OJO !, se eliminarán las bases de datos)
5) Importar bases de datos desde dump (es necesario que el contenedor esté levantado)
6) Borrar bases de datos que estén en dump (es necesario que el contenedor esté levantado)
7) Consultas SQL Elecciones
8) Consultas SQL Establecimientos
9) Consultas SQL Censo Provisorio
10) Consultas SQL Perfiles
0) Salir
```
Las opciones Consultas SQL permiten generar ejemplos de las principales consultas en SQL sobre cada una de las bases de datos. Por ejemplo elegimos opción 10:

```bash
10
1) PerfilesSeccionesPerfiles
2) PerfilesSeccionesSecciones
3) PerfilesSeccionesDistritos
4) PerfilesSeccionesCategorias
5) PerfilesSeccionesElecciones
6) PerfilesSecciones
11) PerfilesCircuitosPerfiles
12) PerfilesCircuitosCircuitos
13) PerfilesCircuitosSecciones
14) PerfilesCircuitosDistritos
15) PerfilesCircuitosCategorias
16) PerfilesCircuitosElecciones
17) PerfilesCircuitos
0) Salir
Ingresar opción: 6

SELECT s.cod_eleccion
     , e.nom_eleccion
     , s.cod_categoria
     , c.nom_categoria
     , s.cod_distrito
     , d.nom_distrito
     , s.cod_seccion
     , t.nom_seccion
     , s.perfil
FROM perfiles_secciones s, elecciones e, categorias c, distritos d, secciones t
WHERE s.cod_eleccion = e.cod_eleccion
AND s.cod_categoria = c.cod_categoria
AND s.cod_distrito = d.cod_distrito
AND s.cod_distrito = t.cod_distrito
AND s.cod_seccion = t.cod_seccion
AND s.cod_eleccion = 'e2023_generales'
AND s.cod_categoria = '01'
AND s.cod_distrito = '02'
ORDER BY s.cod_eleccion,s.cod_categoria,s.cod_distrito,s.cod_seccion,s.perfil
```
Esto muestra una salida de las principales consultas a las bases de datos, usando el mismo modelo que usa la infraestructura, que publica los resultados de las consultas SQL en una API, usando python. Se puede ver en el menu.sh como se realiza esto mediante:
```bash
python3 ws_python/modelo/consultas_perfiles.py
```
Esto sirve para que las y los desarrolladores puedan realizar pruebas, en caso de necesario ajustar esta línea de acuerdo a la forma de invocar python que se tenga.

## Ejemplos

Tomamos como ejemplo, algunas consultas generadas de esta manera

## Consulta de Perfiles por Sección
Generamos la consulta de Perfiles por Sección para las Elecciones Generales 2023, categoría Presidente, donde comentamos (o borramos) columnas que no nos interesan para obtener un listado de secciones por perfil.
```sql
SELECT -- s.cod_eleccion
     --, e.nom_eleccion
     --, s.cod_categoria
     --, c.nom_categoria
       s.cod_distrito
     --, d.nom_distrito
     , s.cod_seccion
     --, t.nom_seccion
     , s.perfil
FROM perfiles_secciones s, elecciones e, categorias c, distritos d, secciones t
WHERE s.cod_eleccion = e.cod_eleccion
AND s.cod_categoria = c.cod_categoria
AND s.cod_distrito = d.cod_distrito
AND s.cod_distrito = t.cod_distrito
AND s.cod_seccion = t.cod_seccion
AND s.cod_eleccion = 'e2023_generales'
AND s.cod_categoria = '01'
ORDER BY s.cod_eleccion,s.cod_categoria,s.cod_distrito,s.cod_seccion,s.perfil
```
Luego vamos al cliente de base de datos, seleccionamos la base de datos correspondiente (consultas), seleccionamos la opción Comando SQL, pegamos la consulta y ejecutamos.

## Consulta de Secciones para un perfil

Obtenemos el listados de secciones(cod_distrito, cod_seccion) para un perfil determinado, en este caso el perfil "00"

```sql
SELECT s.cod_distrito
     , s.cod_seccion
FROM perfiles_secciones s, elecciones e, categorias c, distritos d, secciones t
WHERE s.cod_eleccion = e.cod_eleccion
AND s.cod_categoria = c.cod_categoria
AND s.cod_distrito = d.cod_distrito
AND s.cod_distrito = t.cod_distrito
AND s.cod_seccion = t.cod_seccion
AND s.cod_eleccion = 'e2023_generales'
AND s.cod_categoria = '01'
AND s.perfil = '00'
ORDER BY s.cod_eleccion,s.cod_categoria,s.cod_distrito,s.cod_seccion,s.perfil
```

## Consulta de Resultados por Sección

```bash
1) Levantar  contenedor: docker-compose up
2) Detener   contenedor: docker-compose stop
3) Reiniciar contenedor: docker-compose up --build
4) Eliminar  contenedor: docker-compose down (OJO !, se eliminarán las bases de datos)
5) Importar bases de datos desde dump (es necesario que el contenedor esté levantado)
6) Borrar bases de datos que estén en dump (es necesario que el contenedor esté levantado)
7) Consultas SQL Elecciones
8) Consultas SQL Establecimientos
9) Consultas SQL Censo Provisorio
10) Consultas SQL Perfiles
0) Salir
------------------------------------------------------------
7
1) categorias
2) distritos
3) secciones
4) secciones proviniciales
5) circuitos
6) mesas
7) agrupaciones
8) agrupaciones sublistas
11) resultados totales
12) resultados totales sublistas
13) resultados totales mesas
14) resultados distritos
15) resultados distritos sublistas
16) resultados distritos mesas
17) resultados secciones
18) resultados secciones sublistas
19) resultados secciones mesas
20) resultados circuitos
21) resultados circuitos sublistas
22) resultados circuitos mesas
23) resultados mesas
24) resultados mesas sublistas
25) resultados mesas mesa
26) resultados distritos x Col
27) resultados secciones x Col
28) resultados circuitos x Col
0) Salir
Ingresar opción: 17
```
```sql
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/NULLIF(mv.votos_positivos,0)*100,2) porcentaje
FROM votos_secciones v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,secciones s,mesas_secciones mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito  = d.cod_distrito
AND v.cod_distrito  = s.cod_distrito
AND v.cod_seccion   = s.cod_seccion
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_distrito  = mv.cod_distrito
AND v.cod_seccion   = mv.cod_seccion
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND v.cod_categoria = '03'
AND v.cod_distrito = '01'
AND v.cod_seccion = '001'
GROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,v.cod_seccion,s.nom_seccion,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,nom_agrupacion,mv.votos_positivos
ORDER BY v.cod_categoria,v.cod_distrito,v.cod_seccion,orden,votos DESC
```
Luego vamos al cliente de base de datos, seleccionamos la base de datos correspondiente (e2023_generales), seleccionamos la opción Comando SQL, pegamos la consulta y ejecutamos.

## Consulta de Resultados por Sección para una lista de secciones

Modificamos la columna anterior para que nos muestre los resultados de un determinado conjunto de secciones.

```sql
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/NULLIF(mv.votos_positivos,0)*100,2) porcentaje
FROM votos_secciones v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,secciones s,mesas_secciones mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito  = d.cod_distrito
AND v.cod_distrito  = s.cod_distrito
AND v.cod_seccion   = s.cod_seccion
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_distrito  = mv.cod_distrito
AND v.cod_seccion   = mv.cod_seccion
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND v.cod_categoria = '01'
AND (v.cod_distrito, v.cod_seccion) IN (('01', '001'), ('01', '002'), ('02', '003'))
GROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,v.cod_seccion,s.nom_seccion,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,nom_agrupacion,mv.votos_positivos
ORDER BY v.cod_categoria,v.cod_distrito,v.cod_seccion,orden,votos DESC
```

## Consulta de Resultados por Sección para una lista de secciones, agrupando el resultado total

Ajustamos la consulta anterior, para que, en lugar de listar cada sección, me agrupe el resultado total para es conjunto de las secciones.

```sql
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/NULLIF(SUM(mv.votos_positivos),0)*100,2) porcentaje
FROM votos_secciones v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,secciones s,mesas_secciones mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito  = d.cod_distrito
AND v.cod_distrito  = s.cod_distrito
AND v.cod_seccion   = s.cod_seccion
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_distrito  = mv.cod_distrito
AND v.cod_seccion   = mv.cod_seccion
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND v.cod_categoria = '01'
AND (v.cod_distrito, v.cod_seccion) IN (('01', '001'), ('01', '002'), ('02', '003'))
GROUP BY v.cod_categoria,c.nom_categoria,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,nom_agrupacion
ORDER BY v.cod_categoria,orden,votos DESC
```

## Consulta de Secciones para un perfil formateado

En la base de datos de perfiles, hago la consulta de la secciones para un perfil (en este caso el "00"), formateando la salida para que me sirva para la otra consulta.

```sql
SELECT ',(''' || s.cod_distrito || ''',''' || s.cod_seccion || ''')'
FROM perfiles_secciones s, elecciones e, categorias c, distritos d, secciones t
WHERE s.cod_eleccion = e.cod_eleccion
AND s.cod_categoria = c.cod_categoria
AND s.cod_distrito = d.cod_distrito
AND s.cod_distrito = t.cod_distrito
AND s.cod_seccion = t.cod_seccion
AND s.cod_eleccion = 'e2023_generales'
AND s.cod_categoria = '01'
AND s.perfil = '00'
ORDER BY s.cod_eleccion,s.cod_categoria,s.cod_distrito,s.cod_seccion,s.perfil
```

## Consulta de Resultados por un Perfil

```sql
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/NULLIF(SUM(mv.votos_positivos),0)*100,2) porcentaje
FROM votos_secciones v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,secciones s,mesas_secciones mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito  = d.cod_distrito
AND v.cod_distrito  = s.cod_distrito
AND v.cod_seccion   = s.cod_seccion
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_distrito  = mv.cod_distrito
AND v.cod_seccion   = mv.cod_seccion
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND v.cod_categoria = '01'
AND (v.cod_distrito, v.cod_seccion) IN (
 ('07','011')
,('12','001')
,('12','004')
,('12','006')
,('13','005')
,('14','002')
,('14','004')
,('14','007')
,('14','008')
,('14','009')
,('14','010')
,('14','012')
,('14','013')
,('14','014')
,('14','016')
,('14','017')
,('15','003')
,('15','004')
,('17','003')
,('17','007')
,('17','010')
,('17','011')
,('17','012')
,('17','013')
,('17','014')
,('17','019')
,('17','020')
,('17','021')
,('17','023')
,('18','010')
,('18','012')
,('19','009')
,('20','005')
,('22','007')
,('23','002')
,('23','016')
,('24','005'))
GROUP BY v.cod_categoria,c.nom_categoria,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,nom_agrupacion
ORDER BY v.cod_categoria,orden,votos DESC
```
