# Backend para consultas a bases de datos

- Permite levantar contenedores en docker con **Bases de Datos** electorales y otras localmente. 
- Publica una **API** en Pyhton para poder realizar consultas sobre las mismas. La documentación de la API puede consultarse con los contenedores levantados en http://localhost:9000/redoc

**Requiere tener docker instalado**: https://docs.docker.com/

## Qué hace
- Levanta un adminer en http://localhost:8080 para hacer consultas en SQL a las bases de datos desde un navegador
- Permite realizar una conexión desde un cliente de base de datos PostGreSQL en el puerto 2032
- Permite realizar consultas a la base mediante una API en Pyhton, ver documentación http://localhost:9000/redoc

## Instalando el proyecto y las bases de datos
1. Descargar el proyecto del repositorio de git. Trabajaremos sobre la rama develop: https://gitlab.com/lid.datos/lid-bases-de-datos/-/tree/develop
2. En el proyecto, al mismo nivel que el directorio ws_python, crear un directorio dump
3. Descargar las bases de datos con las que se quiere trabajar. Se pueden descargar de este drive https://drive.google.com/drive/folders/1K6GtqZQjnE1U0DxkjGqrhBak48hjXMBJ de manera que los archivos sql queden dentro del directorio dump descomprimidos (con extensión .sql)
4. Cada archivos .sql es un dump de una base de datos PostGreSQL
5. Las bases que empiezan con e son las bases electorales, están normalizadas y tienen todas la misma estructura
6. Para conocer las bases de datos electorales con SQL tenemos esta guía https://docs.google.com/document/d/1W_zmxMQK1D_BnoMVnCnnVOuR6TAI-8KugpFe8YaJBaE

## Ejemplos de consultas a la API

### Elecciones
- http://localhost:9000/api/elecciones
- http://localhost:9000/api/categorias?eleccion=e2021_generales
- http://localhost:9000/api/distritos?eleccion=e2021_generales
- http://localhost:9000/api/secciones?eleccion=e2021_generales&distrito=01
- http://localhost:9000/api/circuitos?eleccion=e2021_generales&distrito=01&seccion=001
- http://localhost:9000/api/mesas?eleccion=e2021_generales&distrito=01&seccion=001&circuito=00001
- http://localhost:9000/api/agrupaciones?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001&circuito=00001

#### Total país (por ahora sólo para presidente categoría 01)
- http://localhost:9000/api/resultadosTotales?eleccion=e2019_generales&categoria=01
- http://localhost:9000/api/resultadosTotalesSublistas?eleccion=e2019_paso&categoria=01
- http://localhost:9000/api/resultadosTotalesMesas?eleccion=e2019_generales&categoria=01

#### Por distrito
- http://localhost:9000/api/resultadosDistritos?eleccion=e2021_generales&categoria=03&distrito=01
- http://localhost:9000/api/resultadosDistritosSublistas?eleccion=e2021_paso&categoria=03&distrito=01
- http://localhost:9000/api/resultadosDistritosMesas?eleccion=e2021_generales&categoria=03&distrito=01
#### Resultados diputados para CABA de todas las elecciones:
- http://localhost:9000/api/resultadosDistritos?categoria=03&distrito=01

#### Resultados CABA Elecciones Generales 2021 todas las categorías:
- http://localhost:9000/api/resultadosDistritos?eleccion=e2021_generales&distrito=01


#### Por sección
- http://localhost:9000/api/resultadosSecciones?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001
- http://localhost:9000/api/resultadosSeccionesSublistas?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001
- http://localhost:9000/api/resultadosSeccionesMesas?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001

#### Por circuito
- http://localhost:9000/api/resultadosCircuitos?eleccion=e2021_generales&distrito=01&categoria=03&seccion=001&circuito=00001
- http://localhost:9000/api/resultadosCircuitosSublistas?eleccion=e2021_generales&distrito=01&categoria=03&seccion=001&circuito=00001
- http://localhost:9000/api/resultadosCircuitosMesas?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001&circuito=00001

#### Resultados x Col
- http://localhost:9000/api/resultadosDistritosxCol?eleccion=e2021_generales&categoria=03&distrito=01
- http://localhost:9000/api/resultadosSeccionesxCol?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001
- http://localhost:9000/api/resultadosCircuitosxCol?eleccion=e2021_generales&distrito=01&categoria=03&seccion=001

#### Por mesa
- http://localhost:9000/api/resultadosMesas?eleccion=e2021_generales&distrito=01&seccion=001&seccion=001&circuito=00001&mesa=00001X
- http://localhost:9000/api/resultadosMesasSublistas?eleccion=e2021_generales&distrito=01&seccion=001&seccion=001&circuito=00001&mesa=00001X
- http://localhost:9000/api/resultadosMesasMesa?eleccion=e2021_generales&distrito=01&seccion=001&seccion=001&circuito=00001&categoria=03&mesa=00001X

### Consultas
#### Perfiles x Seccion
- http://localhost:9000/api/perfiles/eleccionesSeccionesPerfiles?eleccion=e2021_generales
- http://localhost:9000/api/perfiles/eleccionesSeccionesPerfiles?distrito=01&categoria=03&seccion=001
- http://localhost:9000/api/perfiles/algoritmosSeccionesPerfiles?eleccion=e2021_generales
- http://localhost:9000/api/perfiles/algoritmosSeccionesPerfiles?distrito=01&categoria=03&seccion=001
- http://localhost:9000/api/perfiles/categoriasSeccionesPerfiles?eleccion=e2021_generales
- http://localhost:9000/api/perfiles/categoriasSeccionesPerfiles?distrito=01&categoria=03&seccion=001
- http://localhost:9000/api/perfiles/perfilesSeccionesPerfiles?eleccion=e2021_generales
- http://localhost:9000/api/perfiles/perfilesSeccionesPerfiles?distrito=01&categoria=01&seccion=001
- http://localhost:9000/api/perfiles/distritosSeccionesPerfiles?eleccion=e2021_generales
- http://localhost:9000/api/perfiles/distritosSeccionesPerfiles?distrito=01&categoria=03
- http://localhost:9000/api/perfiles/seccionesSeccionesPerfiles?eleccion=e2021_generales
- http://localhost:9000/api/perfiles/seccionesSeccionesPerfiles?distrito=01&categoria=03&seccion=001
- http://localhost:9000/api/perfiles/perfilesSecciones?eleccion=e2021_generales
- http://localhost:9000/api/perfiles/perfilesSecciones?distrito=01&categoria=03&seccion=001
#### Perfiles x Circuito
- http://localhost:9000/api/perfiles/eleccionesCircuitosPerfiles?eleccion=e2021_generales
- http://localhost:9000/api/perfiles/eleccionesCircuitosPerfiles?distrito=01&categoria=03&seccion=001&circuito=00001
- http://localhost:9000/api/perfiles/algoritmosCircuitosPerfiles?eleccion=e2021_generales
- http://localhost:9000/api/perfiles/algoritmosCircuitosPerfiles?distrito=01&categoria=03&seccion=001&circuito=00001
- http://localhost:9000/api/perfiles/categoriasCircuitosPerfiles?eleccion=e2021_generales
- http://localhost:9000/api/perfiles/categoriasCircuitosPerfiles?distrito=01&seccion=001
- http://localhost:9000/api/perfiles/categoriasCircuitosPerfiles?distrito=01&categoria=03&seccion=001&circuito=00001
- http://localhost:9000/api/perfiles/perfilesCircuitosPerfiles?distrito=01&seccion=001
- http://localhost:9000/api/perfiles/perfilesCircuitosPerfiles?distrito=01&categoria=01&seccion=001&circuito=00001
- http://localhost:9000/api/perfiles/distritosCircuitosPerfiles?distrito=01
- http://localhost:9000/api/perfiles/distritosCircuitosPerfiles?categoria=03
- http://localhost:9000/api/perfiles/seccionesCircuitosPerfiles?distrito=01&seccion=001
- http://localhost:9000/api/perfiles/seccionesCircuitosPerfiles?distrito=01&categoria=03
- http://localhost:9000/api/perfiles/circuitosCircuitosPerfiles?distrito=01&seccion=001
- http://localhost:9000/api/perfiles/circuitosCircuitosPerfiles?distrito=01&categoria=03&circuito=00001
- http://localhost:9000/api/perfiles/perfilesCircuitos?distrito=01&seccion=001
- http://localhost:9000/api/perfiles/perfilesCircuitos?distrito=01&categoria=03&circuito=00001

#### Clasificación y Perfil
- http://localhost:9000/api/perfiles/clasificacionPais?eleccion=e2023_generales&categoria=01
- http://localhost:9000/api/perfiles/clasificacionDistrito?eleccion=e2023_generales&distrito=02&categoria=03&algoritmo=km03
- http://localhost:9000/api/perfiles/perfilPais?eleccion=e2023_generales&categoria=01&algoritmo=km03
- http://localhost:9000/api/perfiles/perfilDistrito?eleccion=e2023_generales&distrito=02&categoria=03&algoritmo=km03
- http://localhost:9000/api/perfiles/clasificacionPerfilPais?eleccion=e2023_generales&categoria=01&algoritmo=km03
- http://localhost:9000/api/perfiles/clasificacionPerfilDistrito?eleccion=e2023_generales&distrito=02&categoria=03&algoritmo=km03

### Establecimientos
- http://localhost:9000/api/clae2?base=d2022_establecimientos
- http://localhost:9000/api/clae6?base=d2022_establecimientos
- http://localhost:9000/api/claeLetra?base=d2022_establecimientos
- http://localhost:9000/api/rangos?base=d2022_establecimientos
- http://localhost:9000/api/provincias?base=d2022_establecimientos
- http://localhost:9000/api/departamentos?base=d2022_establecimientos&provincia=2
- http://localhost:9000/api/establecimientos?base=d2022_establecimientos&departamento=10007
- http://localhost:9000/api/establecimientos?base=d2022_establecimientos&departamento=10007&letra=A

### Censo
- http://localhost:9000/api/censop_provincias?base=d2022_censo_provisorios
- http://localhost:9000/api/censop_departamentos?base=d2022_censo_provisorios&provincia=2
- http://localhost:9000/api/censop_censo?base=d2022_censo_provisorios&provincia=2
- http://localhost:9000/api/censop_censo?base=d2022_censo_provisorios&provincia=2&departamento=2015

---

### Capas del backend

![Gráfico de capas del backend](grafico_capas_backend.jpg "Gráfico de capas del backend")

---

## Componentes del proyecto
- Los archivos [docker-compose.yml](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/docker-compose.yml) y 
[ws_python/Dockerfile](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/ws_python/Dockerfile) contienen las características de la infraestructura, los contenedores y puertos que utilizan.
- En directorio [ws_python](https://gitlab.com/lid.datos/lid-bases-de-datos/-/tree/develop/ws_python) se encuentra el código fuente en Python y sus [requirimientos](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/ws_python/requirements.txt)
- La documentación de la API para poder ser accedida desde un frontend se puede consultar con los contenedores levantados en http://localhost:9000/redoc y está implementada en [main.py](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/ws_python/main.py)
- Las consultas a las bases de datos se manejarán desde [controlador_elecciones.py](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/ws_python/controlador_elecciones.py) y **controlador_establecimientos.py** (pendiente de desarrollo). Para eso se utilizaran las clases correspondientes a las consultas. De manera que las mismas se puedan manipular sencillamente con objetos (sin necesidad de saber SQL).
- Los archivos [elecciones.py](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/ws_python/elecciones.py) y [establecimientos.py](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/ws_python/establecimientos.py) generan consultas SQL para realizar sobre las bases de datos. Las consutas de elecciones son las mismas para todas las bases de datos porque tienen la misma estructura. 
- Una documentación sobre las clases de acceso a las bases de datos, sus contructores y métodos puede consultar en [elecciones.md](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/ws_python/elecciones.md) y [establecimientos.md](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/ws_python/establecimientos.md)
- Los archivos [consultas_elecciones.py](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/ws_python/consultas_elecciones.py) y [consultas_establecimientos.py](https://gitlab.com/lid.datos/lid-bases-de-datos/-/blob/develop/ws_python/consultas_establecimientos.py) son sólo para probar las clases de los archivos respectivos y ver que consultas devuelven.
- Generar: [Documentación la generación de datos](ws_python/generar/README.md)
 
## Levantando los contenedores

Tiene un menu en bash (linux, quizás powershell de windows) que se ejecuta desde el directorio del proyecto con ./menu.sh para facilitar el uso de los contenedores. Con las siguientes opciones:

   ### 1) Crear contenedor PostGreSQL ###
      
   Crea el contenedor con la base de datos PostGreSQL 10.21, desde el *archivo docker-compose.yml* Por defecto el contenedor queda corriendo, con *Ctrl + C* se detiene la ejecución. Sólo es necesario realizarlo una vez.

   ### 2) Importar bases de datos ### 

   Importa las bases de datos desde el directorio dump (es necesario que el contenedor esté corriendo). Tiempo aproximado 15 min, para las 6 bases de datos electorales. 
   
   Una vez finalizada la importación ya se tendrán diponibles las bases de datos y el adminer en: http://localhost:8080/
   - **Seleccionar base de datos:** PostGreSQL
   - **Usuario:** postgres **Contraseña:** lid

   Importante: Sólo es necesario correrlo una vez. Para que funcione antes de importar es necesario que el contenedor estén iniciado previamente (opción 1). Luego bastará con iniciar(opción 3) y detener (opción 4) el contenedor cuando se quiera realizar consultas.

   También podrá conectarse un cliente de base de datos en localhost puerto 2032

   ### 3) Levantar contenedor ### 

   Levanta el contenedor con la base de datos PostGreSQL (necesario para poder importar las bases de datos).

   ### 4) Detener contenedor ###

   Detiene los contenedores. Cuando se termine de realizar consultas se puede detener el contenedor con las bases de datos, sin que se pierdan los datos. Para volverla a arrancar seleccionar **3) Iniciar contendor** nuevamente.

   ### 5) Eliminar contenedor ### 

   Borra los contenedores de la máquina. Para cuando no se quieran usar más los contenedores ni la base de daros se podrán borrar los contendores y sus datos asociados.

**Referencias**:
- Partimos de la imagen del sitio oficial: https://hub.docker.com/_/postgres
- Para importar tomamos esta forma de hacerlo: https://stackoverflow.com/questions/24718706/backup-restore-a-dockerized-postgresql-database
- Los datos de los resultados electorales corresponden a los datasets oficiales publicados oportunamente y descargados del sitio de Andy Tow https://www.andytow.com/


### Comandos útiles en docker

docker run -it lid-bases-de-datos_web /bin/bash
docker ps
docker stop lid-bases-de-datos_web_1
docker start lid-bases-de-datos_web_1
docker stop lid-bases-de-datos_web_1

Modificar el/los archivos .py

docker rm lid-bases-de-datos_web_1
docker build -t python .

docker images
docker run -p 9000:8001 python -it
docker run -it python /bin/bash

docker ps -a 
docker logs lid-bases-de-datos_web_1

### Licencia

Toda la infraestructura, así como las bases de datos están licenciados bajo la GNU General Public License, versión 3.
Para más detalles, consulte el archivo LICENCIA.txt presentes en los repositorios de fuentes y en el drive público con las bases de datos.
