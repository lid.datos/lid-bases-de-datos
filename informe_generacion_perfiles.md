# Informe de generación de perfiles
## CircuitosXPais(codEleccion=e2015_paso,codCategoria=01)
    km03 - inercia: 3682246.246 - silhouette: 0.292
    km05 - inercia: 2552245.916 - silhouette: 0.351
    km10 - inercia: 1548428.791 - silhouette: 0.303
    km15 - inercia: 1175965.894 - silhouette: 0.275
    km20 - inercia: 965168.642 - silhouette: 0.262
    km25 - inercia: 837152.673 - silhouette: 0.252
## SeccionesXPais(codEleccion=e2015_paso,codCategoria=01)
    km03 - inercia: 141020.525 - silhouette: 0.300
    km05 - inercia: 90914.873 - silhouette: 0.351
    km10 - inercia: 47401.971 - silhouette: 0.294
    km15 - inercia: 33571.925 - silhouette: 0.280
    km20 - inercia: 27135.641 - silhouette: 0.266
    km25 - inercia: 22315.989 - silhouette: 0.261
## CircuitosXDistrito(codEleccion=e2015_paso,codCategoria=03)
### 01
    km03 - inercia: 8112.248 - silhouette: 0.418
    km05 - inercia: 3662.753 - silhouette: 0.401
    km10 - inercia: 1556.334 - silhouette: 0.335
    km15 - inercia: 1007.666 - silhouette: 0.327
    km20 - inercia: 755.753 - silhouette: 0.308
    km25 - inercia: 631.019 - silhouette: 0.304
### 02
    km03 - inercia: 401469.185 - silhouette: 0.261
    km05 - inercia: 301979.074 - silhouette: 0.233
    km10 - inercia: 198746.347 - silhouette: 0.233
    km15 - inercia: 156304.063 - silhouette: 0.232
    km20 - inercia: 132852.539 - silhouette: 0.195
    km25 - inercia: 113688.417 - silhouette: 0.216
### 03
    km03 - inercia: 63594.823 - silhouette: 0.345
    km05 - inercia: 39160.979 - silhouette: 0.331
    km10 - inercia: 17197.122 - silhouette: 0.357
    km15 - inercia: 10035.271 - silhouette: 0.360
    km20 - inercia: 7238.375 - silhouette: 0.317
    km25 - inercia: 5502.158 - silhouette: 0.329
### 04
    km03 - inercia: 411806.176 - silhouette: 0.507
    km05 - inercia: 270401.623 - silhouette: 0.297
    km10 - inercia: 155113.536 - silhouette: 0.283
    km15 - inercia: 106799.158 - silhouette: 0.266
    km20 - inercia: 75872.888 - silhouette: 0.263
    km25 - inercia: 60942.380 - silhouette: 0.273
### 05
    km03 - inercia: 29654.172 - silhouette: 0.364
    km05 - inercia: 19585.332 - silhouette: 0.364
    km10 - inercia: 9642.278 - silhouette: 0.309
    km15 - inercia: 6380.506 - silhouette: 0.290
    km20 - inercia: 4397.813 - silhouette: 0.325
    km25 - inercia: 3609.787 - silhouette: 0.297
### 06
    km03 - inercia: 60143.389 - silhouette: 0.388
    km05 - inercia: 35317.922 - silhouette: 0.342
    km10 - inercia: 13151.928 - silhouette: 0.342
    km15 - inercia: 6883.651 - silhouette: 0.312
    km20 - inercia: 5064.426 - silhouette: 0.284
    km25 - inercia: 3976.345 - silhouette: 0.258
### 07
    km03 - inercia: 57479.097 - silhouette: 0.293
    km05 - inercia: 36880.436 - silhouette: 0.276
    km10 - inercia: 16181.618 - silhouette: 0.277
    km15 - inercia: 10487.588 - silhouette: 0.287
    km20 - inercia: 7132.791 - silhouette: 0.299
    km25 - inercia: 5396.817 - silhouette: 0.287
### 08
    km03 - inercia: 267471.927 - silhouette: 0.326
    km05 - inercia: 197903.008 - silhouette: 0.270
    km10 - inercia: 113986.049 - silhouette: 0.268
    km15 - inercia: 79562.713 - silhouette: 0.258
    km20 - inercia: 63867.306 - silhouette: 0.247
    km25 - inercia: 51667.805 - silhouette: 0.261
### 09
    km03 - inercia: 125891.374 - silhouette: 0.515
    km05 - inercia: 61748.344 - silhouette: 0.398
    km10 - inercia: 18268.420 - silhouette: 0.393
    km15 - inercia: 8106.828 - silhouette: 0.354
    km20 - inercia: 4621.655 - silhouette: 0.360
    km25 - inercia: 3291.578 - silhouette: 0.326
### 10
    km03 - inercia: 20362.637 - silhouette: 0.393
    km05 - inercia: 12883.225 - silhouette: 0.357
    km10 - inercia: 6254.373 - silhouette: 0.333
    km15 - inercia: 4002.059 - silhouette: 0.323
    km20 - inercia: 2898.191 - silhouette: 0.288
    km25 - inercia: 2234.334 - silhouette: 0.289
### 11
    km03 - inercia: 11622.606 - silhouette: 0.441
    km05 - inercia: 7885.629 - silhouette: 0.281
    km10 - inercia: 4641.881 - silhouette: 0.286
    km15 - inercia: 3061.821 - silhouette: 0.266
    km20 - inercia: 2124.824 - silhouette: 0.276
    km25 - inercia: 1520.818 - silhouette: 0.287
### 12
    km03 - inercia: 60624.364 - silhouette: 0.422
    km05 - inercia: 33196.363 - silhouette: 0.364
    km10 - inercia: 16093.341 - silhouette: 0.331
    km15 - inercia: 10348.222 - silhouette: 0.322
    km20 - inercia: 7322.955 - silhouette: 0.325
    km25 - inercia: 5247.894 - silhouette: 0.306
### 13
    km03 - inercia: 26431.499 - silhouette: 0.429
    km05 - inercia: 17795.548 - silhouette: 0.323
    km10 - inercia: 10199.570 - silhouette: 0.325
    km15 - inercia: 7680.839 - silhouette: 0.265
    km20 - inercia: 6102.534 - silhouette: 0.257
    km25 - inercia: 4856.639 - silhouette: 0.250
### 14
    km03 - inercia: 24035.045 - silhouette: 0.374
    km05 - inercia: 10994.566 - silhouette: 0.355
    km10 - inercia: 5085.759 - silhouette: 0.320
    km15 - inercia: 3584.761 - silhouette: 0.297
    km20 - inercia: 2812.052 - silhouette: 0.254
    km25 - inercia: 2140.522 - silhouette: 0.237
### 15
    km03 - inercia: 73296.394 - silhouette: 0.279
    km05 - inercia: 49339.956 - silhouette: 0.287
    km10 - inercia: 30379.171 - silhouette: 0.276
    km15 - inercia: 21644.155 - silhouette: 0.243
    km20 - inercia: 16655.172 - silhouette: 0.258
    km25 - inercia: 13393.556 - silhouette: 0.246
### 16
    km03 - inercia: 104776.177 - silhouette: 0.383
    km05 - inercia: 53899.060 - silhouette: 0.406
    km10 - inercia: 26517.306 - silhouette: 0.338
    km15 - inercia: 14979.666 - silhouette: 0.331
    km20 - inercia: 9499.653 - silhouette: 0.333
    km25 - inercia: 6687.156 - silhouette: 0.357
### 17
    km03 - inercia: 101428.876 - silhouette: 0.473
    km05 - inercia: 55839.148 - silhouette: 0.410
    km10 - inercia: 27258.901 - silhouette: 0.337
    km15 - inercia: 18746.517 - silhouette: 0.317
    km20 - inercia: 13846.695 - silhouette: 0.278
    km25 - inercia: 11175.713 - silhouette: 0.287
### 18
    km03 - inercia: 18950.065 - silhouette: 0.421
    km05 - inercia: 12949.938 - silhouette: 0.366
    km10 - inercia: 7745.681 - silhouette: 0.288
    km15 - inercia: 5302.848 - silhouette: 0.275
    km20 - inercia: 4144.005 - silhouette: 0.277
    km25 - inercia: 3534.716 - silhouette: 0.224
### 19
    km03 - inercia: 56114.546 - silhouette: 0.382
    km05 - inercia: 38990.256 - silhouette: 0.306
    km10 - inercia: 23349.163 - silhouette: 0.280
    km15 - inercia: 16179.448 - silhouette: 0.277
    km20 - inercia: 12734.003 - silhouette: 0.275
    km25 - inercia: 9781.301 - silhouette: 0.277
### 20
    km03 - inercia: 15828.046 - silhouette: 0.407
    km05 - inercia: 6640.625 - silhouette: 0.378
    km10 - inercia: 1587.035 - silhouette: 0.327
    km15 - inercia: 566.274 - silhouette: 0.265
    km20 - inercia: 243.249 - silhouette: 0.149
    km25 - inercia: 82.774 - silhouette: 0.108
### 21
    km03 - inercia: 197980.237 - silhouette: 0.343
    km05 - inercia: 144773.986 - silhouette: 0.259
    km10 - inercia: 92039.156 - silhouette: 0.245
    km15 - inercia: 71547.752 - silhouette: 0.231
    km20 - inercia: 57931.928 - silhouette: 0.207
    km25 - inercia: 48388.960 - silhouette: 0.201
### 22
    km03 - inercia: 59253.665 - silhouette: 0.582
    km05 - inercia: 33241.004 - silhouette: 0.439
    km10 - inercia: 11795.223 - silhouette: 0.402
    km15 - inercia: 7303.832 - silhouette: 0.340
    km20 - inercia: 4836.120 - silhouette: 0.353
    km25 - inercia: 3561.186 - silhouette: 0.341
### 23
    km03 - inercia: 56060.061 - silhouette: 0.477
    km05 - inercia: 37692.618 - silhouette: 0.500
    km10 - inercia: 15560.669 - silhouette: 0.348
    km15 - inercia: 9269.568 - silhouette: 0.308
    km20 - inercia: 6781.778 - silhouette: 0.283
    km25 - inercia: 5281.328 - silhouette: 0.261
### 24
    km03 - inercia: 7121.922 - silhouette: 0.615
    km05 - inercia: 3190.650 - silhouette: 0.389
    km10 - inercia: 507.495 - silhouette: 0.307
    km15 - inercia: 82.060 - silhouette: 0.218
    km20 - inercia: 12.642 - silhouette: 0.090
    (n_samples=23 should be >= n_clusters=25.)
## SeccionesXDistrito(codEleccion=e2015_paso,codCategoria=03)
### 01
    km03 - inercia: 234.503 - silhouette: 0.481
    km05 - inercia: 89.685 - silhouette: 0.458
    km10 - inercia: 10.403 - silhouette: 0.256
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 16305.363 - silhouette: 0.292
    km05 - inercia: 11873.944 - silhouette: 0.270
    km10 - inercia: 7327.883 - silhouette: 0.251
    km15 - inercia: 5524.749 - silhouette: 0.244
    km20 - inercia: 4127.047 - silhouette: 0.262
    km25 - inercia: 3154.229 - silhouette: 0.272
### 03
    km03 - inercia: 709.138 - silhouette: 0.477
    km05 - inercia: 311.804 - silhouette: 0.389
    km10 - inercia: 68.572 - silhouette: 0.187
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 04
    km03 - inercia: 1282.999 - silhouette: 0.322
    km05 - inercia: 784.959 - silhouette: 0.309
    km10 - inercia: 247.358 - silhouette: 0.295
    km15 - inercia: 90.942 - silhouette: 0.268
    km20 - inercia: 30.624 - silhouette: 0.195
    km25 - inercia: 3.653 - silhouette: 0.046
### 05
    km03 - inercia: 781.113 - silhouette: 0.399
    km05 - inercia: 432.326 - silhouette: 0.387
    km10 - inercia: 136.278 - silhouette: 0.343
    km15 - inercia: 39.180 - silhouette: 0.284
    km20 - inercia: 11.718 - silhouette: 0.166
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 06
    km03 - inercia: 675.116 - silhouette: 0.347
    km05 - inercia: 386.277 - silhouette: 0.374
    km10 - inercia: 141.989 - silhouette: 0.326
    km15 - inercia: 54.866 - silhouette: 0.258
    km20 - inercia: 14.593 - silhouette: 0.167
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 07
    km03 - inercia: 1666.830 - silhouette: 0.378
    km05 - inercia: 818.696 - silhouette: 0.323
    km10 - inercia: 166.866 - silhouette: 0.169
    (n_samples=14 should be >= n_clusters=15.)
    (n_samples=14 should be >= n_clusters=20.)
    (n_samples=14 should be >= n_clusters=25.)
### 08
    km03 - inercia: 2316.461 - silhouette: 0.370
    km05 - inercia: 1058.671 - silhouette: 0.365
    km10 - inercia: 233.037 - silhouette: 0.271
    km15 - inercia: 24.968 - silhouette: 0.082
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 09
    km03 - inercia: 139.570 - silhouette: 0.531
    km05 - inercia: 18.939 - silhouette: 0.449
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 10
    km03 - inercia: 575.965 - silhouette: 0.451
    km05 - inercia: 250.071 - silhouette: 0.371
    km10 - inercia: 53.888 - silhouette: 0.288
    km15 - inercia: 2.665 - silhouette: 0.052
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 11
    km03 - inercia: 1299.809 - silhouette: 0.428
    km05 - inercia: 698.203 - silhouette: 0.375
    km10 - inercia: 199.649 - silhouette: 0.259
    km15 - inercia: 57.461 - silhouette: 0.213
    km20 - inercia: 6.949 - silhouette: 0.089
    (n_samples=22 should be >= n_clusters=25.)
### 12
    km03 - inercia: 935.682 - silhouette: 0.399
    km05 - inercia: 583.441 - silhouette: 0.341
    km10 - inercia: 158.576 - silhouette: 0.262
    km15 - inercia: 16.391 - silhouette: 0.158
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 13
    km03 - inercia: 773.806 - silhouette: 0.385
    km05 - inercia: 400.111 - silhouette: 0.356
    km10 - inercia: 107.104 - silhouette: 0.257
    km15 - inercia: 11.802 - silhouette: 0.157
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 14
    km03 - inercia: 813.039 - silhouette: 0.426
    km05 - inercia: 377.625 - silhouette: 0.398
    km10 - inercia: 115.564 - silhouette: 0.211
    km15 - inercia: 10.093 - silhouette: 0.098
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 15
    km03 - inercia: 961.603 - silhouette: 0.401
    km05 - inercia: 532.701 - silhouette: 0.328
    km10 - inercia: 132.345 - silhouette: 0.249
    km15 - inercia: 15.401 - silhouette: 0.021
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 16
    km03 - inercia: 1198.519 - silhouette: 0.432
    km05 - inercia: 456.583 - silhouette: 0.319
    km10 - inercia: 20.070 - silhouette: 0.287
    (n_samples=13 should be >= n_clusters=15.)
    (n_samples=13 should be >= n_clusters=20.)
    (n_samples=13 should be >= n_clusters=25.)
### 17
    km03 - inercia: 2535.476 - silhouette: 0.398
    km05 - inercia: 1205.752 - silhouette: 0.398
    km10 - inercia: 338.551 - silhouette: 0.346
    km15 - inercia: 104.430 - silhouette: 0.240
    km20 - inercia: 25.878 - silhouette: 0.080
    (n_samples=23 should be >= n_clusters=25.)
### 18
    km03 - inercia: 942.443 - silhouette: 0.404
    km05 - inercia: 510.230 - silhouette: 0.348
    km10 - inercia: 126.171 - silhouette: 0.281
    km15 - inercia: 19.032 - silhouette: 0.185
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 475.913 - silhouette: 0.485
    km05 - inercia: 141.237 - silhouette: 0.445
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 110.017 - silhouette: 0.524
    km05 - inercia: 20.225 - silhouette: 0.339
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 21
    km03 - inercia: 378.611 - silhouette: 0.448
    km05 - inercia: 217.330 - silhouette: 0.346
    km10 - inercia: 93.058 - silhouette: 0.198
    km15 - inercia: 20.476 - silhouette: 0.147
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 22
    km03 - inercia: 1377.929 - silhouette: 0.529
    km05 - inercia: 690.519 - silhouette: 0.356
    km10 - inercia: 199.013 - silhouette: 0.315
    km15 - inercia: 90.301 - silhouette: 0.193
    km20 - inercia: 27.338 - silhouette: 0.171
    km25 - inercia: 3.418 - silhouette: 0.073
### 23
    km03 - inercia: 383.776 - silhouette: 0.549
    km05 - inercia: 182.038 - silhouette: 0.478
    km10 - inercia: 28.781 - silhouette: 0.329
    km15 - inercia: 1.926 - silhouette: 0.083
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 24
    (Number of labels is 3. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=3 should be >= n_clusters=5.)
    (n_samples=3 should be >= n_clusters=10.)
    (n_samples=3 should be >= n_clusters=15.)
    (n_samples=3 should be >= n_clusters=20.)
    (n_samples=3 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2015_paso,codCategoria=04)
### 02
    km03 - inercia: 365387.888 - silhouette: 0.274
    km05 - inercia: 297528.651 - silhouette: 0.222
    km10 - inercia: 196193.042 - silhouette: 0.230
    km15 - inercia: 152954.585 - silhouette: 0.224
    km20 - inercia: 134389.789 - silhouette: 0.223
    km25 - inercia: 115478.037 - silhouette: 0.204
### 03
    km03 - inercia: 44054.520 - silhouette: 0.388
    km05 - inercia: 24543.734 - silhouette: 0.363
    km10 - inercia: 10731.931 - silhouette: 0.317
    km15 - inercia: 6773.811 - silhouette: 0.309
    km20 - inercia: 4626.998 - silhouette: 0.304
    km25 - inercia: 3380.366 - silhouette: 0.307
### 07
    km03 - inercia: 29131.354 - silhouette: 0.365
    km05 - inercia: 19751.200 - silhouette: 0.327
    km10 - inercia: 10342.329 - silhouette: 0.341
    km15 - inercia: 6927.651 - silhouette: 0.264
    km20 - inercia: 4885.475 - silhouette: 0.300
    km25 - inercia: 3948.982 - silhouette: 0.270
### 08
    km03 - inercia: 183738.239 - silhouette: 0.294
    km05 - inercia: 121946.108 - silhouette: 0.287
    km10 - inercia: 80291.093 - silhouette: 0.262
    km15 - inercia: 59317.939 - silhouette: 0.262
    km20 - inercia: 46607.978 - silhouette: 0.264
    km25 - inercia: 38581.329 - silhouette: 0.274
### 18
    km03 - inercia: 15887.457 - silhouette: 0.403
    km05 - inercia: 11082.500 - silhouette: 0.323
    km10 - inercia: 6817.581 - silhouette: 0.260
    km15 - inercia: 4973.869 - silhouette: 0.237
    km20 - inercia: 3808.898 - silhouette: 0.246
    km25 - inercia: 3128.837 - silhouette: 0.243
### 19
    km03 - inercia: 68160.222 - silhouette: 0.293
    km05 - inercia: 42813.065 - silhouette: 0.329
    km10 - inercia: 25728.215 - silhouette: 0.277
    km15 - inercia: 17750.998 - silhouette: 0.267
    km20 - inercia: 13420.099 - silhouette: 0.261
    km25 - inercia: 10560.908 - silhouette: 0.249
## SeccionesXDistrito(codEleccion=e2015_paso,codCategoria=04)
### 02
    km03 - inercia: 14289.314 - silhouette: 0.294
    km05 - inercia: 10431.576 - silhouette: 0.267
    km10 - inercia: 6764.700 - silhouette: 0.231
    km15 - inercia: 4826.404 - silhouette: 0.245
    km20 - inercia: 3778.445 - silhouette: 0.259
    km25 - inercia: 2805.989 - silhouette: 0.282
### 03
    km03 - inercia: 493.169 - silhouette: 0.483
    km05 - inercia: 181.071 - silhouette: 0.469
    km10 - inercia: 37.991 - silhouette: 0.241
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 07
    km03 - inercia: 1331.544 - silhouette: 0.367
    km05 - inercia: 713.967 - silhouette: 0.318
    km10 - inercia: 121.288 - silhouette: 0.187
    (n_samples=14 should be >= n_clusters=15.)
    (n_samples=14 should be >= n_clusters=20.)
    (n_samples=14 should be >= n_clusters=25.)
### 08
    km03 - inercia: 2117.408 - silhouette: 0.322
    km05 - inercia: 914.959 - silhouette: 0.345
    km10 - inercia: 216.957 - silhouette: 0.284
    km15 - inercia: 17.655 - silhouette: 0.066
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 18
    km03 - inercia: 835.323 - silhouette: 0.442
    km05 - inercia: 434.210 - silhouette: 0.384
    km10 - inercia: 127.695 - silhouette: 0.253
    km15 - inercia: 24.244 - silhouette: 0.159
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 532.760 - silhouette: 0.429
    km05 - inercia: 181.118 - silhouette: 0.322
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2015_paso,codCategoria=06)
### 02
    km03 - inercia: 255553.374 - silhouette: 0.236
    km05 - inercia: 187222.015 - silhouette: 0.239
    km10 - inercia: 124418.051 - silhouette: 0.219
    km15 - inercia: 97290.527 - silhouette: 0.203
    km20 - inercia: 80080.341 - silhouette: 0.206
    km25 - inercia: 67856.008 - silhouette: 0.204
### 03
    km03 - inercia: 57442.085 - silhouette: 0.350
    km05 - inercia: 36900.912 - silhouette: 0.369
    km10 - inercia: 17245.972 - silhouette: 0.307
    km15 - inercia: 10958.457 - silhouette: 0.309
    km20 - inercia: 7910.877 - silhouette: 0.305
    km25 - inercia: 5789.128 - silhouette: 0.306
### 07
    km03 - inercia: 35002.469 - silhouette: 0.296
    km05 - inercia: 24645.751 - silhouette: 0.298
    km10 - inercia: 12709.439 - silhouette: 0.304
    km15 - inercia: 9062.809 - silhouette: 0.275
    km20 - inercia: 6710.973 - silhouette: 0.278
    km25 - inercia: 5150.584 - silhouette: 0.257
### 08
    km03 - inercia: 265737.486 - silhouette: 0.308
    km05 - inercia: 184257.405 - silhouette: 0.265
    km10 - inercia: 110912.265 - silhouette: 0.246
    km15 - inercia: 77854.616 - silhouette: 0.248
    km20 - inercia: 58929.886 - silhouette: 0.266
    km25 - inercia: 47801.143 - silhouette: 0.265
### 18
    km03 - inercia: 21250.141 - silhouette: 0.411
    km05 - inercia: 15081.636 - silhouette: 0.337
    km10 - inercia: 8685.057 - silhouette: 0.282
    km15 - inercia: 6261.698 - silhouette: 0.272
    km20 - inercia: 4791.409 - silhouette: 0.259
    km25 - inercia: 3826.606 - silhouette: 0.259
### 19
    km03 - inercia: 39096.321 - silhouette: 0.308
    km05 - inercia: 26160.194 - silhouette: 0.263
    km10 - inercia: 14130.348 - silhouette: 0.263
    km15 - inercia: 8928.269 - silhouette: 0.272
    km20 - inercia: 5944.993 - silhouette: 0.250
    km25 - inercia: 4009.646 - silhouette: 0.231
## SeccionesXDistrito(codEleccion=e2015_paso,codCategoria=06)
### 02
    km03 - inercia: 10031.526 - silhouette: 0.315
    km05 - inercia: 7422.812 - silhouette: 0.282
    km10 - inercia: 4518.435 - silhouette: 0.260
    km15 - inercia: 2656.109 - silhouette: 0.304
    km20 - inercia: 1914.292 - silhouette: 0.293
    km25 - inercia: 1405.930 - silhouette: 0.285
### 03
    km03 - inercia: 696.057 - silhouette: 0.374
    km05 - inercia: 351.891 - silhouette: 0.357
    km10 - inercia: 55.169 - silhouette: 0.263
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 07
    km03 - inercia: 1762.625 - silhouette: 0.346
    km05 - inercia: 850.572 - silhouette: 0.306
    km10 - inercia: 130.301 - silhouette: 0.231
    (n_samples=14 should be >= n_clusters=15.)
    (n_samples=14 should be >= n_clusters=20.)
    (n_samples=14 should be >= n_clusters=25.)
### 08
    km03 - inercia: 3031.137 - silhouette: 0.333
    km05 - inercia: 1285.122 - silhouette: 0.385
    km10 - inercia: 258.467 - silhouette: 0.317
    km15 - inercia: 26.126 - silhouette: 0.104
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 18
    km03 - inercia: 1171.971 - silhouette: 0.409
    km05 - inercia: 678.332 - silhouette: 0.345
    km10 - inercia: 164.014 - silhouette: 0.306
    km15 - inercia: 34.336 - silhouette: 0.174
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 127.054 - silhouette: 0.128
    (n_samples=4 should be >= n_clusters=5.)
    (n_samples=4 should be >= n_clusters=10.)
    (n_samples=4 should be >= n_clusters=15.)
    (n_samples=4 should be >= n_clusters=20.)
    (n_samples=4 should be >= n_clusters=25.)
## CircuitosXPais(codEleccion=e2015_generales,codCategoria=01)
    km03 - inercia: 2289563.774 - silhouette: 0.348
    km05 - inercia: 1539468.861 - silhouette: 0.329
    km10 - inercia: 954783.802 - silhouette: 0.288
    km15 - inercia: 732368.205 - silhouette: 0.285
    km20 - inercia: 592864.057 - silhouette: 0.262
    km25 - inercia: 510830.135 - silhouette: 0.257
## SeccionesXPais(codEleccion=e2015_generales,codCategoria=01)
    km03 - inercia: 115960.896 - silhouette: 0.352
    km05 - inercia: 75569.920 - silhouette: 0.331
    km10 - inercia: 39194.549 - silhouette: 0.327
    km15 - inercia: 28375.274 - silhouette: 0.305
    km20 - inercia: 22540.918 - silhouette: 0.278
    km25 - inercia: 18297.247 - silhouette: 0.289
## CircuitosXDistrito(codEleccion=e2015_generales,codCategoria=03)
### 01
    km03 - inercia: 8118.447 - silhouette: 0.446
    km05 - inercia: 3879.173 - silhouette: 0.394
    km10 - inercia: 1656.279 - silhouette: 0.355
    km15 - inercia: 1036.417 - silhouette: 0.345
    km20 - inercia: 805.914 - silhouette: 0.327
    km25 - inercia: 643.826 - silhouette: 0.334
### 02
    km03 - inercia: 246256.987 - silhouette: 0.295
    km05 - inercia: 179191.453 - silhouette: 0.274
    km10 - inercia: 120404.880 - silhouette: 0.242
    km15 - inercia: 94884.309 - silhouette: 0.238
    km20 - inercia: 80052.645 - silhouette: 0.243
    km25 - inercia: 69749.416 - silhouette: 0.244
### 03
    km03 - inercia: 36728.596 - silhouette: 0.290
    km05 - inercia: 25894.883 - silhouette: 0.311
    km10 - inercia: 13873.420 - silhouette: 0.288
    km15 - inercia: 9073.782 - silhouette: 0.313
    km20 - inercia: 6886.684 - silhouette: 0.305
    km25 - inercia: 5262.469 - silhouette: 0.305
### 04
    km03 - inercia: 196487.978 - silhouette: 0.361
    km05 - inercia: 145576.149 - silhouette: 0.287
    km10 - inercia: 83168.296 - silhouette: 0.273
    km15 - inercia: 63409.509 - silhouette: 0.261
    km20 - inercia: 52334.681 - silhouette: 0.243
    km25 - inercia: 44166.966 - silhouette: 0.240
### 05
    km03 - inercia: 23311.411 - silhouette: 0.395
    km05 - inercia: 14858.881 - silhouette: 0.353
    km10 - inercia: 7473.754 - silhouette: 0.311
    km15 - inercia: 5282.238 - silhouette: 0.306
    km20 - inercia: 3842.463 - silhouette: 0.293
    km25 - inercia: 2864.318 - silhouette: 0.318
### 06
    km03 - inercia: 20794.405 - silhouette: 0.420
    km05 - inercia: 12475.251 - silhouette: 0.359
    km10 - inercia: 5809.234 - silhouette: 0.356
    km15 - inercia: 3826.094 - silhouette: 0.352
    km20 - inercia: 2701.818 - silhouette: 0.342
    km25 - inercia: 2128.352 - silhouette: 0.330
### 07
    km03 - inercia: 46850.829 - silhouette: 0.316
    km05 - inercia: 33247.619 - silhouette: 0.308
    km10 - inercia: 16578.330 - silhouette: 0.325
    km15 - inercia: 11053.074 - silhouette: 0.280
    km20 - inercia: 8165.059 - silhouette: 0.293
    km25 - inercia: 5989.696 - silhouette: 0.310
### 08
    km03 - inercia: 118067.705 - silhouette: 0.298
    km05 - inercia: 80331.765 - silhouette: 0.274
    km10 - inercia: 48428.547 - silhouette: 0.290
    km15 - inercia: 34740.494 - silhouette: 0.291
    km20 - inercia: 27396.626 - silhouette: 0.285
    km25 - inercia: 22162.012 - silhouette: 0.278
### 09
    km03 - inercia: 39548.583 - silhouette: 0.463
    km05 - inercia: 14012.457 - silhouette: 0.465
    km10 - inercia: 4928.746 - silhouette: 0.388
    km15 - inercia: 2516.016 - silhouette: 0.406
    km20 - inercia: 1439.469 - silhouette: 0.433
    km25 - inercia: 997.252 - silhouette: 0.428
### 10
    km03 - inercia: 32511.671 - silhouette: 0.370
    km05 - inercia: 18897.641 - silhouette: 0.370
    km10 - inercia: 9095.437 - silhouette: 0.348
    km15 - inercia: 5889.804 - silhouette: 0.337
    km20 - inercia: 4359.858 - silhouette: 0.313
    km25 - inercia: 3109.496 - silhouette: 0.357
### 11
    km03 - inercia: 50372.790 - silhouette: 0.318
    km05 - inercia: 30854.856 - silhouette: 0.301
    km10 - inercia: 10866.392 - silhouette: 0.329
    km15 - inercia: 6040.536 - silhouette: 0.331
    km20 - inercia: 4553.186 - silhouette: 0.256
    km25 - inercia: 3281.645 - silhouette: 0.267
### 12
    km03 - inercia: 24539.809 - silhouette: 0.460
    km05 - inercia: 16301.293 - silhouette: 0.364
    km10 - inercia: 8452.678 - silhouette: 0.344
    km15 - inercia: 5490.146 - silhouette: 0.298
    km20 - inercia: 4019.894 - silhouette: 0.300
    km25 - inercia: 3208.630 - silhouette: 0.287
### 13
    km03 - inercia: 26553.649 - silhouette: 0.395
    km05 - inercia: 19002.523 - silhouette: 0.344
    km10 - inercia: 10887.075 - silhouette: 0.303
    km15 - inercia: 6891.478 - silhouette: 0.351
    km20 - inercia: 5452.016 - silhouette: 0.315
    km25 - inercia: 4171.958 - silhouette: 0.308
### 14
    km03 - inercia: 21095.365 - silhouette: 0.445
    km05 - inercia: 13402.246 - silhouette: 0.425
    km10 - inercia: 7559.290 - silhouette: 0.283
    km15 - inercia: 5439.326 - silhouette: 0.274
    km20 - inercia: 3760.271 - silhouette: 0.292
    km25 - inercia: 2928.917 - silhouette: 0.286
### 15
    km03 - inercia: 50334.801 - silhouette: 0.358
    km05 - inercia: 36117.873 - silhouette: 0.330
    km10 - inercia: 21565.903 - silhouette: 0.272
    km15 - inercia: 16941.615 - silhouette: 0.240
    km20 - inercia: 13450.830 - silhouette: 0.234
    km25 - inercia: 11798.327 - silhouette: 0.213
### 16
    km03 - inercia: 53333.763 - silhouette: 0.426
    km05 - inercia: 30755.118 - silhouette: 0.408
    km10 - inercia: 16673.760 - silhouette: 0.323
    km15 - inercia: 11780.962 - silhouette: 0.305
    km20 - inercia: 7988.896 - silhouette: 0.322
    km25 - inercia: 6529.714 - silhouette: 0.291
### 17
    km03 - inercia: 68223.298 - silhouette: 0.387
    km05 - inercia: 41621.171 - silhouette: 0.377
    km10 - inercia: 20900.051 - silhouette: 0.321
    km15 - inercia: 14144.488 - silhouette: 0.331
    km20 - inercia: 11271.600 - silhouette: 0.287
    km25 - inercia: 9133.631 - silhouette: 0.303
### 18
    km03 - inercia: 37958.567 - silhouette: 0.392
    km05 - inercia: 20497.883 - silhouette: 0.323
    km10 - inercia: 10682.340 - silhouette: 0.323
    km15 - inercia: 7183.604 - silhouette: 0.319
    km20 - inercia: 5207.698 - silhouette: 0.325
    km25 - inercia: 4144.479 - silhouette: 0.293
### 19
    km03 - inercia: 59288.922 - silhouette: 0.373
    km05 - inercia: 40380.016 - silhouette: 0.308
    km10 - inercia: 20816.838 - silhouette: 0.325
    km15 - inercia: 13310.630 - silhouette: 0.300
    km20 - inercia: 10200.964 - silhouette: 0.285
    km25 - inercia: 8046.088 - silhouette: 0.278
### 20
    km03 - inercia: 19452.854 - silhouette: 0.667
    km05 - inercia: 7166.182 - silhouette: 0.381
    km10 - inercia: 1460.598 - silhouette: 0.388
    km15 - inercia: 416.306 - silhouette: 0.282
    km20 - inercia: 151.355 - silhouette: 0.247
    km25 - inercia: 35.870 - silhouette: 0.134
### 21
    km03 - inercia: 141317.043 - silhouette: 0.248
    km05 - inercia: 106356.565 - silhouette: 0.260
    km10 - inercia: 72265.800 - silhouette: 0.219
    km15 - inercia: 55355.076 - silhouette: 0.229
    km20 - inercia: 46459.164 - silhouette: 0.225
    km25 - inercia: 40161.529 - silhouette: 0.208
### 22
    km03 - inercia: 36194.542 - silhouette: 0.417
    km05 - inercia: 20322.207 - silhouette: 0.405
    km10 - inercia: 9642.070 - silhouette: 0.325
    km15 - inercia: 6509.290 - silhouette: 0.297
    km20 - inercia: 4829.717 - silhouette: 0.288
    km25 - inercia: 3701.201 - silhouette: 0.297
### 23
    km03 - inercia: 51203.278 - silhouette: 0.451
    km05 - inercia: 33515.634 - silhouette: 0.421
    km10 - inercia: 16881.048 - silhouette: 0.313
    km15 - inercia: 11871.531 - silhouette: 0.286
    km20 - inercia: 8999.489 - silhouette: 0.278
    km25 - inercia: 7020.500 - silhouette: 0.294
### 24
    km03 - inercia: 3819.017 - silhouette: 0.615
    km05 - inercia: 1873.310 - silhouette: 0.375
    km10 - inercia: 448.785 - silhouette: 0.344
    km15 - inercia: 82.266 - silhouette: 0.182
    km20 - inercia: 11.874 - silhouette: 0.066
    (n_samples=23 should be >= n_clusters=25.)
## SeccionesXDistrito(codEleccion=e2015_generales,codCategoria=03)
### 01
    km03 - inercia: 237.946 - silhouette: 0.503
    km05 - inercia: 107.087 - silhouette: 0.438
    km10 - inercia: 10.140 - silhouette: 0.349
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 12053.287 - silhouette: 0.324
    km05 - inercia: 8118.252 - silhouette: 0.315
    km10 - inercia: 5296.404 - silhouette: 0.247
    km15 - inercia: 3716.799 - silhouette: 0.270
    km20 - inercia: 2651.323 - silhouette: 0.293
    km25 - inercia: 1997.507 - silhouette: 0.301
### 03
    km03 - inercia: 756.613 - silhouette: 0.333
    km05 - inercia: 462.416 - silhouette: 0.303
    km10 - inercia: 131.644 - silhouette: 0.170
    km15 - inercia: 5.369 - silhouette: 0.027
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 04
    km03 - inercia: 1155.122 - silhouette: 0.366
    km05 - inercia: 617.164 - silhouette: 0.373
    km10 - inercia: 237.271 - silhouette: 0.321
    km15 - inercia: 88.506 - silhouette: 0.251
    km20 - inercia: 30.331 - silhouette: 0.149
    km25 - inercia: 1.905 - silhouette: 0.042
### 05
    km03 - inercia: 738.817 - silhouette: 0.357
    km05 - inercia: 440.651 - silhouette: 0.335
    km10 - inercia: 171.709 - silhouette: 0.276
    km15 - inercia: 80.465 - silhouette: 0.179
    km20 - inercia: 14.219 - silhouette: 0.167
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 06
    km03 - inercia: 516.767 - silhouette: 0.386
    km05 - inercia: 314.452 - silhouette: 0.337
    km10 - inercia: 87.863 - silhouette: 0.361
    km15 - inercia: 30.256 - silhouette: 0.291
    km20 - inercia: 3.195 - silhouette: 0.222
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 07
    km03 - inercia: 1743.189 - silhouette: 0.440
    km05 - inercia: 787.770 - silhouette: 0.328
    km10 - inercia: 141.080 - silhouette: 0.213
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 08
    km03 - inercia: 1439.750 - silhouette: 0.269
    km05 - inercia: 546.551 - silhouette: 0.354
    km10 - inercia: 150.538 - silhouette: 0.225
    km15 - inercia: 15.413 - silhouette: 0.098
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 09
    km03 - inercia: 175.120 - silhouette: 0.416
    km05 - inercia: 38.016 - silhouette: 0.294
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 10
    km03 - inercia: 1296.310 - silhouette: 0.395
    km05 - inercia: 570.324 - silhouette: 0.365
    km10 - inercia: 92.402 - silhouette: 0.277
    km15 - inercia: 4.800 - silhouette: 0.043
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 11
    km03 - inercia: 2971.818 - silhouette: 0.467
    km05 - inercia: 1842.772 - silhouette: 0.318
    km10 - inercia: 428.298 - silhouette: 0.269
    km15 - inercia: 85.017 - silhouette: 0.185
    km20 - inercia: 6.060 - silhouette: 0.077
    (n_samples=22 should be >= n_clusters=25.)
### 12
    km03 - inercia: 779.945 - silhouette: 0.336
    km05 - inercia: 452.942 - silhouette: 0.293
    km10 - inercia: 108.255 - silhouette: 0.274
    km15 - inercia: 18.865 - silhouette: 0.111
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 13
    km03 - inercia: 833.080 - silhouette: 0.306
    km05 - inercia: 435.426 - silhouette: 0.348
    km10 - inercia: 67.961 - silhouette: 0.389
    km15 - inercia: 3.134 - silhouette: 0.252
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 14
    km03 - inercia: 1061.909 - silhouette: 0.385
    km05 - inercia: 504.148 - silhouette: 0.382
    km10 - inercia: 101.425 - silhouette: 0.357
    km15 - inercia: 8.660 - silhouette: 0.166
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 15
    km03 - inercia: 1132.195 - silhouette: 0.331
    km05 - inercia: 766.352 - silhouette: 0.206
    km10 - inercia: 168.481 - silhouette: 0.234
    km15 - inercia: 5.406 - silhouette: 0.091
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 16
    km03 - inercia: 1287.324 - silhouette: 0.445
    km05 - inercia: 480.810 - silhouette: 0.383
    km10 - inercia: 31.518 - silhouette: 0.246
    (n_samples=13 should be >= n_clusters=15.)
    (n_samples=13 should be >= n_clusters=20.)
    (n_samples=13 should be >= n_clusters=25.)
### 17
    km03 - inercia: 2848.901 - silhouette: 0.340
    km05 - inercia: 1391.644 - silhouette: 0.356
    km10 - inercia: 365.818 - silhouette: 0.323
    km15 - inercia: 98.869 - silhouette: 0.266
    km20 - inercia: 14.856 - silhouette: 0.131
    (n_samples=23 should be >= n_clusters=25.)
### 18
    km03 - inercia: 1400.665 - silhouette: 0.371
    km05 - inercia: 688.173 - silhouette: 0.334
    km10 - inercia: 202.443 - silhouette: 0.236
    km15 - inercia: 43.390 - silhouette: 0.131
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 525.995 - silhouette: 0.406
    km05 - inercia: 109.285 - silhouette: 0.378
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 112.594 - silhouette: 0.514
    km05 - inercia: 24.821 - silhouette: 0.175
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 21
    km03 - inercia: 527.055 - silhouette: 0.406
    km05 - inercia: 272.047 - silhouette: 0.359
    km10 - inercia: 101.652 - silhouette: 0.214
    km15 - inercia: 26.112 - silhouette: 0.099
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 22
    km03 - inercia: 1526.283 - silhouette: 0.534
    km05 - inercia: 571.368 - silhouette: 0.550
    km10 - inercia: 200.343 - silhouette: 0.284
    km15 - inercia: 57.118 - silhouette: 0.203
    km20 - inercia: 14.894 - silhouette: 0.195
    km25 - inercia: 1.443 - silhouette: 0.085
### 23
    km03 - inercia: 698.966 - silhouette: 0.483
    km05 - inercia: 330.829 - silhouette: 0.381
    km10 - inercia: 70.903 - silhouette: 0.288
    km15 - inercia: 6.479 - silhouette: 0.080
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 24
    (Number of labels is 3. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=3 should be >= n_clusters=5.)
    (n_samples=3 should be >= n_clusters=10.)
    (n_samples=3 should be >= n_clusters=15.)
    (n_samples=3 should be >= n_clusters=20.)
    (n_samples=3 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2015_generales,codCategoria=04)
### 02
    km03 - inercia: 209247.320 - silhouette: 0.313
    km05 - inercia: 147647.211 - silhouette: 0.301
    km10 - inercia: 98479.100 - silhouette: 0.251
    km15 - inercia: 76536.270 - silhouette: 0.236
    km20 - inercia: 64478.486 - silhouette: 0.232
    km25 - inercia: 55405.015 - silhouette: 0.233
### 03
    km03 - inercia: 32398.344 - silhouette: 0.476
    km05 - inercia: 21338.454 - silhouette: 0.359
    km10 - inercia: 9080.007 - silhouette: 0.337
    km15 - inercia: 5683.973 - silhouette: 0.335
    km20 - inercia: 3567.528 - silhouette: 0.344
    km25 - inercia: 2712.455 - silhouette: 0.348
### 07
    km03 - inercia: 20305.042 - silhouette: 0.405
    km05 - inercia: 12991.657 - silhouette: 0.371
    km10 - inercia: 7869.411 - silhouette: 0.314
    km15 - inercia: 5555.028 - silhouette: 0.304
    km20 - inercia: 3991.595 - silhouette: 0.298
    km25 - inercia: 3009.096 - silhouette: 0.280
### 08
    km03 - inercia: 103986.699 - silhouette: 0.326
    km05 - inercia: 67240.715 - silhouette: 0.285
    km10 - inercia: 41345.319 - silhouette: 0.289
    km15 - inercia: 28310.145 - silhouette: 0.276
    km20 - inercia: 22118.283 - silhouette: 0.269
    km25 - inercia: 18861.216 - silhouette: 0.238
### 10
    km03 - inercia: 20055.379 - silhouette: 0.438
    km05 - inercia: 12076.458 - silhouette: 0.383
    km10 - inercia: 6742.055 - silhouette: 0.332
    km15 - inercia: 4391.674 - silhouette: 0.308
    km20 - inercia: 3258.079 - silhouette: 0.269
    km25 - inercia: 2390.948 - silhouette: 0.276
### 14
    km03 - inercia: 20799.227 - silhouette: 0.448
    km05 - inercia: 14099.955 - silhouette: 0.344
    km10 - inercia: 7517.052 - silhouette: 0.307
    km15 - inercia: 5512.706 - silhouette: 0.269
    km20 - inercia: 4021.794 - silhouette: 0.265
    km25 - inercia: 3157.347 - silhouette: 0.242
### 18
    km03 - inercia: 16513.464 - silhouette: 0.391
    km05 - inercia: 10519.552 - silhouette: 0.355
    km10 - inercia: 6490.358 - silhouette: 0.298
    km15 - inercia: 4559.556 - silhouette: 0.292
    km20 - inercia: 3634.883 - silhouette: 0.277
    km25 - inercia: 2915.300 - silhouette: 0.281
### 19
    km03 - inercia: 64867.379 - silhouette: 0.367
    km05 - inercia: 44229.778 - silhouette: 0.308
    km10 - inercia: 24361.995 - silhouette: 0.288
    km15 - inercia: 15958.628 - silhouette: 0.306
    km20 - inercia: 11855.512 - silhouette: 0.300
    km25 - inercia: 9159.609 - silhouette: 0.302
### 20
    km03 - inercia: 8673.726 - silhouette: 0.458
    km05 - inercia: 3442.420 - silhouette: 0.461
    km10 - inercia: 676.276 - silhouette: 0.424
    km15 - inercia: 255.954 - silhouette: 0.271
    km20 - inercia: 72.746 - silhouette: 0.224
    km25 - inercia: 13.939 - silhouette: 0.121
## SeccionesXDistrito(codEleccion=e2015_generales,codCategoria=04)
### 02
    km03 - inercia: 10205.283 - silhouette: 0.330
    km05 - inercia: 6583.932 - silhouette: 0.328
    km10 - inercia: 3829.237 - silhouette: 0.274
    km15 - inercia: 2680.849 - silhouette: 0.281
    km20 - inercia: 2025.903 - silhouette: 0.293
    km25 - inercia: 1568.334 - silhouette: 0.297
### 03
    km03 - inercia: 536.496 - silhouette: 0.310
    km05 - inercia: 249.199 - silhouette: 0.384
    km10 - inercia: 59.249 - silhouette: 0.193
    km15 - inercia: 2.205 - silhouette: 0.057
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 07
    km03 - inercia: 1023.596 - silhouette: 0.486
    km05 - inercia: 552.270 - silhouette: 0.312
    km10 - inercia: 88.918 - silhouette: 0.210
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 08
    km03 - inercia: 1113.558 - silhouette: 0.331
    km05 - inercia: 416.551 - silhouette: 0.385
    km10 - inercia: 110.815 - silhouette: 0.247
    km15 - inercia: 15.636 - silhouette: 0.091
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 10
    km03 - inercia: 587.436 - silhouette: 0.515
    km05 - inercia: 241.903 - silhouette: 0.407
    km10 - inercia: 42.555 - silhouette: 0.241
    km15 - inercia: 2.292 - silhouette: 0.036
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 14
    km03 - inercia: 1022.465 - silhouette: 0.457
    km05 - inercia: 463.179 - silhouette: 0.438
    km10 - inercia: 102.125 - silhouette: 0.245
    km15 - inercia: 21.832 - silhouette: 0.060
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 18
    km03 - inercia: 1002.111 - silhouette: 0.416
    km05 - inercia: 495.331 - silhouette: 0.336
    km10 - inercia: 141.263 - silhouette: 0.233
    km15 - inercia: 31.677 - silhouette: 0.129
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 499.482 - silhouette: 0.463
    km05 - inercia: 92.078 - silhouette: 0.437
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 32.317 - silhouette: 0.633
    km05 - inercia: 7.994 - silhouette: 0.191
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2015_generales,codCategoria=06)
### 02
    km03 - inercia: 168999.599 - silhouette: 0.315
    km05 - inercia: 119748.056 - silhouette: 0.254
    km10 - inercia: 77247.069 - silhouette: 0.259
    km15 - inercia: 59205.789 - silhouette: 0.243
    km20 - inercia: 47873.231 - silhouette: 0.234
    km25 - inercia: 40867.698 - silhouette: 0.233
### 03
    km03 - inercia: 44800.252 - silhouette: 0.458
    km05 - inercia: 31544.954 - silhouette: 0.288
    km10 - inercia: 12772.033 - silhouette: 0.333
    km15 - inercia: 8474.724 - silhouette: 0.308
    km20 - inercia: 6084.964 - silhouette: 0.296
    km25 - inercia: 4670.524 - silhouette: 0.303
### 07
    km03 - inercia: 29916.878 - silhouette: 0.370
    km05 - inercia: 20281.377 - silhouette: 0.340
    km10 - inercia: 12149.806 - silhouette: 0.295
    km15 - inercia: 8788.523 - silhouette: 0.266
    km20 - inercia: 6562.920 - silhouette: 0.247
    km25 - inercia: 4855.434 - silhouette: 0.265
### 08
    km03 - inercia: 127723.034 - silhouette: 0.307
    km05 - inercia: 85874.466 - silhouette: 0.284
    km10 - inercia: 51046.771 - silhouette: 0.277
    km15 - inercia: 37054.180 - silhouette: 0.268
    km20 - inercia: 28485.963 - silhouette: 0.273
    km25 - inercia: 23792.247 - silhouette: 0.262
### 10
    km03 - inercia: 75986.614 - silhouette: 0.203
    km05 - inercia: 58218.925 - silhouette: 0.214
    km10 - inercia: 35268.487 - silhouette: 0.216
    km15 - inercia: 26479.344 - silhouette: 0.198
    km20 - inercia: 21062.275 - silhouette: 0.201
    km25 - inercia: 16933.085 - silhouette: 0.216
### 14
    km03 - inercia: 20784.619 - silhouette: 0.446
    km05 - inercia: 13729.570 - silhouette: 0.339
    km10 - inercia: 7542.812 - silhouette: 0.281
    km15 - inercia: 5711.944 - silhouette: 0.252
    km20 - inercia: 4128.440 - silhouette: 0.246
    km25 - inercia: 3217.348 - silhouette: 0.256
### 18
    km03 - inercia: 29406.306 - silhouette: 0.346
    km05 - inercia: 19258.425 - silhouette: 0.351
    km10 - inercia: 11001.869 - silhouette: 0.316
    km15 - inercia: 7728.275 - silhouette: 0.335
    km20 - inercia: 5939.619 - silhouette: 0.318
    km25 - inercia: 4577.693 - silhouette: 0.303
### 19
    km03 - inercia: 68496.555 - silhouette: 0.340
    km05 - inercia: 44158.149 - silhouette: 0.313
    km10 - inercia: 20239.956 - silhouette: 0.307
    km15 - inercia: 11889.647 - silhouette: 0.326
    km20 - inercia: 7362.263 - silhouette: 0.342
    km25 - inercia: 4917.736 - silhouette: 0.334
### 20
    km03 - inercia: 11276.996 - silhouette: 0.363
    km05 - inercia: 6714.742 - silhouette: 0.290
    km10 - inercia: 1821.250 - silhouette: 0.371
    km15 - inercia: 667.912 - silhouette: 0.288
    km20 - inercia: 231.721 - silhouette: 0.201
    km25 - inercia: 54.707 - silhouette: 0.102
## SeccionesXDistrito(codEleccion=e2015_generales,codCategoria=06)
### 02
    km03 - inercia: 8216.314 - silhouette: 0.351
    km05 - inercia: 5192.952 - silhouette: 0.342
    km10 - inercia: 2827.037 - silhouette: 0.313
    km15 - inercia: 1810.633 - silhouette: 0.294
    km20 - inercia: 1237.711 - silhouette: 0.313
    km25 - inercia: 874.132 - silhouette: 0.306
### 03
    km03 - inercia: 766.668 - silhouette: 0.301
    km05 - inercia: 423.004 - silhouette: 0.262
    km10 - inercia: 139.190 - silhouette: 0.112
    km15 - inercia: 3.921 - silhouette: 0.052
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 07
    km03 - inercia: 1288.722 - silhouette: 0.492
    km05 - inercia: 626.308 - silhouette: 0.340
    km10 - inercia: 143.432 - silhouette: 0.173
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 08
    km03 - inercia: 1498.547 - silhouette: 0.350
    km05 - inercia: 567.065 - silhouette: 0.424
    km10 - inercia: 126.842 - silhouette: 0.245
    km15 - inercia: 18.597 - silhouette: 0.079
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 10
    km03 - inercia: 2464.332 - silhouette: 0.277
    km05 - inercia: 1346.846 - silhouette: 0.291
    km10 - inercia: 275.075 - silhouette: 0.226
    km15 - inercia: 21.260 - silhouette: 0.034
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 14
    km03 - inercia: 1082.604 - silhouette: 0.427
    km05 - inercia: 517.449 - silhouette: 0.407
    km10 - inercia: 120.443 - silhouette: 0.285
    km15 - inercia: 23.229 - silhouette: 0.058
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 18
    km03 - inercia: 2399.650 - silhouette: 0.396
    km05 - inercia: 1324.761 - silhouette: 0.383
    km10 - inercia: 371.832 - silhouette: 0.285
    km15 - inercia: 68.568 - silhouette: 0.132
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 428.832 - silhouette: 0.057
    (n_samples=4 should be >= n_clusters=5.)
    (n_samples=4 should be >= n_clusters=10.)
    (n_samples=4 should be >= n_clusters=15.)
    (n_samples=4 should be >= n_clusters=20.)
    (n_samples=4 should be >= n_clusters=25.)
### 20
    km03 - inercia: 182.304 - silhouette: 0.622
    km05 - inercia: 18.039 - silhouette: 0.373
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2017_paso,codCategoria=03)
### 01
    km03 - inercia: 5122.590 - silhouette: 0.440
    km05 - inercia: 2243.403 - silhouette: 0.453
    km10 - inercia: 791.104 - silhouette: 0.392
    km15 - inercia: 531.127 - silhouette: 0.315
    km20 - inercia: 401.555 - silhouette: 0.302
    km25 - inercia: 321.776 - silhouette: 0.299
### 02
    km03 - inercia: 352974.851 - silhouette: 0.303
    km05 - inercia: 283097.189 - silhouette: 0.266
    km10 - inercia: 191313.063 - silhouette: 0.272
    km15 - inercia: 149406.932 - silhouette: 0.222
    km20 - inercia: 122106.154 - silhouette: 0.218
    km25 - inercia: 105929.964 - silhouette: 0.194
### 03
    km03 - inercia: 41346.549 - silhouette: 0.426
    km05 - inercia: 26408.129 - silhouette: 0.392
    km10 - inercia: 14954.239 - silhouette: 0.338
    km15 - inercia: 9634.123 - silhouette: 0.306
    km20 - inercia: 6995.415 - silhouette: 0.299
    km25 - inercia: 5180.196 - silhouette: 0.303
### 04
    km03 - inercia: 139506.620 - silhouette: 0.325
    km05 - inercia: 100711.050 - silhouette: 0.306
    km10 - inercia: 60809.005 - silhouette: 0.267
    km15 - inercia: 44839.016 - silhouette: 0.263
    km20 - inercia: 36649.853 - silhouette: 0.244
    km25 - inercia: 31940.364 - silhouette: 0.233
### 05
    km03 - inercia: 53281.972 - silhouette: 0.344
    km05 - inercia: 32936.566 - silhouette: 0.352
    km10 - inercia: 13920.337 - silhouette: 0.336
    km15 - inercia: 9212.114 - silhouette: 0.341
    km20 - inercia: 6666.824 - silhouette: 0.318
    km25 - inercia: 5293.837 - silhouette: 0.291
### 06
    km03 - inercia: 29535.651 - silhouette: 0.431
    km05 - inercia: 20139.240 - silhouette: 0.335
    km10 - inercia: 10467.740 - silhouette: 0.337
    km15 - inercia: 6081.706 - silhouette: 0.327
    km20 - inercia: 4471.194 - silhouette: 0.308
    km25 - inercia: 3271.046 - silhouette: 0.298
### 07
    km03 - inercia: 25724.653 - silhouette: 0.355
    km05 - inercia: 16062.917 - silhouette: 0.359
    km10 - inercia: 8130.627 - silhouette: 0.296
    km15 - inercia: 5507.703 - silhouette: 0.302
    km20 - inercia: 3810.335 - silhouette: 0.289
    km25 - inercia: 2955.858 - silhouette: 0.263
### 08
    km03 - inercia: 37868.291 - silhouette: 0.454
    km05 - inercia: 23426.700 - silhouette: 0.381
    km10 - inercia: 12415.435 - silhouette: 0.314
    km15 - inercia: 8846.506 - silhouette: 0.269
    km20 - inercia: 6964.452 - silhouette: 0.235
    km25 - inercia: 5877.804 - silhouette: 0.221
### 09
    km03 - inercia: 39816.879 - silhouette: 0.434
    km05 - inercia: 18034.938 - silhouette: 0.434
    km10 - inercia: 9100.847 - silhouette: 0.327
    km15 - inercia: 6146.456 - silhouette: 0.314
    km20 - inercia: 4250.082 - silhouette: 0.292
    km25 - inercia: 3150.313 - silhouette: 0.269
### 10
    km03 - inercia: 32349.194 - silhouette: 0.365
    km05 - inercia: 22709.005 - silhouette: 0.297
    km10 - inercia: 12673.905 - silhouette: 0.303
    km15 - inercia: 8528.931 - silhouette: 0.295
    km20 - inercia: 6607.481 - silhouette: 0.288
    km25 - inercia: 5409.680 - silhouette: 0.269
### 11
    km03 - inercia: 13836.679 - silhouette: 0.484
    km05 - inercia: 7711.610 - silhouette: 0.438
    km10 - inercia: 2741.072 - silhouette: 0.342
    km15 - inercia: 1740.687 - silhouette: 0.324
    km20 - inercia: 1182.907 - silhouette: 0.265
    km25 - inercia: 868.605 - silhouette: 0.270
### 12
    km03 - inercia: 39361.606 - silhouette: 0.300
    km05 - inercia: 22751.839 - silhouette: 0.375
    km10 - inercia: 11326.081 - silhouette: 0.301
    km15 - inercia: 7824.857 - silhouette: 0.303
    km20 - inercia: 6052.989 - silhouette: 0.297
    km25 - inercia: 4830.797 - silhouette: 0.283
### 13
    km03 - inercia: 29419.338 - silhouette: 0.386
    km05 - inercia: 19102.662 - silhouette: 0.363
    km10 - inercia: 11470.875 - silhouette: 0.297
    km15 - inercia: 8142.889 - silhouette: 0.289
    km20 - inercia: 6295.195 - silhouette: 0.286
    km25 - inercia: 4980.763 - silhouette: 0.278
### 14
    km03 - inercia: 22411.655 - silhouette: 0.433
    km05 - inercia: 12800.509 - silhouette: 0.417
    km10 - inercia: 6674.352 - silhouette: 0.356
    km15 - inercia: 4171.819 - silhouette: 0.349
    km20 - inercia: 3028.269 - silhouette: 0.304
    km25 - inercia: 2379.526 - silhouette: 0.287
### 15
    km03 - inercia: 60098.918 - silhouette: 0.403
    km05 - inercia: 35447.996 - silhouette: 0.380
    km10 - inercia: 19671.006 - silhouette: 0.300
    km15 - inercia: 14065.335 - silhouette: 0.261
    km20 - inercia: 11599.825 - silhouette: 0.201
    km25 - inercia: 9467.039 - silhouette: 0.206
### 16
    km03 - inercia: 36877.537 - silhouette: 0.377
    km05 - inercia: 24710.306 - silhouette: 0.318
    km10 - inercia: 13618.955 - silhouette: 0.310
    km15 - inercia: 8800.833 - silhouette: 0.355
    km20 - inercia: 6623.871 - silhouette: 0.332
    km25 - inercia: 4968.484 - silhouette: 0.334
### 17
    km03 - inercia: 78176.684 - silhouette: 0.458
    km05 - inercia: 43110.140 - silhouette: 0.409
    km10 - inercia: 23163.448 - silhouette: 0.306
    km15 - inercia: 17586.570 - silhouette: 0.254
    km20 - inercia: 14139.919 - silhouette: 0.245
    km25 - inercia: 11892.041 - silhouette: 0.248
### 18
    km03 - inercia: 13545.932 - silhouette: 0.459
    km05 - inercia: 8006.070 - silhouette: 0.351
    km10 - inercia: 4767.075 - silhouette: 0.292
    km15 - inercia: 3677.354 - silhouette: 0.269
    km20 - inercia: 2905.889 - silhouette: 0.254
    km25 - inercia: 2460.064 - silhouette: 0.234
### 19
    km03 - inercia: 27691.721 - silhouette: 0.430
    km05 - inercia: 16393.916 - silhouette: 0.449
    km10 - inercia: 5495.249 - silhouette: 0.400
    km15 - inercia: 3024.625 - silhouette: 0.325
    km20 - inercia: 2294.810 - silhouette: 0.267
    km25 - inercia: 1782.167 - silhouette: 0.275
### 20
    km03 - inercia: 10361.619 - silhouette: 0.331
    km05 - inercia: 6489.022 - silhouette: 0.289
    km10 - inercia: 2159.978 - silhouette: 0.261
    km15 - inercia: 924.038 - silhouette: 0.208
    km20 - inercia: 276.441 - silhouette: 0.198
    km25 - inercia: 61.648 - silhouette: 0.189
### 21
    km03 - inercia: 204460.579 - silhouette: 0.287
    km05 - inercia: 163421.362 - silhouette: 0.244
    km10 - inercia: 111144.085 - silhouette: 0.232
    km15 - inercia: 77773.013 - silhouette: 0.201
    km20 - inercia: 63218.733 - silhouette: 0.210
    km25 - inercia: 54546.121 - silhouette: 0.208
### 22
    km03 - inercia: 20202.785 - silhouette: 0.490
    km05 - inercia: 13704.806 - silhouette: 0.357
    km10 - inercia: 7661.265 - silhouette: 0.317
    km15 - inercia: 5081.073 - silhouette: 0.311
    km20 - inercia: 3752.288 - silhouette: 0.271
    km25 - inercia: 3060.803 - silhouette: 0.269
### 23
    km03 - inercia: 30614.131 - silhouette: 0.491
    km05 - inercia: 17340.049 - silhouette: 0.418
    km10 - inercia: 8809.570 - silhouette: 0.319
    km15 - inercia: 6168.053 - silhouette: 0.323
    km20 - inercia: 4658.339 - silhouette: 0.283
    km25 - inercia: 3669.218 - silhouette: 0.286
### 24
    km03 - inercia: 5138.096 - silhouette: 0.703
    km05 - inercia: 2519.278 - silhouette: 0.491
    km10 - inercia: 851.231 - silhouette: 0.353
    km15 - inercia: 230.601 - silhouette: 0.251
    km20 - inercia: 80.098 - silhouette: 0.150
    km25 - inercia: 21.327 - silhouette: 0.074
## SeccionesXDistrito(codEleccion=e2017_paso,codCategoria=03)
### 01
    km03 - inercia: 132.253 - silhouette: 0.530
    km05 - inercia: 38.698 - silhouette: 0.525
    km10 - inercia: 3.788 - silhouette: 0.345
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 14506.409 - silhouette: 0.357
    km05 - inercia: 10200.812 - silhouette: 0.300
    km10 - inercia: 5922.065 - silhouette: 0.269
    km15 - inercia: 4226.475 - silhouette: 0.250
    km20 - inercia: 3352.131 - silhouette: 0.227
    km25 - inercia: 2593.150 - silhouette: 0.261
### 03
    km03 - inercia: 961.913 - silhouette: 0.406
    km05 - inercia: 420.521 - silhouette: 0.328
    km10 - inercia: 84.344 - silhouette: 0.226
    km15 - inercia: 5.699 - silhouette: 0.042
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 04
    km03 - inercia: 862.546 - silhouette: 0.468
    km05 - inercia: 446.317 - silhouette: 0.379
    km10 - inercia: 162.793 - silhouette: 0.320
    km15 - inercia: 70.736 - silhouette: 0.195
    km20 - inercia: 22.075 - silhouette: 0.117
    km25 - inercia: 1.952 - silhouette: 0.024
### 05
    km03 - inercia: 1553.269 - silhouette: 0.358
    km05 - inercia: 902.107 - silhouette: 0.365
    km10 - inercia: 326.358 - silhouette: 0.292
    km15 - inercia: 129.325 - silhouette: 0.205
    km20 - inercia: 39.263 - silhouette: 0.120
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 06
    km03 - inercia: 1051.818 - silhouette: 0.433
    km05 - inercia: 581.847 - silhouette: 0.380
    km10 - inercia: 195.096 - silhouette: 0.312
    km15 - inercia: 81.695 - silhouette: 0.213
    km20 - inercia: 23.360 - silhouette: 0.152
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 07
    km03 - inercia: 1223.551 - silhouette: 0.379
    km05 - inercia: 655.599 - silhouette: 0.318
    km10 - inercia: 131.118 - silhouette: 0.253
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 08
    km03 - inercia: 282.759 - silhouette: 0.387
    km05 - inercia: 150.267 - silhouette: 0.310
    km10 - inercia: 34.135 - silhouette: 0.203
    km15 - inercia: 3.369 - silhouette: 0.079
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 09
    km03 - inercia: 45.845 - silhouette: 0.493
    km05 - inercia: 17.519 - silhouette: 0.203
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 10
    km03 - inercia: 1388.340 - silhouette: 0.287
    km05 - inercia: 627.322 - silhouette: 0.323
    km10 - inercia: 194.966 - silhouette: 0.211
    km15 - inercia: 3.755 - silhouette: 0.078
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 11
    km03 - inercia: 1009.917 - silhouette: 0.449
    km05 - inercia: 397.266 - silhouette: 0.413
    km10 - inercia: 117.636 - silhouette: 0.286
    km15 - inercia: 28.783 - silhouette: 0.271
    km20 - inercia: 2.192 - silhouette: 0.102
    (n_samples=22 should be >= n_clusters=25.)
### 12
    km03 - inercia: 1414.175 - silhouette: 0.494
    km05 - inercia: 664.135 - silhouette: 0.455
    km10 - inercia: 135.299 - silhouette: 0.266
    km15 - inercia: 25.218 - silhouette: 0.106
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 13
    km03 - inercia: 811.797 - silhouette: 0.418
    km05 - inercia: 474.603 - silhouette: 0.309
    km10 - inercia: 145.669 - silhouette: 0.216
    km15 - inercia: 23.504 - silhouette: 0.141
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 14
    km03 - inercia: 1303.682 - silhouette: 0.367
    km05 - inercia: 591.157 - silhouette: 0.325
    km10 - inercia: 122.233 - silhouette: 0.292
    km15 - inercia: 13.274 - silhouette: 0.116
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 15
    km03 - inercia: 1319.047 - silhouette: 0.413
    km05 - inercia: 660.679 - silhouette: 0.370
    km10 - inercia: 118.246 - silhouette: 0.225
    km15 - inercia: 10.263 - silhouette: 0.048
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 16
    km03 - inercia: 639.247 - silhouette: 0.296
    km05 - inercia: 273.265 - silhouette: 0.253
    km10 - inercia: 33.899 - silhouette: 0.132
    (n_samples=13 should be >= n_clusters=15.)
    (n_samples=13 should be >= n_clusters=20.)
    (n_samples=13 should be >= n_clusters=25.)
### 17
    km03 - inercia: 2382.000 - silhouette: 0.468
    km05 - inercia: 1135.795 - silhouette: 0.367
    km10 - inercia: 397.008 - silhouette: 0.298
    km15 - inercia: 148.663 - silhouette: 0.251
    km20 - inercia: 24.284 - silhouette: 0.135
    (n_samples=23 should be >= n_clusters=25.)
### 18
    km03 - inercia: 573.800 - silhouette: 0.351
    km05 - inercia: 372.763 - silhouette: 0.253
    km10 - inercia: 124.241 - silhouette: 0.257
    km15 - inercia: 28.394 - silhouette: 0.178
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 82.709 - silhouette: 0.501
    km05 - inercia: 35.251 - silhouette: 0.252
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 134.355 - silhouette: 0.571
    km05 - inercia: 16.031 - silhouette: 0.355
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 21
    km03 - inercia: 640.211 - silhouette: 0.345
    km05 - inercia: 411.058 - silhouette: 0.310
    km10 - inercia: 137.529 - silhouette: 0.236
    km15 - inercia: 44.095 - silhouette: 0.115
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 22
    km03 - inercia: 1656.820 - silhouette: 0.445
    km05 - inercia: 732.544 - silhouette: 0.451
    km10 - inercia: 195.203 - silhouette: 0.403
    km15 - inercia: 61.973 - silhouette: 0.284
    km20 - inercia: 15.425 - silhouette: 0.191
    km25 - inercia: 1.505 - silhouette: 0.055
### 23
    km03 - inercia: 523.054 - silhouette: 0.543
    km05 - inercia: 210.912 - silhouette: 0.374
    km10 - inercia: 36.855 - silhouette: 0.232
    km15 - inercia: 6.393 - silhouette: 0.052
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 24
    (Number of labels is 3. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=3 should be >= n_clusters=5.)
    (n_samples=3 should be >= n_clusters=10.)
    (n_samples=3 should be >= n_clusters=15.)
    (n_samples=3 should be >= n_clusters=20.)
    (n_samples=3 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2017_paso,codCategoria=06)
### 01
    km03 - inercia: 5084.311 - silhouette: 0.444
    km05 - inercia: 2253.413 - silhouette: 0.453
    km10 - inercia: 801.287 - silhouette: 0.384
    km15 - inercia: 556.760 - silhouette: 0.307
    km20 - inercia: 408.880 - silhouette: 0.310
    km25 - inercia: 338.027 - silhouette: 0.298
### 02
    km03 - inercia: 135172.779 - silhouette: 0.349
    km05 - inercia: 95158.916 - silhouette: 0.290
    km10 - inercia: 65750.756 - silhouette: 0.242
    km15 - inercia: 49992.834 - silhouette: 0.218
    km20 - inercia: 41233.393 - silhouette: 0.229
    km25 - inercia: 35468.255 - silhouette: 0.214
### 03
    km03 - inercia: 37823.270 - silhouette: 0.401
    km05 - inercia: 24456.895 - silhouette: 0.368
    km10 - inercia: 15108.017 - silhouette: 0.281
    km15 - inercia: 9551.575 - silhouette: 0.307
    km20 - inercia: 6758.037 - silhouette: 0.312
    km25 - inercia: 5176.273 - silhouette: 0.297
### 13
    km03 - inercia: 33063.626 - silhouette: 0.403
    km05 - inercia: 22026.726 - silhouette: 0.361
    km10 - inercia: 13662.975 - silhouette: 0.302
    km15 - inercia: 9061.778 - silhouette: 0.300
    km20 - inercia: 7113.219 - silhouette: 0.309
    km25 - inercia: 5551.439 - silhouette: 0.305
## SeccionesXDistrito(codEleccion=e2017_paso,codCategoria=06)
### 01
    km03 - inercia: 131.841 - silhouette: 0.528
    km05 - inercia: 38.759 - silhouette: 0.527
    km10 - inercia: 3.741 - silhouette: 0.339
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 6316.313 - silhouette: 0.398
    km05 - inercia: 4308.936 - silhouette: 0.280
    km10 - inercia: 2118.610 - silhouette: 0.286
    km15 - inercia: 1359.529 - silhouette: 0.266
    km20 - inercia: 890.123 - silhouette: 0.239
    km25 - inercia: 589.456 - silhouette: 0.222
### 03
    km03 - inercia: 876.793 - silhouette: 0.428
    km05 - inercia: 436.186 - silhouette: 0.275
    km10 - inercia: 117.790 - silhouette: 0.122
    km15 - inercia: 4.932 - silhouette: 0.038
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 13
    km03 - inercia: 876.103 - silhouette: 0.407
    km05 - inercia: 541.590 - silhouette: 0.312
    km10 - inercia: 160.328 - silhouette: 0.245
    km15 - inercia: 20.562 - silhouette: 0.162
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2017_generales,codCategoria=03)
### 01
    km03 - inercia: 5576.606 - silhouette: 0.442
    km05 - inercia: 2440.037 - silhouette: 0.448
    km10 - inercia: 885.651 - silhouette: 0.382
    km15 - inercia: 608.938 - silhouette: 0.314
    km20 - inercia: 453.715 - silhouette: 0.298
    km25 - inercia: 361.565 - silhouette: 0.309
### 02
    km03 - inercia: 225851.083 - silhouette: 0.328
    km05 - inercia: 160148.429 - silhouette: 0.314
    km10 - inercia: 104028.534 - silhouette: 0.259
    km15 - inercia: 81905.762 - silhouette: 0.241
    km20 - inercia: 68803.275 - silhouette: 0.223
    km25 - inercia: 59790.078 - silhouette: 0.222
### 03
    km03 - inercia: 33067.158 - silhouette: 0.434
    km05 - inercia: 18752.164 - silhouette: 0.324
    km10 - inercia: 9359.964 - silhouette: 0.318
    km15 - inercia: 5690.909 - silhouette: 0.321
    km20 - inercia: 4256.290 - silhouette: 0.310
    km25 - inercia: 3161.708 - silhouette: 0.302
### 04
    km03 - inercia: 149026.759 - silhouette: 0.359
    km05 - inercia: 104568.033 - silhouette: 0.337
    km10 - inercia: 55310.062 - silhouette: 0.300
    km15 - inercia: 39010.049 - silhouette: 0.285
    km20 - inercia: 31082.322 - silhouette: 0.267
    km25 - inercia: 26181.988 - silhouette: 0.272
### 05
    km03 - inercia: 32850.461 - silhouette: 0.401
    km05 - inercia: 21678.619 - silhouette: 0.343
    km10 - inercia: 10271.506 - silhouette: 0.349
    km15 - inercia: 6515.781 - silhouette: 0.333
    km20 - inercia: 4649.659 - silhouette: 0.346
    km25 - inercia: 3659.223 - silhouette: 0.324
### 06
    km03 - inercia: 27979.355 - silhouette: 0.365
    km05 - inercia: 17857.741 - silhouette: 0.332
    km10 - inercia: 8817.525 - silhouette: 0.337
    km15 - inercia: 5903.634 - silhouette: 0.318
    km20 - inercia: 4264.327 - silhouette: 0.309
    km25 - inercia: 3162.356 - silhouette: 0.312
### 07
    km03 - inercia: 20509.166 - silhouette: 0.381
    km05 - inercia: 12936.080 - silhouette: 0.333
    km10 - inercia: 7410.684 - silhouette: 0.305
    km15 - inercia: 5096.558 - silhouette: 0.281
    km20 - inercia: 3663.046 - silhouette: 0.295
    km25 - inercia: 2889.345 - silhouette: 0.255
### 08
    km03 - inercia: 28127.016 - silhouette: 0.470
    km05 - inercia: 15983.968 - silhouette: 0.384
    km10 - inercia: 7994.221 - silhouette: 0.335
    km15 - inercia: 5660.287 - silhouette: 0.310
    km20 - inercia: 4515.515 - silhouette: 0.287
    km25 - inercia: 3861.462 - silhouette: 0.282
### 09
    km03 - inercia: 55390.794 - silhouette: 0.545
    km05 - inercia: 24804.318 - silhouette: 0.438
    km10 - inercia: 8098.029 - silhouette: 0.395
    km15 - inercia: 4278.407 - silhouette: 0.365
    km20 - inercia: 2659.063 - silhouette: 0.381
    km25 - inercia: 1989.616 - silhouette: 0.324
### 10
    km03 - inercia: 57929.804 - silhouette: 0.297
    km05 - inercia: 39295.507 - silhouette: 0.295
    km10 - inercia: 24871.364 - silhouette: 0.251
    km15 - inercia: 18259.364 - silhouette: 0.244
    km20 - inercia: 13856.323 - silhouette: 0.263
    km25 - inercia: 11101.135 - silhouette: 0.257
### 11
    km03 - inercia: 6840.746 - silhouette: 0.439
    km05 - inercia: 3386.591 - silhouette: 0.423
    km10 - inercia: 1409.773 - silhouette: 0.371
    km15 - inercia: 823.747 - silhouette: 0.351
    km20 - inercia: 583.708 - silhouette: 0.317
    km25 - inercia: 408.552 - silhouette: 0.244
### 12
    km03 - inercia: 26842.785 - silhouette: 0.452
    km05 - inercia: 16752.473 - silhouette: 0.408
    km10 - inercia: 7823.155 - silhouette: 0.357
    km15 - inercia: 5489.209 - silhouette: 0.304
    km20 - inercia: 3996.898 - silhouette: 0.294
    km25 - inercia: 2993.177 - silhouette: 0.285
### 13
    km03 - inercia: 32258.602 - silhouette: 0.402
    km05 - inercia: 18922.031 - silhouette: 0.383
    km10 - inercia: 10306.272 - silhouette: 0.340
    km15 - inercia: 7468.910 - silhouette: 0.347
    km20 - inercia: 5410.914 - silhouette: 0.352
    km25 - inercia: 4308.458 - silhouette: 0.309
### 14
    km03 - inercia: 22815.178 - silhouette: 0.452
    km05 - inercia: 13147.799 - silhouette: 0.426
    km10 - inercia: 6659.630 - silhouette: 0.334
    km15 - inercia: 4619.326 - silhouette: 0.312
    km20 - inercia: 3528.713 - silhouette: 0.264
    km25 - inercia: 2739.397 - silhouette: 0.275
### 15
    km03 - inercia: 52957.335 - silhouette: 0.363
    km05 - inercia: 30652.832 - silhouette: 0.355
    km10 - inercia: 17987.134 - silhouette: 0.280
    km15 - inercia: 13397.656 - silhouette: 0.258
    km20 - inercia: 10706.224 - silhouette: 0.232
    km25 - inercia: 8641.180 - silhouette: 0.272
### 16
    km03 - inercia: 30979.454 - silhouette: 0.457
    km05 - inercia: 16621.300 - silhouette: 0.344
    km10 - inercia: 8790.508 - silhouette: 0.299
    km15 - inercia: 6037.392 - silhouette: 0.280
    km20 - inercia: 4371.166 - silhouette: 0.301
    km25 - inercia: 3463.400 - silhouette: 0.282
### 17
    km03 - inercia: 70778.671 - silhouette: 0.437
    km05 - inercia: 46082.475 - silhouette: 0.346
    km10 - inercia: 26845.743 - silhouette: 0.330
    km15 - inercia: 20026.911 - silhouette: 0.300
    km20 - inercia: 16381.923 - silhouette: 0.294
    km25 - inercia: 13484.715 - silhouette: 0.274
### 18
    km03 - inercia: 10714.696 - silhouette: 0.463
    km05 - inercia: 5056.092 - silhouette: 0.433
    km10 - inercia: 2773.009 - silhouette: 0.330
    km15 - inercia: 2098.155 - silhouette: 0.278
    km20 - inercia: 1655.363 - silhouette: 0.277
    km25 - inercia: 1372.154 - silhouette: 0.259
### 19
    km03 - inercia: 58719.834 - silhouette: 0.455
    km05 - inercia: 35256.006 - silhouette: 0.404
    km10 - inercia: 13537.055 - silhouette: 0.378
    km15 - inercia: 7779.833 - silhouette: 0.360
    km20 - inercia: 5355.911 - silhouette: 0.320
    km25 - inercia: 3935.738 - silhouette: 0.332
### 20
    km03 - inercia: 11415.159 - silhouette: 0.314
    km05 - inercia: 6046.407 - silhouette: 0.332
    km10 - inercia: 2189.781 - silhouette: 0.256
    km15 - inercia: 903.981 - silhouette: 0.230
    km20 - inercia: 328.106 - silhouette: 0.232
    km25 - inercia: 117.063 - silhouette: 0.145
### 21
    km03 - inercia: 128730.519 - silhouette: 0.346
    km05 - inercia: 87856.204 - silhouette: 0.323
    km10 - inercia: 55912.137 - silhouette: 0.256
    km15 - inercia: 42568.151 - silhouette: 0.258
    km20 - inercia: 35017.905 - silhouette: 0.250
    km25 - inercia: 30834.924 - silhouette: 0.240
### 22
    km03 - inercia: 20800.091 - silhouette: 0.554
    km05 - inercia: 13300.031 - silhouette: 0.449
    km10 - inercia: 6435.281 - silhouette: 0.432
    km15 - inercia: 4652.755 - silhouette: 0.410
    km20 - inercia: 3339.120 - silhouette: 0.425
    km25 - inercia: 2673.004 - silhouette: 0.326
### 23
    km03 - inercia: 30863.952 - silhouette: 0.501
    km05 - inercia: 17643.834 - silhouette: 0.380
    km10 - inercia: 8155.305 - silhouette: 0.372
    km15 - inercia: 5123.874 - silhouette: 0.373
    km20 - inercia: 3956.792 - silhouette: 0.357
    km25 - inercia: 3172.336 - silhouette: 0.342
### 24
    km03 - inercia: 4476.705 - silhouette: 0.715
    km05 - inercia: 2096.995 - silhouette: 0.468
    km10 - inercia: 525.302 - silhouette: 0.424
    km15 - inercia: 151.031 - silhouette: 0.358
    km20 - inercia: 47.798 - silhouette: 0.257
    km25 - inercia: 10.587 - silhouette: 0.138
## SeccionesXDistrito(codEleccion=e2017_generales,codCategoria=03)
### 01
    km03 - inercia: 171.038 - silhouette: 0.544
    km05 - inercia: 50.251 - silhouette: 0.527
    km10 - inercia: 5.274 - silhouette: 0.336
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 12392.048 - silhouette: 0.409
    km05 - inercia: 7766.694 - silhouette: 0.335
    km10 - inercia: 4555.934 - silhouette: 0.313
    km15 - inercia: 3080.564 - silhouette: 0.301
    km20 - inercia: 2336.268 - silhouette: 0.281
    km25 - inercia: 1829.821 - silhouette: 0.280
### 03
    km03 - inercia: 599.920 - silhouette: 0.251
    km05 - inercia: 346.889 - silhouette: 0.237
    km10 - inercia: 82.991 - silhouette: 0.161
    km15 - inercia: 8.207 - silhouette: 0.041
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 04
    km03 - inercia: 743.728 - silhouette: 0.550
    km05 - inercia: 323.225 - silhouette: 0.417
    km10 - inercia: 124.547 - silhouette: 0.295
    km15 - inercia: 45.175 - silhouette: 0.239
    km20 - inercia: 12.615 - silhouette: 0.158
    km25 - inercia: 0.906 - silhouette: 0.038
### 05
    km03 - inercia: 1464.535 - silhouette: 0.387
    km05 - inercia: 801.965 - silhouette: 0.370
    km10 - inercia: 217.373 - silhouette: 0.340
    km15 - inercia: 78.359 - silhouette: 0.274
    km20 - inercia: 15.887 - silhouette: 0.206
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 06
    km03 - inercia: 1508.590 - silhouette: 0.308
    km05 - inercia: 673.852 - silhouette: 0.394
    km10 - inercia: 193.196 - silhouette: 0.375
    km15 - inercia: 59.352 - silhouette: 0.294
    km20 - inercia: 11.644 - silhouette: 0.209
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 07
    km03 - inercia: 1016.640 - silhouette: 0.345
    km05 - inercia: 474.656 - silhouette: 0.340
    km10 - inercia: 97.937 - silhouette: 0.230
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 08
    km03 - inercia: 245.060 - silhouette: 0.380
    km05 - inercia: 118.908 - silhouette: 0.406
    km10 - inercia: 16.852 - silhouette: 0.312
    km15 - inercia: 1.836 - silhouette: 0.088
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 09
    km03 - inercia: 48.184 - silhouette: 0.519
    km05 - inercia: 9.780 - silhouette: 0.329
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 10
    km03 - inercia: 2353.254 - silhouette: 0.275
    km05 - inercia: 1325.794 - silhouette: 0.279
    km10 - inercia: 272.368 - silhouette: 0.206
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 11
    km03 - inercia: 496.662 - silhouette: 0.528
    km05 - inercia: 244.515 - silhouette: 0.417
    km10 - inercia: 61.879 - silhouette: 0.358
    km15 - inercia: 15.968 - silhouette: 0.221
    km20 - inercia: 1.385 - silhouette: 0.083
    (n_samples=22 should be >= n_clusters=25.)
### 12
    km03 - inercia: 854.546 - silhouette: 0.476
    km05 - inercia: 322.774 - silhouette: 0.380
    km10 - inercia: 56.506 - silhouette: 0.305
    km15 - inercia: 11.431 - silhouette: 0.075
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 13
    km03 - inercia: 1149.934 - silhouette: 0.403
    km05 - inercia: 511.524 - silhouette: 0.399
    km10 - inercia: 104.623 - silhouette: 0.336
    km15 - inercia: 10.958 - silhouette: 0.169
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 14
    km03 - inercia: 1289.764 - silhouette: 0.371
    km05 - inercia: 646.407 - silhouette: 0.383
    km10 - inercia: 100.395 - silhouette: 0.329
    km15 - inercia: 8.261 - silhouette: 0.103
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 15
    km03 - inercia: 1116.278 - silhouette: 0.470
    km05 - inercia: 550.682 - silhouette: 0.357
    km10 - inercia: 122.392 - silhouette: 0.244
    km15 - inercia: 8.073 - silhouette: 0.053
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 16
    km03 - inercia: 285.652 - silhouette: 0.457
    km05 - inercia: 112.461 - silhouette: 0.366
    km10 - inercia: 5.915 - silhouette: 0.214
    (n_samples=13 should be >= n_clusters=15.)
    (n_samples=13 should be >= n_clusters=20.)
    (n_samples=13 should be >= n_clusters=25.)
### 17
    km03 - inercia: 3276.692 - silhouette: 0.362
    km05 - inercia: 1480.953 - silhouette: 0.391
    km10 - inercia: 450.597 - silhouette: 0.348
    km15 - inercia: 130.158 - silhouette: 0.271
    km20 - inercia: 16.689 - silhouette: 0.137
    (n_samples=23 should be >= n_clusters=25.)
### 18
    km03 - inercia: 394.328 - silhouette: 0.410
    km05 - inercia: 174.797 - silhouette: 0.385
    km10 - inercia: 53.252 - silhouette: 0.304
    km15 - inercia: 13.536 - silhouette: 0.196
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 275.664 - silhouette: 0.419
    km05 - inercia: 48.174 - silhouette: 0.423
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 110.549 - silhouette: 0.542
    km05 - inercia: 27.917 - silhouette: 0.332
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 21
    km03 - inercia: 644.351 - silhouette: 0.408
    km05 - inercia: 329.572 - silhouette: 0.346
    km10 - inercia: 102.174 - silhouette: 0.266
    km15 - inercia: 26.610 - silhouette: 0.130
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 22
    km03 - inercia: 1842.158 - silhouette: 0.457
    km05 - inercia: 745.754 - silhouette: 0.431
    km10 - inercia: 253.051 - silhouette: 0.363
    km15 - inercia: 64.445 - silhouette: 0.275
    km20 - inercia: 15.667 - silhouette: 0.218
    km25 - inercia: 1.123 - silhouette: 0.105
### 23
    km03 - inercia: 734.264 - silhouette: 0.491
    km05 - inercia: 312.709 - silhouette: 0.394
    km10 - inercia: 47.854 - silhouette: 0.244
    km15 - inercia: 3.427 - silhouette: 0.087
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 24
    (Number of labels is 3. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=3 should be >= n_clusters=5.)
    (n_samples=3 should be >= n_clusters=10.)
    (n_samples=3 should be >= n_clusters=15.)
    (n_samples=3 should be >= n_clusters=20.)
    (n_samples=3 should be >= n_clusters=25.)
## CircuitosXPais(codEleccion=e2019_paso,codCategoria=01)
    km03 - inercia: 1589333.208 - silhouette: 0.407
    km05 - inercia: 1176211.257 - silhouette: 0.330
    km10 - inercia: 721672.641 - silhouette: 0.324
    km15 - inercia: 536835.431 - silhouette: 0.280
    km20 - inercia: 452185.731 - silhouette: 0.250
    km25 - inercia: 389966.919 - silhouette: 0.237
## SeccionesXPais(codEleccion=e2019_paso,codCategoria=01)
    km03 - inercia: 68478.799 - silhouette: 0.381
    km05 - inercia: 47088.171 - silhouette: 0.346
    km10 - inercia: 24768.035 - silhouette: 0.289
    km15 - inercia: 18745.914 - silhouette: 0.269
    km20 - inercia: 14666.770 - silhouette: 0.260
    km25 - inercia: 11930.722 - silhouette: 0.259
## CircuitosXDistrito(codEleccion=e2019_paso,codCategoria=03)
### 01
    km03 - inercia: 8581.800 - silhouette: 0.503
    km05 - inercia: 3532.090 - silhouette: 0.486
    km10 - inercia: 1161.383 - silhouette: 0.422
    km15 - inercia: 736.241 - silhouette: 0.332
    km20 - inercia: 564.712 - silhouette: 0.321
    km25 - inercia: 465.459 - silhouette: 0.299
### 02
    km03 - inercia: 159902.409 - silhouette: 0.412
    km05 - inercia: 102039.748 - silhouette: 0.331
    km10 - inercia: 67240.603 - silhouette: 0.270
    km15 - inercia: 53757.044 - silhouette: 0.250
    km20 - inercia: 45703.119 - silhouette: 0.241
    km25 - inercia: 39948.466 - silhouette: 0.233
### 03
    km03 - inercia: 67511.001 - silhouette: 0.422
    km05 - inercia: 40865.933 - silhouette: 0.439
    km10 - inercia: 20550.511 - silhouette: 0.295
    km15 - inercia: 12246.105 - silhouette: 0.270
    km20 - inercia: 8318.879 - silhouette: 0.294
    km25 - inercia: 5749.622 - silhouette: 0.267
### 04
    km03 - inercia: 169174.294 - silhouette: 0.364
    km05 - inercia: 116051.860 - silhouette: 0.304
    km10 - inercia: 67316.680 - silhouette: 0.265
    km15 - inercia: 52676.348 - silhouette: 0.237
    km20 - inercia: 42714.352 - silhouette: 0.230
    km25 - inercia: 37175.931 - silhouette: 0.216
### 05
    km03 - inercia: 15605.383 - silhouette: 0.476
    km05 - inercia: 9170.824 - silhouette: 0.403
    km10 - inercia: 4518.667 - silhouette: 0.332
    km15 - inercia: 3095.829 - silhouette: 0.295
    km20 - inercia: 2191.353 - silhouette: 0.305
    km25 - inercia: 1776.012 - silhouette: 0.295
### 06
    km03 - inercia: 18327.734 - silhouette: 0.459
    km05 - inercia: 9182.914 - silhouette: 0.431
    km10 - inercia: 3965.203 - silhouette: 0.343
    km15 - inercia: 2749.039 - silhouette: 0.307
    km20 - inercia: 2018.400 - silhouette: 0.270
    km25 - inercia: 1547.549 - silhouette: 0.280
### 07
    km03 - inercia: 31372.751 - silhouette: 0.378
    km05 - inercia: 20791.708 - silhouette: 0.292
    km10 - inercia: 12039.615 - silhouette: 0.247
    km15 - inercia: 8993.967 - silhouette: 0.239
    km20 - inercia: 6745.005 - silhouette: 0.233
    km25 - inercia: 4945.451 - silhouette: 0.271
### 08
    km03 - inercia: 55042.156 - silhouette: 0.435
    km05 - inercia: 36729.613 - silhouette: 0.341
    km10 - inercia: 20947.078 - silhouette: 0.279
    km15 - inercia: 14486.104 - silhouette: 0.279
    km20 - inercia: 11467.793 - silhouette: 0.257
    km25 - inercia: 9459.223 - silhouette: 0.270
### 09
    km03 - inercia: 43679.667 - silhouette: 0.610
    km05 - inercia: 20234.405 - silhouette: 0.420
    km10 - inercia: 7956.086 - silhouette: 0.360
    km15 - inercia: 4964.925 - silhouette: 0.353
    km20 - inercia: 3441.210 - silhouette: 0.359
    km25 - inercia: 2498.070 - silhouette: 0.304
### 10
    km03 - inercia: 33168.571 - silhouette: 0.365
    km05 - inercia: 18508.002 - silhouette: 0.340
    km10 - inercia: 10227.201 - silhouette: 0.276
    km15 - inercia: 6417.657 - silhouette: 0.285
    km20 - inercia: 4768.650 - silhouette: 0.257
    km25 - inercia: 3855.481 - silhouette: 0.223
### 11
    km03 - inercia: 8212.298 - silhouette: 0.407
    km05 - inercia: 4793.733 - silhouette: 0.339
    km10 - inercia: 2336.533 - silhouette: 0.280
    km15 - inercia: 1577.108 - silhouette: 0.268
    km20 - inercia: 1113.725 - silhouette: 0.262
    km25 - inercia: 854.636 - silhouette: 0.238
### 12
    km03 - inercia: 47266.698 - silhouette: 0.566
    km05 - inercia: 19108.826 - silhouette: 0.449
    km10 - inercia: 8871.045 - silhouette: 0.370
    km15 - inercia: 5352.560 - silhouette: 0.355
    km20 - inercia: 3660.598 - silhouette: 0.319
    km25 - inercia: 2534.838 - silhouette: 0.301
### 13
    km03 - inercia: 35754.173 - silhouette: 0.556
    km05 - inercia: 14391.801 - silhouette: 0.427
    km10 - inercia: 7059.899 - silhouette: 0.331
    km15 - inercia: 4741.028 - silhouette: 0.314
    km20 - inercia: 3622.312 - silhouette: 0.306
    km25 - inercia: 2936.332 - silhouette: 0.276
### 14
    km03 - inercia: 22380.605 - silhouette: 0.368
    km05 - inercia: 12722.314 - silhouette: 0.331
    km10 - inercia: 6398.644 - silhouette: 0.346
    km15 - inercia: 4335.986 - silhouette: 0.298
    km20 - inercia: 3290.112 - silhouette: 0.277
    km25 - inercia: 2471.979 - silhouette: 0.270
### 15
    km03 - inercia: 55621.137 - silhouette: 0.345
    km05 - inercia: 36679.788 - silhouette: 0.336
    km10 - inercia: 19281.985 - silhouette: 0.322
    km15 - inercia: 13458.043 - silhouette: 0.274
    km20 - inercia: 10487.031 - silhouette: 0.269
    km25 - inercia: 8177.504 - silhouette: 0.276
### 16
    km03 - inercia: 43788.973 - silhouette: 0.339
    km05 - inercia: 31998.679 - silhouette: 0.291
    km10 - inercia: 18117.286 - silhouette: 0.249
    km15 - inercia: 12392.127 - silhouette: 0.261
    km20 - inercia: 9570.408 - silhouette: 0.253
    km25 - inercia: 7430.403 - silhouette: 0.238
### 17
    km03 - inercia: 55217.515 - silhouette: 0.421
    km05 - inercia: 32409.500 - silhouette: 0.355
    km10 - inercia: 17544.811 - silhouette: 0.309
    km15 - inercia: 12523.307 - silhouette: 0.322
    km20 - inercia: 9968.586 - silhouette: 0.286
    km25 - inercia: 8327.512 - silhouette: 0.284
### 18
    km03 - inercia: 13614.157 - silhouette: 0.566
    km05 - inercia: 5956.469 - silhouette: 0.469
    km10 - inercia: 2297.793 - silhouette: 0.407
    km15 - inercia: 1627.649 - silhouette: 0.328
    km20 - inercia: 1239.839 - silhouette: 0.314
    km25 - inercia: 973.367 - silhouette: 0.295
### 19
    km03 - inercia: 45811.488 - silhouette: 0.416
    km05 - inercia: 22528.548 - silhouette: 0.443
    km10 - inercia: 9843.894 - silhouette: 0.342
    km15 - inercia: 6207.678 - silhouette: 0.326
    km20 - inercia: 4733.453 - silhouette: 0.285
    km25 - inercia: 3729.115 - silhouette: 0.309
### 20
    km03 - inercia: 27720.393 - silhouette: 0.537
    km05 - inercia: 12461.171 - silhouette: 0.360
    km10 - inercia: 4559.043 - silhouette: 0.327
    km15 - inercia: 1300.853 - silhouette: 0.261
    km20 - inercia: 415.551 - silhouette: 0.186
    km25 - inercia: 107.814 - silhouette: 0.149
### 21
    km03 - inercia: 87855.233 - silhouette: 0.399
    km05 - inercia: 55096.208 - silhouette: 0.316
    km10 - inercia: 35672.920 - silhouette: 0.259
    km15 - inercia: 27822.905 - silhouette: 0.254
    km20 - inercia: 22952.636 - silhouette: 0.238
    km25 - inercia: 19776.070 - silhouette: 0.226
### 22
    km03 - inercia: 55131.209 - silhouette: 0.485
    km05 - inercia: 39591.176 - silhouette: 0.350
    km10 - inercia: 17693.630 - silhouette: 0.355
    km15 - inercia: 10913.767 - silhouette: 0.315
    km20 - inercia: 7469.073 - silhouette: 0.301
    km25 - inercia: 5892.000 - silhouette: 0.304
### 23
    km03 - inercia: 25820.402 - silhouette: 0.520
    km05 - inercia: 14738.348 - silhouette: 0.381
    km10 - inercia: 8228.834 - silhouette: 0.325
    km15 - inercia: 6150.634 - silhouette: 0.276
    km20 - inercia: 4873.563 - silhouette: 0.266
    km25 - inercia: 3901.849 - silhouette: 0.272
### 24
    km03 - inercia: 3134.803 - silhouette: 0.426
    km05 - inercia: 1545.895 - silhouette: 0.380
    km10 - inercia: 529.519 - silhouette: 0.352
    km15 - inercia: 190.712 - silhouette: 0.302
    km20 - inercia: 77.714 - silhouette: 0.176
    km25 - inercia: 17.332 - silhouette: 0.128
## SeccionesXDistrito(codEleccion=e2019_paso,codCategoria=03)
### 01
    km03 - inercia: 253.724 - silhouette: 0.564
    km05 - inercia: 68.157 - silhouette: 0.539
    km10 - inercia: 5.458 - silhouette: 0.271
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 6504.120 - silhouette: 0.403
    km05 - inercia: 3966.608 - silhouette: 0.354
    km10 - inercia: 2457.646 - silhouette: 0.262
    km15 - inercia: 1637.222 - silhouette: 0.253
    km20 - inercia: 1252.630 - silhouette: 0.274
    km25 - inercia: 977.431 - silhouette: 0.257
### 03
    km03 - inercia: 407.405 - silhouette: 0.397
    km05 - inercia: 211.755 - silhouette: 0.254
    km10 - inercia: 40.388 - silhouette: 0.149
    (n_samples=14 should be >= n_clusters=15.)
    (n_samples=14 should be >= n_clusters=20.)
    (n_samples=14 should be >= n_clusters=25.)
### 04
    km03 - inercia: 876.570 - silhouette: 0.517
    km05 - inercia: 506.309 - silhouette: 0.358
    km10 - inercia: 157.861 - silhouette: 0.306
    km15 - inercia: 73.205 - silhouette: 0.213
    km20 - inercia: 21.519 - silhouette: 0.113
    km25 - inercia: 0.989 - silhouette: 0.046
### 05
    km03 - inercia: 463.373 - silhouette: 0.469
    km05 - inercia: 217.403 - silhouette: 0.409
    km10 - inercia: 79.851 - silhouette: 0.249
    km15 - inercia: 31.793 - silhouette: 0.191
    km20 - inercia: 9.629 - silhouette: 0.110
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 06
    km03 - inercia: 434.249 - silhouette: 0.478
    km05 - inercia: 241.105 - silhouette: 0.352
    km10 - inercia: 84.098 - silhouette: 0.320
    km15 - inercia: 30.546 - silhouette: 0.261
    km20 - inercia: 4.530 - silhouette: 0.131
    (n_samples=23 should be >= n_clusters=25.)
### 07
    km03 - inercia: 1681.126 - silhouette: 0.331
    km05 - inercia: 968.786 - silhouette: 0.252
    km10 - inercia: 178.984 - silhouette: 0.203
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 08
    km03 - inercia: 152.631 - silhouette: 0.462
    km05 - inercia: 83.377 - silhouette: 0.425
    km10 - inercia: 16.314 - silhouette: 0.284
    km15 - inercia: 2.354 - silhouette: 0.048
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 09
    km03 - inercia: 62.253 - silhouette: 0.572
    km05 - inercia: 5.317 - silhouette: 0.442
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 10
    km03 - inercia: 945.079 - silhouette: 0.396
    km05 - inercia: 401.421 - silhouette: 0.292
    km10 - inercia: 58.466 - silhouette: 0.277
    km15 - inercia: 1.679 - silhouette: 0.082
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 11
    km03 - inercia: 565.248 - silhouette: 0.482
    km05 - inercia: 271.810 - silhouette: 0.451
    km10 - inercia: 66.806 - silhouette: 0.362
    km15 - inercia: 21.364 - silhouette: 0.224
    km20 - inercia: 3.602 - silhouette: 0.071
    (n_samples=22 should be >= n_clusters=25.)
### 12
    km03 - inercia: 338.633 - silhouette: 0.532
    km05 - inercia: 131.159 - silhouette: 0.443
    km10 - inercia: 29.509 - silhouette: 0.242
    km15 - inercia: 6.895 - silhouette: 0.103
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 13
    km03 - inercia: 540.865 - silhouette: 0.468
    km05 - inercia: 236.799 - silhouette: 0.482
    km10 - inercia: 50.963 - silhouette: 0.269
    km15 - inercia: 7.860 - silhouette: 0.194
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 14
    km03 - inercia: 1140.454 - silhouette: 0.289
    km05 - inercia: 600.196 - silhouette: 0.325
    km10 - inercia: 119.761 - silhouette: 0.252
    km15 - inercia: 19.197 - silhouette: 0.098
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 15
    km03 - inercia: 1195.748 - silhouette: 0.314
    km05 - inercia: 505.310 - silhouette: 0.404
    km10 - inercia: 80.619 - silhouette: 0.283
    km15 - inercia: 7.181 - silhouette: 0.043
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 16
    km03 - inercia: 622.212 - silhouette: 0.282
    km05 - inercia: 314.247 - silhouette: 0.287
    km10 - inercia: 46.226 - silhouette: 0.136
    (n_samples=13 should be >= n_clusters=15.)
    (n_samples=13 should be >= n_clusters=20.)
    (n_samples=13 should be >= n_clusters=25.)
### 17
    km03 - inercia: 1870.938 - silhouette: 0.438
    km05 - inercia: 753.635 - silhouette: 0.431
    km10 - inercia: 263.805 - silhouette: 0.336
    km15 - inercia: 86.072 - silhouette: 0.222
    km20 - inercia: 18.411 - silhouette: 0.126
    (n_samples=23 should be >= n_clusters=25.)
### 18
    km03 - inercia: 408.585 - silhouette: 0.492
    km05 - inercia: 145.020 - silhouette: 0.437
    km10 - inercia: 28.003 - silhouette: 0.309
    km15 - inercia: 7.175 - silhouette: 0.117
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 111.096 - silhouette: 0.635
    km05 - inercia: 25.853 - silhouette: 0.478
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 185.761 - silhouette: 0.341
    km05 - inercia: 56.703 - silhouette: 0.205
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 21
    km03 - inercia: 441.750 - silhouette: 0.443
    km05 - inercia: 216.707 - silhouette: 0.432
    km10 - inercia: 52.284 - silhouette: 0.368
    km15 - inercia: 9.501 - silhouette: 0.206
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 22
    km03 - inercia: 1583.480 - silhouette: 0.358
    km05 - inercia: 899.665 - silhouette: 0.328
    km10 - inercia: 349.002 - silhouette: 0.323
    km15 - inercia: 141.233 - silhouette: 0.235
    km20 - inercia: 34.880 - silhouette: 0.212
    km25 - inercia: 3.876 - silhouette: 0.095
### 23
    km03 - inercia: 488.832 - silhouette: 0.509
    km05 - inercia: 252.791 - silhouette: 0.390
    km10 - inercia: 38.787 - silhouette: 0.375
    km15 - inercia: 2.219 - silhouette: 0.167
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 24
    km03 - inercia: 90.368 - silhouette: 0.078
    (n_samples=4 should be >= n_clusters=5.)
    (n_samples=4 should be >= n_clusters=10.)
    (n_samples=4 should be >= n_clusters=15.)
    (n_samples=4 should be >= n_clusters=20.)
    (n_samples=4 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2019_paso,codCategoria=04)
### 02
    km03 - inercia: 161643.641 - silhouette: 0.422
    km05 - inercia: 107852.642 - silhouette: 0.333
    km10 - inercia: 66836.064 - silhouette: 0.269
    km15 - inercia: 52014.224 - silhouette: 0.240
    km20 - inercia: 43355.717 - silhouette: 0.243
    km25 - inercia: 37695.707 - silhouette: 0.226
### 03
    km03 - inercia: 57137.464 - silhouette: 0.404
    km05 - inercia: 33299.061 - silhouette: 0.349
    km10 - inercia: 16158.402 - silhouette: 0.322
    km15 - inercia: 10900.468 - silhouette: 0.293
    km20 - inercia: 7991.977 - silhouette: 0.265
    km25 - inercia: 5889.200 - silhouette: 0.268
## SeccionesXDistrito(codEleccion=e2019_paso,codCategoria=04)
### 02
    km03 - inercia: 6317.106 - silhouette: 0.418
    km05 - inercia: 3879.080 - silhouette: 0.365
    km10 - inercia: 2247.169 - silhouette: 0.293
    km15 - inercia: 1550.420 - silhouette: 0.259
    km20 - inercia: 1173.370 - silhouette: 0.261
    km25 - inercia: 925.937 - silhouette: 0.254
### 03
    km03 - inercia: 570.233 - silhouette: 0.381
    km05 - inercia: 265.966 - silhouette: 0.348
    km10 - inercia: 51.979 - silhouette: 0.193
    (n_samples=14 should be >= n_clusters=15.)
    (n_samples=14 should be >= n_clusters=20.)
    (n_samples=14 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2019_paso,codCategoria=06)
### 02
    km03 - inercia: 106429.529 - silhouette: 0.375
    km05 - inercia: 69872.657 - silhouette: 0.343
    km10 - inercia: 44708.031 - silhouette: 0.251
    km15 - inercia: 35846.832 - silhouette: 0.235
    km20 - inercia: 29423.285 - silhouette: 0.240
    km25 - inercia: 25412.936 - silhouette: 0.223
### 03
    km03 - inercia: 64491.369 - silhouette: 0.396
    km05 - inercia: 40670.872 - silhouette: 0.440
    km10 - inercia: 20427.442 - silhouette: 0.300
    km15 - inercia: 14378.220 - silhouette: 0.252
    km20 - inercia: 10032.164 - silhouette: 0.272
    km25 - inercia: 7690.643 - silhouette: 0.260
## SeccionesXDistrito(codEleccion=e2019_paso,codCategoria=06)
### 02
    km03 - inercia: 4422.844 - silhouette: 0.408
    km05 - inercia: 2459.812 - silhouette: 0.420
    km10 - inercia: 1150.389 - silhouette: 0.321
    km15 - inercia: 762.404 - silhouette: 0.283
    km20 - inercia: 552.646 - silhouette: 0.246
    km25 - inercia: 389.125 - silhouette: 0.263
### 03
    km03 - inercia: 924.707 - silhouette: 0.389
    km05 - inercia: 365.458 - silhouette: 0.312
    km10 - inercia: 65.279 - silhouette: 0.180
    (n_samples=14 should be >= n_clusters=15.)
    (n_samples=14 should be >= n_clusters=20.)
    (n_samples=14 should be >= n_clusters=25.)
## CircuitosXPais(codEleccion=e2019_generales,codCategoria=01)
    km03 - inercia: 1096079.616 - silhouette: 0.464
    km05 - inercia: 670030.311 - silhouette: 0.393
    km10 - inercia: 409335.089 - silhouette: 0.335
    km15 - inercia: 315961.445 - silhouette: 0.288
    km20 - inercia: 264641.900 - silhouette: 0.275
    km25 - inercia: 225489.577 - silhouette: 0.264
## SeccionesXPais(codEleccion=e2019_generales,codCategoria=01)
    km03 - inercia: 55096.380 - silhouette: 0.426
    km05 - inercia: 34602.485 - silhouette: 0.383
    km10 - inercia: 17320.731 - silhouette: 0.359
    km15 - inercia: 11950.927 - silhouette: 0.299
    km20 - inercia: 9160.441 - silhouette: 0.294
    km25 - inercia: 7746.228 - silhouette: 0.292
## CircuitosXDistrito(codEleccion=e2019_generales,codCategoria=03)
### 01
    km03 - inercia: 8505.559 - silhouette: 0.514
    km05 - inercia: 3560.070 - silhouette: 0.487
    km10 - inercia: 1142.356 - silhouette: 0.428
    km15 - inercia: 633.704 - silhouette: 0.393
    km20 - inercia: 456.394 - silhouette: 0.379
    km25 - inercia: 359.383 - silhouette: 0.365
### 02
    km03 - inercia: 150312.932 - silhouette: 0.452
    km05 - inercia: 87971.344 - silhouette: 0.382
    km10 - inercia: 55317.678 - silhouette: 0.294
    km15 - inercia: 43052.950 - silhouette: 0.264
    km20 - inercia: 34918.613 - silhouette: 0.280
    km25 - inercia: 29776.978 - silhouette: 0.253
### 03
    km03 - inercia: 31152.440 - silhouette: 0.345
    km05 - inercia: 21336.411 - silhouette: 0.331
    km10 - inercia: 10577.981 - silhouette: 0.315
    km15 - inercia: 6067.789 - silhouette: 0.338
    km20 - inercia: 4458.382 - silhouette: 0.309
    km25 - inercia: 3484.854 - silhouette: 0.288
### 04
    km03 - inercia: 132996.598 - silhouette: 0.413
    km05 - inercia: 91925.280 - silhouette: 0.302
    km10 - inercia: 53608.398 - silhouette: 0.282
    km15 - inercia: 39092.240 - silhouette: 0.253
    km20 - inercia: 32329.328 - silhouette: 0.252
    km25 - inercia: 28076.444 - silhouette: 0.233
### 05
    km03 - inercia: 18762.493 - silhouette: 0.523
    km05 - inercia: 9455.062 - silhouette: 0.517
    km10 - inercia: 2503.695 - silhouette: 0.444
    km15 - inercia: 1515.148 - silhouette: 0.366
    km20 - inercia: 1183.712 - silhouette: 0.318
    km25 - inercia: 931.239 - silhouette: 0.299
### 06
    km03 - inercia: 33859.892 - silhouette: 0.465
    km05 - inercia: 16193.686 - silhouette: 0.455
    km10 - inercia: 4218.500 - silhouette: 0.412
    km15 - inercia: 2490.201 - silhouette: 0.356
    km20 - inercia: 1668.334 - silhouette: 0.361
    km25 - inercia: 1263.403 - silhouette: 0.336
### 07
    km03 - inercia: 20944.095 - silhouette: 0.355
    km05 - inercia: 12821.562 - silhouette: 0.346
    km10 - inercia: 5762.094 - silhouette: 0.336
    km15 - inercia: 3940.934 - silhouette: 0.293
    km20 - inercia: 2843.691 - silhouette: 0.280
    km25 - inercia: 2131.994 - silhouette: 0.285
### 08
    km03 - inercia: 49122.230 - silhouette: 0.487
    km05 - inercia: 26987.840 - silhouette: 0.432
    km10 - inercia: 9422.263 - silhouette: 0.362
    km15 - inercia: 6743.491 - silhouette: 0.319
    km20 - inercia: 5150.309 - silhouette: 0.292
    km25 - inercia: 4439.387 - silhouette: 0.246
### 09
    km03 - inercia: 25475.227 - silhouette: 0.423
    km05 - inercia: 14048.981 - silhouette: 0.456
    km10 - inercia: 5880.762 - silhouette: 0.364
    km15 - inercia: 3524.551 - silhouette: 0.289
    km20 - inercia: 2382.625 - silhouette: 0.296
    km25 - inercia: 1617.758 - silhouette: 0.293
### 10
    km03 - inercia: 14891.925 - silhouette: 0.411
    km05 - inercia: 8148.690 - silhouette: 0.380
    km10 - inercia: 4541.596 - silhouette: 0.303
    km15 - inercia: 2811.250 - silhouette: 0.300
    km20 - inercia: 2209.101 - silhouette: 0.273
    km25 - inercia: 1651.038 - silhouette: 0.292
### 11
    km03 - inercia: 6063.923 - silhouette: 0.421
    km05 - inercia: 2899.324 - silhouette: 0.428
    km10 - inercia: 1503.346 - silhouette: 0.279
    km15 - inercia: 1013.998 - silhouette: 0.298
    km20 - inercia: 678.026 - silhouette: 0.309
    km25 - inercia: 473.132 - silhouette: 0.309
### 12
    km03 - inercia: 97907.612 - silhouette: 0.468
    km05 - inercia: 51835.709 - silhouette: 0.428
    km10 - inercia: 25788.089 - silhouette: 0.352
    km15 - inercia: 16146.916 - silhouette: 0.354
    km20 - inercia: 10738.030 - silhouette: 0.381
    km25 - inercia: 7894.140 - silhouette: 0.411
### 13
    km03 - inercia: 25362.806 - silhouette: 0.469
    km05 - inercia: 13589.342 - silhouette: 0.454
    km10 - inercia: 4306.534 - silhouette: 0.420
    km15 - inercia: 2673.298 - silhouette: 0.374
    km20 - inercia: 2031.706 - silhouette: 0.371
    km25 - inercia: 1553.368 - silhouette: 0.315
### 14
    km03 - inercia: 18406.353 - silhouette: 0.428
    km05 - inercia: 9826.197 - silhouette: 0.369
    km10 - inercia: 4999.590 - silhouette: 0.318
    km15 - inercia: 3244.122 - silhouette: 0.323
    km20 - inercia: 2197.655 - silhouette: 0.331
    km25 - inercia: 1683.070 - silhouette: 0.313
### 15
    km03 - inercia: 41993.286 - silhouette: 0.384
    km05 - inercia: 26310.331 - silhouette: 0.340
    km10 - inercia: 14306.302 - silhouette: 0.312
    km15 - inercia: 10544.577 - silhouette: 0.272
    km20 - inercia: 8330.235 - silhouette: 0.262
    km25 - inercia: 7133.794 - silhouette: 0.236
### 16
    km03 - inercia: 31597.463 - silhouette: 0.391
    km05 - inercia: 22022.159 - silhouette: 0.297
    km10 - inercia: 13169.878 - silhouette: 0.280
    km15 - inercia: 9652.246 - silhouette: 0.269
    km20 - inercia: 7360.374 - silhouette: 0.272
    km25 - inercia: 5928.526 - silhouette: 0.255
### 17
    km03 - inercia: 54739.632 - silhouette: 0.444
    km05 - inercia: 33031.251 - silhouette: 0.366
    km10 - inercia: 16945.575 - silhouette: 0.330
    km15 - inercia: 12582.282 - silhouette: 0.307
    km20 - inercia: 9604.856 - silhouette: 0.290
    km25 - inercia: 7999.270 - silhouette: 0.282
### 18
    km03 - inercia: 13761.362 - silhouette: 0.559
    km05 - inercia: 5035.177 - silhouette: 0.508
    km10 - inercia: 1906.751 - silhouette: 0.407
    km15 - inercia: 1154.064 - silhouette: 0.408
    km20 - inercia: 870.688 - silhouette: 0.354
    km25 - inercia: 708.605 - silhouette: 0.337
### 19
    km03 - inercia: 40512.779 - silhouette: 0.431
    km05 - inercia: 21785.490 - silhouette: 0.420
    km10 - inercia: 8837.886 - silhouette: 0.332
    km15 - inercia: 5580.609 - silhouette: 0.328
    km20 - inercia: 3863.851 - silhouette: 0.320
    km25 - inercia: 3045.831 - silhouette: 0.298
### 20
    km03 - inercia: 10451.478 - silhouette: 0.493
    km05 - inercia: 5251.103 - silhouette: 0.386
    km10 - inercia: 1747.024 - silhouette: 0.367
    km15 - inercia: 650.893 - silhouette: 0.348
    km20 - inercia: 214.482 - silhouette: 0.300
    km25 - inercia: 55.750 - silhouette: 0.229
### 21
    km03 - inercia: 80958.821 - silhouette: 0.410
    km05 - inercia: 51732.589 - silhouette: 0.397
    km10 - inercia: 25324.138 - silhouette: 0.338
    km15 - inercia: 17957.753 - silhouette: 0.288
    km20 - inercia: 13868.829 - silhouette: 0.296
    km25 - inercia: 11424.281 - silhouette: 0.294
### 22
    km03 - inercia: 45066.351 - silhouette: 0.468
    km05 - inercia: 26753.989 - silhouette: 0.391
    km10 - inercia: 13168.580 - silhouette: 0.344
    km15 - inercia: 9610.111 - silhouette: 0.312
    km20 - inercia: 7399.853 - silhouette: 0.321
    km25 - inercia: 5812.840 - silhouette: 0.322
### 23
    km03 - inercia: 22527.914 - silhouette: 0.553
    km05 - inercia: 10430.335 - silhouette: 0.449
    km10 - inercia: 5198.909 - silhouette: 0.330
    km15 - inercia: 3686.616 - silhouette: 0.305
    km20 - inercia: 3038.306 - silhouette: 0.274
    km25 - inercia: 2502.480 - silhouette: 0.270
### 24
    km03 - inercia: 3709.846 - silhouette: 0.765
    km05 - inercia: 1636.976 - silhouette: 0.442
    km10 - inercia: 507.968 - silhouette: 0.394
    km15 - inercia: 213.832 - silhouette: 0.230
    km20 - inercia: 68.145 - silhouette: 0.202
    km25 - inercia: 8.839 - silhouette: 0.114
## SeccionesXDistrito(codEleccion=e2019_generales,codCategoria=03)
### 01
    km03 - inercia: 263.039 - silhouette: 0.578
    km05 - inercia: 73.478 - silhouette: 0.529
    km10 - inercia: 6.298 - silhouette: 0.315
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 4898.402 - silhouette: 0.438
    km05 - inercia: 2363.453 - silhouette: 0.449
    km10 - inercia: 1209.478 - silhouette: 0.339
    km15 - inercia: 892.796 - silhouette: 0.302
    km20 - inercia: 699.668 - silhouette: 0.291
    km25 - inercia: 524.131 - silhouette: 0.285
### 03
    km03 - inercia: 335.422 - silhouette: 0.450
    km05 - inercia: 201.033 - silhouette: 0.260
    km10 - inercia: 46.950 - silhouette: 0.154
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 04
    km03 - inercia: 719.724 - silhouette: 0.531
    km05 - inercia: 362.938 - silhouette: 0.367
    km10 - inercia: 130.259 - silhouette: 0.275
    km15 - inercia: 51.651 - silhouette: 0.195
    km20 - inercia: 13.710 - silhouette: 0.155
    km25 - inercia: 0.792 - silhouette: 0.042
### 05
    km03 - inercia: 397.838 - silhouette: 0.534
    km05 - inercia: 177.667 - silhouette: 0.433
    km10 - inercia: 51.604 - silhouette: 0.386
    km15 - inercia: 17.746 - silhouette: 0.249
    km20 - inercia: 2.369 - silhouette: 0.219
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 06
    km03 - inercia: 348.087 - silhouette: 0.487
    km05 - inercia: 137.273 - silhouette: 0.460
    km10 - inercia: 48.629 - silhouette: 0.335
    km15 - inercia: 16.681 - silhouette: 0.288
    km20 - inercia: 3.855 - silhouette: 0.215
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 07
    km03 - inercia: 681.468 - silhouette: 0.450
    km05 - inercia: 310.013 - silhouette: 0.356
    km10 - inercia: 53.708 - silhouette: 0.214
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 08
    km03 - inercia: 145.418 - silhouette: 0.443
    km05 - inercia: 66.630 - silhouette: 0.397
    km10 - inercia: 10.543 - silhouette: 0.384
    km15 - inercia: 1.167 - silhouette: 0.135
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 09
    km03 - inercia: 21.528 - silhouette: 0.657
    km05 - inercia: 3.634 - silhouette: 0.486
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 10
    km03 - inercia: 439.695 - silhouette: 0.479
    km05 - inercia: 181.979 - silhouette: 0.347
    km10 - inercia: 44.057 - silhouette: 0.216
    km15 - inercia: 3.599 - silhouette: 0.057
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 11
    km03 - inercia: 509.399 - silhouette: 0.497
    km05 - inercia: 213.386 - silhouette: 0.467
    km10 - inercia: 74.082 - silhouette: 0.270
    km15 - inercia: 17.015 - silhouette: 0.235
    km20 - inercia: 1.336 - silhouette: 0.114
    (n_samples=22 should be >= n_clusters=25.)
### 12
    km03 - inercia: 3292.593 - silhouette: 0.421
    km05 - inercia: 1313.310 - silhouette: 0.446
    km10 - inercia: 363.965 - silhouette: 0.248
    km15 - inercia: 50.045 - silhouette: 0.146
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 13
    km03 - inercia: 386.463 - silhouette: 0.552
    km05 - inercia: 147.801 - silhouette: 0.503
    km10 - inercia: 30.267 - silhouette: 0.309
    km15 - inercia: 2.926 - silhouette: 0.175
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 14
    km03 - inercia: 1110.584 - silhouette: 0.361
    km05 - inercia: 502.784 - silhouette: 0.331
    km10 - inercia: 80.798 - silhouette: 0.304
    km15 - inercia: 8.396 - silhouette: 0.092
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 15
    km03 - inercia: 997.305 - silhouette: 0.478
    km05 - inercia: 387.844 - silhouette: 0.440
    km10 - inercia: 82.713 - silhouette: 0.314
    km15 - inercia: 0.866 - silhouette: 0.100
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 16
    km03 - inercia: 553.531 - silhouette: 0.351
    km05 - inercia: 209.648 - silhouette: 0.427
    km10 - inercia: 20.259 - silhouette: 0.211
    (n_samples=13 should be >= n_clusters=15.)
    (n_samples=13 should be >= n_clusters=20.)
    (n_samples=13 should be >= n_clusters=25.)
### 17
    km03 - inercia: 1658.616 - silhouette: 0.474
    km05 - inercia: 839.961 - silhouette: 0.370
    km10 - inercia: 246.997 - silhouette: 0.314
    km15 - inercia: 76.859 - silhouette: 0.239
    km20 - inercia: 9.582 - silhouette: 0.155
    (n_samples=23 should be >= n_clusters=25.)
### 18
    km03 - inercia: 465.666 - silhouette: 0.529
    km05 - inercia: 176.321 - silhouette: 0.439
    km10 - inercia: 23.171 - silhouette: 0.374
    km15 - inercia: 3.522 - silhouette: 0.215
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 165.642 - silhouette: 0.511
    km05 - inercia: 37.112 - silhouette: 0.409
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 124.903 - silhouette: 0.424
    km05 - inercia: 26.102 - silhouette: 0.166
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 21
    km03 - inercia: 447.265 - silhouette: 0.349
    km05 - inercia: 155.938 - silhouette: 0.491
    km10 - inercia: 29.747 - silhouette: 0.430
    km15 - inercia: 4.748 - silhouette: 0.229
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 22
    km03 - inercia: 1654.027 - silhouette: 0.370
    km05 - inercia: 840.207 - silhouette: 0.399
    km10 - inercia: 260.102 - silhouette: 0.399
    km15 - inercia: 87.335 - silhouette: 0.369
    km20 - inercia: 27.955 - silhouette: 0.268
    km25 - inercia: 6.265 - silhouette: 0.067
### 23
    km03 - inercia: 440.644 - silhouette: 0.528
    km05 - inercia: 178.395 - silhouette: 0.416
    km10 - inercia: 27.043 - silhouette: 0.386
    km15 - inercia: 0.515 - silhouette: 0.195
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 24
    km03 - inercia: 95.645 - silhouette: 0.044
    (n_samples=4 should be >= n_clusters=5.)
    (n_samples=4 should be >= n_clusters=10.)
    (n_samples=4 should be >= n_clusters=15.)
    (n_samples=4 should be >= n_clusters=20.)
    (n_samples=4 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2019_generales,codCategoria=04)
### 02
    km03 - inercia: 141696.757 - silhouette: 0.463
    km05 - inercia: 80338.841 - silhouette: 0.399
    km10 - inercia: 49404.814 - silhouette: 0.315
    km15 - inercia: 37868.591 - silhouette: 0.271
    km20 - inercia: 31037.263 - silhouette: 0.257
    km25 - inercia: 26895.856 - silhouette: 0.251
### 03
    km03 - inercia: 28209.029 - silhouette: 0.380
    km05 - inercia: 19590.263 - silhouette: 0.324
    km10 - inercia: 10370.523 - silhouette: 0.308
    km15 - inercia: 6800.019 - silhouette: 0.322
    km20 - inercia: 5154.502 - silhouette: 0.304
    km25 - inercia: 4036.308 - silhouette: 0.298
## SeccionesXDistrito(codEleccion=e2019_generales,codCategoria=04)
### 02
    km03 - inercia: 4735.375 - silhouette: 0.450
    km05 - inercia: 2225.980 - silhouette: 0.455
    km10 - inercia: 1164.952 - silhouette: 0.330
    km15 - inercia: 835.891 - silhouette: 0.313
    km20 - inercia: 631.738 - silhouette: 0.293
    km25 - inercia: 476.937 - silhouette: 0.299
### 03
    km03 - inercia: 447.270 - silhouette: 0.396
    km05 - inercia: 218.833 - silhouette: 0.350
    km10 - inercia: 36.758 - silhouette: 0.194
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2019_generales,codCategoria=06)
### 02
    km03 - inercia: 120909.931 - silhouette: 0.401
    km05 - inercia: 63316.153 - silhouette: 0.408
    km10 - inercia: 35719.693 - silhouette: 0.297
    km15 - inercia: 27619.400 - silhouette: 0.270
    km20 - inercia: 22812.206 - silhouette: 0.251
    km25 - inercia: 19076.242 - silhouette: 0.256
### 03
    km03 - inercia: 39751.940 - silhouette: 0.465
    km05 - inercia: 26508.165 - silhouette: 0.345
    km10 - inercia: 13274.478 - silhouette: 0.313
    km15 - inercia: 9018.045 - silhouette: 0.301
    km20 - inercia: 6552.640 - silhouette: 0.296
    km25 - inercia: 4761.610 - silhouette: 0.316
## SeccionesXDistrito(codEleccion=e2019_generales,codCategoria=06)
### 02
    km03 - inercia: 3520.714 - silhouette: 0.432
    km05 - inercia: 1747.476 - silhouette: 0.415
    km10 - inercia: 683.056 - silhouette: 0.321
    km15 - inercia: 427.821 - silhouette: 0.326
    km20 - inercia: 319.616 - silhouette: 0.293
    km25 - inercia: 227.482 - silhouette: 0.267
### 03
    km03 - inercia: 600.718 - silhouette: 0.506
    km05 - inercia: 255.364 - silhouette: 0.318
    km10 - inercia: 51.198 - silhouette: 0.225
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2021_paso,codCategoria=03)
### 01
    km03 - inercia: 5387.881 - silhouette: 0.503
    km05 - inercia: 2534.390 - silhouette: 0.469
    km10 - inercia: 1020.090 - silhouette: 0.357
    km15 - inercia: 633.918 - silhouette: 0.336
    km20 - inercia: 470.072 - silhouette: 0.317
    km25 - inercia: 379.183 - silhouette: 0.314
### 02
    km03 - inercia: 227263.477 - silhouette: 0.326
    km05 - inercia: 176653.457 - silhouette: 0.288
    km10 - inercia: 128502.061 - silhouette: 0.207
    km15 - inercia: 108389.462 - silhouette: 0.207
    km20 - inercia: 91544.365 - silhouette: 0.220
    km25 - inercia: 80766.552 - silhouette: 0.192
### 03
    km03 - inercia: 42486.838 - silhouette: 0.437
    km05 - inercia: 25957.235 - silhouette: 0.384
    km10 - inercia: 13915.941 - silhouette: 0.316
    km15 - inercia: 9860.033 - silhouette: 0.275
    km20 - inercia: 7374.854 - silhouette: 0.278
    km25 - inercia: 5922.209 - silhouette: 0.262
### 04
    km03 - inercia: 182595.275 - silhouette: 0.393
    km05 - inercia: 113250.987 - silhouette: 0.331
    km10 - inercia: 67872.709 - silhouette: 0.260
    km15 - inercia: 51959.721 - silhouette: 0.232
    km20 - inercia: 42992.988 - silhouette: 0.239
    km25 - inercia: 36646.050 - silhouette: 0.231
### 05
    km03 - inercia: 14838.703 - silhouette: 0.446
    km05 - inercia: 7482.582 - silhouette: 0.420
    km10 - inercia: 3678.879 - silhouette: 0.352
    km15 - inercia: 2438.762 - silhouette: 0.333
    km20 - inercia: 1617.231 - silhouette: 0.342
    km25 - inercia: 1254.825 - silhouette: 0.327
### 06
    km03 - inercia: 28718.868 - silhouette: 0.410
    km05 - inercia: 17925.848 - silhouette: 0.381
    km10 - inercia: 8284.570 - silhouette: 0.359
    km15 - inercia: 5503.596 - silhouette: 0.343
    km20 - inercia: 4154.420 - silhouette: 0.344
    km25 - inercia: 3240.789 - silhouette: 0.306
### 07
    km03 - inercia: 18055.775 - silhouette: 0.408
    km05 - inercia: 12269.856 - silhouette: 0.307
    km10 - inercia: 7340.698 - silhouette: 0.272
    km15 - inercia: 4910.655 - silhouette: 0.257
    km20 - inercia: 3840.845 - silhouette: 0.228
    km25 - inercia: 2875.113 - silhouette: 0.252
### 08
    km03 - inercia: 49041.168 - silhouette: 0.405
    km05 - inercia: 33076.201 - silhouette: 0.317
    km10 - inercia: 19537.836 - silhouette: 0.281
    km15 - inercia: 13014.135 - silhouette: 0.265
    km20 - inercia: 10464.239 - silhouette: 0.231
    km25 - inercia: 8907.247 - silhouette: 0.214
### 09
    km03 - inercia: 77794.778 - silhouette: 0.309
    km05 - inercia: 49048.804 - silhouette: 0.318
    km10 - inercia: 21074.427 - silhouette: 0.286
    km15 - inercia: 13309.229 - silhouette: 0.294
    km20 - inercia: 8827.753 - silhouette: 0.304
    km25 - inercia: 6518.806 - silhouette: 0.294
### 10
    km03 - inercia: 24905.517 - silhouette: 0.333
    km05 - inercia: 15455.457 - silhouette: 0.340
    km10 - inercia: 7701.904 - silhouette: 0.335
    km15 - inercia: 4814.760 - silhouette: 0.303
    km20 - inercia: 3254.169 - silhouette: 0.318
    km25 - inercia: 2310.544 - silhouette: 0.311
### 11
    km03 - inercia: 11913.577 - silhouette: 0.465
    km05 - inercia: 6400.558 - silhouette: 0.477
    km10 - inercia: 2377.315 - silhouette: 0.328
    km15 - inercia: 1565.160 - silhouette: 0.252
    km20 - inercia: 1138.000 - silhouette: 0.239
    km25 - inercia: 892.166 - silhouette: 0.232
### 12
    km03 - inercia: 23693.713 - silhouette: 0.368
    km05 - inercia: 16493.119 - silhouette: 0.361
    km10 - inercia: 8395.705 - silhouette: 0.328
    km15 - inercia: 5747.246 - silhouette: 0.285
    km20 - inercia: 4687.378 - silhouette: 0.264
    km25 - inercia: 3758.741 - silhouette: 0.247
### 13
    km03 - inercia: 42048.318 - silhouette: 0.330
    km05 - inercia: 31287.462 - silhouette: 0.317
    km10 - inercia: 17412.032 - silhouette: 0.274
    km15 - inercia: 13135.300 - silhouette: 0.267
    km20 - inercia: 10724.892 - silhouette: 0.242
    km25 - inercia: 8579.916 - silhouette: 0.264
### 14
    km03 - inercia: 19387.585 - silhouette: 0.392
    km05 - inercia: 10989.378 - silhouette: 0.371
    km10 - inercia: 5735.882 - silhouette: 0.375
    km15 - inercia: 3245.493 - silhouette: 0.363
    km20 - inercia: 2311.770 - silhouette: 0.346
    km25 - inercia: 1709.561 - silhouette: 0.316
### 15
    km03 - inercia: 26152.945 - silhouette: 0.395
    km05 - inercia: 17747.356 - silhouette: 0.346
    km10 - inercia: 10501.941 - silhouette: 0.272
    km15 - inercia: 7282.153 - silhouette: 0.258
    km20 - inercia: 5945.418 - silhouette: 0.241
    km25 - inercia: 4817.508 - silhouette: 0.240
### 16
    km03 - inercia: 37233.820 - silhouette: 0.331
    km05 - inercia: 23791.690 - silhouette: 0.305
    km10 - inercia: 11873.830 - silhouette: 0.319
    km15 - inercia: 7500.238 - silhouette: 0.315
    km20 - inercia: 5881.050 - silhouette: 0.291
    km25 - inercia: 4769.449 - silhouette: 0.275
### 17
    km03 - inercia: 108917.951 - silhouette: 0.467
    km05 - inercia: 69300.664 - silhouette: 0.338
    km10 - inercia: 42477.541 - silhouette: 0.298
    km15 - inercia: 29600.168 - silhouette: 0.279
    km20 - inercia: 23294.391 - silhouette: 0.260
    km25 - inercia: 19059.942 - silhouette: 0.267
### 18
    km03 - inercia: 13126.813 - silhouette: 0.476
    km05 - inercia: 6266.966 - silhouette: 0.436
    km10 - inercia: 3498.951 - silhouette: 0.365
    km15 - inercia: 2488.437 - silhouette: 0.284
    km20 - inercia: 1912.471 - silhouette: 0.288
    km25 - inercia: 1439.814 - silhouette: 0.288
### 19
    km03 - inercia: 32586.990 - silhouette: 0.434
    km05 - inercia: 22624.085 - silhouette: 0.321
    km10 - inercia: 11579.431 - silhouette: 0.279
    km15 - inercia: 7982.439 - silhouette: 0.262
    km20 - inercia: 6359.173 - silhouette: 0.274
    km25 - inercia: 4743.266 - silhouette: 0.293
### 20
    km03 - inercia: 10327.391 - silhouette: 0.303
    km05 - inercia: 6352.416 - silhouette: 0.277
    km10 - inercia: 2578.355 - silhouette: 0.285
    km15 - inercia: 1054.155 - silhouette: 0.246
    km20 - inercia: 470.827 - silhouette: 0.219
    km25 - inercia: 148.354 - silhouette: 0.160
### 21
    km03 - inercia: 143919.051 - silhouette: 0.274
    km05 - inercia: 103358.971 - silhouette: 0.292
    km10 - inercia: 57864.459 - silhouette: 0.260
    km15 - inercia: 45841.704 - silhouette: 0.238
    km20 - inercia: 39284.805 - silhouette: 0.221
    km25 - inercia: 33313.527 - silhouette: 0.221
### 22
    km03 - inercia: 53757.617 - silhouette: 0.425
    km05 - inercia: 33394.416 - silhouette: 0.429
    km10 - inercia: 18793.608 - silhouette: 0.323
    km15 - inercia: 13617.195 - silhouette: 0.261
    km20 - inercia: 10096.292 - silhouette: 0.266
    km25 - inercia: 8383.751 - silhouette: 0.242
### 23
    km03 - inercia: 35441.766 - silhouette: 0.474
    km05 - inercia: 17620.779 - silhouette: 0.421
    km10 - inercia: 8995.967 - silhouette: 0.368
    km15 - inercia: 6056.236 - silhouette: 0.330
    km20 - inercia: 4692.791 - silhouette: 0.329
    km25 - inercia: 3983.811 - silhouette: 0.311
### 24
    km03 - inercia: 4310.606 - silhouette: 0.566
    km05 - inercia: 2389.926 - silhouette: 0.431
    km10 - inercia: 679.005 - silhouette: 0.302
    km15 - inercia: 135.360 - silhouette: 0.282
    km20 - inercia: 51.586 - silhouette: 0.143
    km25 - inercia: 11.547 - silhouette: 0.065
## SeccionesXDistrito(codEleccion=e2021_paso,codCategoria=03)
### 01
    km03 - inercia: 171.902 - silhouette: 0.592
    km05 - inercia: 54.575 - silhouette: 0.509
    km10 - inercia: 6.084 - silhouette: 0.310
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 11189.951 - silhouette: 0.337
    km05 - inercia: 7921.691 - silhouette: 0.336
    km10 - inercia: 4449.530 - silhouette: 0.271
    km15 - inercia: 3356.583 - silhouette: 0.241
    km20 - inercia: 2789.490 - silhouette: 0.219
    km25 - inercia: 2288.653 - silhouette: 0.200
### 03
    km03 - inercia: 1079.221 - silhouette: 0.490
    km05 - inercia: 371.855 - silhouette: 0.443
    km10 - inercia: 60.541 - silhouette: 0.258
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 04
    km03 - inercia: 1032.082 - silhouette: 0.533
    km05 - inercia: 542.802 - silhouette: 0.317
    km10 - inercia: 163.763 - silhouette: 0.319
    km15 - inercia: 56.448 - silhouette: 0.249
    km20 - inercia: 22.529 - silhouette: 0.093
    km25 - inercia: 0.991 - silhouette: 0.030
### 05
    km03 - inercia: 351.308 - silhouette: 0.451
    km05 - inercia: 147.005 - silhouette: 0.449
    km10 - inercia: 49.516 - silhouette: 0.377
    km15 - inercia: 15.029 - silhouette: 0.359
    km20 - inercia: 2.855 - silhouette: 0.211
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 06
    km03 - inercia: 1170.703 - silhouette: 0.414
    km05 - inercia: 622.246 - silhouette: 0.384
    km10 - inercia: 211.932 - silhouette: 0.313
    km15 - inercia: 90.417 - silhouette: 0.191
    km20 - inercia: 21.077 - silhouette: 0.155
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 07
    km03 - inercia: 692.924 - silhouette: 0.268
    km05 - inercia: 405.234 - silhouette: 0.242
    km10 - inercia: 87.568 - silhouette: 0.137
    (n_samples=14 should be >= n_clusters=15.)
    (n_samples=14 should be >= n_clusters=20.)
    (n_samples=14 should be >= n_clusters=25.)
### 08
    km03 - inercia: 393.109 - silhouette: 0.405
    km05 - inercia: 206.130 - silhouette: 0.360
    km10 - inercia: 36.756 - silhouette: 0.296
    km15 - inercia: 2.663 - silhouette: 0.137
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 09
    km03 - inercia: 236.252 - silhouette: 0.302
    km05 - inercia: 62.033 - silhouette: 0.321
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 10
    km03 - inercia: 1351.117 - silhouette: 0.363
    km05 - inercia: 443.532 - silhouette: 0.444
    km10 - inercia: 84.627 - silhouette: 0.269
    km15 - inercia: 2.795 - silhouette: 0.071
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 11
    km03 - inercia: 795.342 - silhouette: 0.477
    km05 - inercia: 319.148 - silhouette: 0.474
    km10 - inercia: 89.597 - silhouette: 0.318
    km15 - inercia: 21.185 - silhouette: 0.257
    km20 - inercia: 3.345 - silhouette: 0.071
    (n_samples=22 should be >= n_clusters=25.)
### 12
    km03 - inercia: 844.826 - silhouette: 0.415
    km05 - inercia: 356.068 - silhouette: 0.388
    km10 - inercia: 116.322 - silhouette: 0.286
    km15 - inercia: 20.843 - silhouette: 0.128
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 13
    km03 - inercia: 1991.520 - silhouette: 0.413
    km05 - inercia: 795.121 - silhouette: 0.406
    km10 - inercia: 194.174 - silhouette: 0.278
    km15 - inercia: 20.545 - silhouette: 0.121
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 14
    km03 - inercia: 516.065 - silhouette: 0.400
    km05 - inercia: 294.908 - silhouette: 0.259
    km10 - inercia: 62.353 - silhouette: 0.292
    km15 - inercia: 9.894 - silhouette: 0.074
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 15
    km03 - inercia: 1065.729 - silhouette: 0.506
    km05 - inercia: 563.247 - silhouette: 0.426
    km10 - inercia: 126.712 - silhouette: 0.151
    km15 - inercia: 12.922 - silhouette: 0.023
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 16
    km03 - inercia: 385.540 - silhouette: 0.398
    km05 - inercia: 177.341 - silhouette: 0.312
    km10 - inercia: 28.600 - silhouette: 0.155
    (n_samples=13 should be >= n_clusters=15.)
    (n_samples=13 should be >= n_clusters=20.)
    (n_samples=13 should be >= n_clusters=25.)
### 17
    km03 - inercia: 3571.527 - silhouette: 0.364
    km05 - inercia: 1876.923 - silhouette: 0.385
    km10 - inercia: 622.846 - silhouette: 0.264
    km15 - inercia: 208.007 - silhouette: 0.192
    km20 - inercia: 45.989 - silhouette: 0.063
    (n_samples=23 should be >= n_clusters=25.)
### 18
    km03 - inercia: 590.611 - silhouette: 0.491
    km05 - inercia: 287.441 - silhouette: 0.385
    km10 - inercia: 80.071 - silhouette: 0.212
    km15 - inercia: 13.164 - silhouette: 0.138
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 209.426 - silhouette: 0.463
    km05 - inercia: 44.021 - silhouette: 0.415
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 265.597 - silhouette: 0.423
    km05 - inercia: 73.400 - silhouette: 0.304
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 21
    km03 - inercia: 566.107 - silhouette: 0.324
    km05 - inercia: 368.827 - silhouette: 0.210
    km10 - inercia: 128.015 - silhouette: 0.188
    km15 - inercia: 37.465 - silhouette: 0.085
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 22
    km03 - inercia: 2883.710 - silhouette: 0.406
    km05 - inercia: 1639.999 - silhouette: 0.389
    km10 - inercia: 428.624 - silhouette: 0.436
    km15 - inercia: 126.944 - silhouette: 0.381
    km20 - inercia: 43.838 - silhouette: 0.258
    km25 - inercia: 2.568 - silhouette: 0.084
### 23
    km03 - inercia: 873.360 - silhouette: 0.526
    km05 - inercia: 447.009 - silhouette: 0.381
    km10 - inercia: 59.437 - silhouette: 0.291
    km15 - inercia: 3.307 - silhouette: 0.052
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 24
    km03 - inercia: 34.181 - silhouette: 0.237
    (n_samples=4 should be >= n_clusters=5.)
    (n_samples=4 should be >= n_clusters=10.)
    (n_samples=4 should be >= n_clusters=15.)
    (n_samples=4 should be >= n_clusters=20.)
    (n_samples=4 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2021_paso,codCategoria=06)
### 01
    km03 - inercia: 5551.215 - silhouette: 0.499
    km05 - inercia: 2578.127 - silhouette: 0.473
    km10 - inercia: 1028.984 - silhouette: 0.359
    km15 - inercia: 673.304 - silhouette: 0.311
    km20 - inercia: 516.579 - silhouette: 0.290
    km25 - inercia: 416.817 - silhouette: 0.267
### 02
    km03 - inercia: 103468.121 - silhouette: 0.343
    km05 - inercia: 76224.839 - silhouette: 0.284
    km10 - inercia: 51904.849 - silhouette: 0.253
    km15 - inercia: 41016.194 - silhouette: 0.222
    km20 - inercia: 35157.661 - silhouette: 0.216
    km25 - inercia: 29848.160 - silhouette: 0.210
### 03
    km03 - inercia: 47902.689 - silhouette: 0.414
    km05 - inercia: 33043.836 - silhouette: 0.435
    km10 - inercia: 17007.101 - silhouette: 0.339
    km15 - inercia: 11888.562 - silhouette: 0.291
    km20 - inercia: 8888.045 - silhouette: 0.276
    km25 - inercia: 6636.438 - silhouette: 0.271
### 06
    km03 - inercia: 26953.660 - silhouette: 0.412
    km05 - inercia: 16952.625 - silhouette: 0.378
    km10 - inercia: 7298.838 - silhouette: 0.376
    km15 - inercia: 4875.554 - silhouette: 0.357
    km20 - inercia: 3529.550 - silhouette: 0.320
    km25 - inercia: 2723.154 - silhouette: 0.320
### 13
    km03 - inercia: 41539.875 - silhouette: 0.331
    km05 - inercia: 30840.399 - silhouette: 0.304
    km10 - inercia: 17800.463 - silhouette: 0.293
    km15 - inercia: 13214.026 - silhouette: 0.264
    km20 - inercia: 10343.676 - silhouette: 0.272
    km25 - inercia: 8514.814 - silhouette: 0.229
## SeccionesXDistrito(codEleccion=e2021_paso,codCategoria=06)
### 01
    km03 - inercia: 192.795 - silhouette: 0.551
    km05 - inercia: 55.957 - silhouette: 0.498
    km10 - inercia: 6.201 - silhouette: 0.294
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 5103.148 - silhouette: 0.356
    km05 - inercia: 3556.822 - silhouette: 0.276
    km10 - inercia: 2093.048 - silhouette: 0.264
    km15 - inercia: 1398.744 - silhouette: 0.242
    km20 - inercia: 941.381 - silhouette: 0.223
    km25 - inercia: 599.610 - silhouette: 0.239
### 03
    km03 - inercia: 1090.670 - silhouette: 0.480
    km05 - inercia: 400.985 - silhouette: 0.441
    km10 - inercia: 72.702 - silhouette: 0.222
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 06
    km03 - inercia: 1056.554 - silhouette: 0.423
    km05 - inercia: 562.746 - silhouette: 0.352
    km10 - inercia: 211.697 - silhouette: 0.247
    km15 - inercia: 69.996 - silhouette: 0.227
    km20 - inercia: 15.871 - silhouette: 0.160
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 13
    km03 - inercia: 1908.491 - silhouette: 0.397
    km05 - inercia: 827.997 - silhouette: 0.382
    km10 - inercia: 207.369 - silhouette: 0.216
    km15 - inercia: 29.714 - silhouette: 0.082
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2021_generales,codCategoria=03)
### 01
    km03 - inercia: 5196.146 - silhouette: 0.495
    km05 - inercia: 2436.149 - silhouette: 0.464
    km10 - inercia: 1021.164 - silhouette: 0.359
    km15 - inercia: 653.986 - silhouette: 0.340
    km20 - inercia: 474.718 - silhouette: 0.327
    km25 - inercia: 381.359 - silhouette: 0.322
### 02
    km03 - inercia: 232327.791 - silhouette: 0.349
    km05 - inercia: 148041.017 - silhouette: 0.314
    km10 - inercia: 94806.454 - silhouette: 0.262
    km15 - inercia: 76663.397 - silhouette: 0.246
    km20 - inercia: 63950.658 - silhouette: 0.231
    km25 - inercia: 57372.677 - silhouette: 0.208
### 03
    km03 - inercia: 37675.350 - silhouette: 0.417
    km05 - inercia: 24528.897 - silhouette: 0.327
    km10 - inercia: 13723.652 - silhouette: 0.290
    km15 - inercia: 9093.821 - silhouette: 0.255
    km20 - inercia: 6956.045 - silhouette: 0.280
    km25 - inercia: 5519.393 - silhouette: 0.247
### 04
    km03 - inercia: 147737.081 - silhouette: 0.426
    km05 - inercia: 89296.058 - silhouette: 0.332
    km10 - inercia: 50708.733 - silhouette: 0.300
    km15 - inercia: 37065.338 - silhouette: 0.277
    km20 - inercia: 30124.374 - silhouette: 0.266
    km25 - inercia: 24448.789 - silhouette: 0.273
### 05
    km03 - inercia: 28965.724 - silhouette: 0.442
    km05 - inercia: 12812.047 - silhouette: 0.425
    km10 - inercia: 4743.346 - silhouette: 0.419
    km15 - inercia: 2938.441 - silhouette: 0.350
    km20 - inercia: 2009.588 - silhouette: 0.333
    km25 - inercia: 1440.771 - silhouette: 0.291
### 06
    km03 - inercia: 22502.499 - silhouette: 0.408
    km05 - inercia: 14514.343 - silhouette: 0.363
    km10 - inercia: 6966.864 - silhouette: 0.372
    km15 - inercia: 4026.118 - silhouette: 0.364
    km20 - inercia: 2745.671 - silhouette: 0.375
    km25 - inercia: 2177.668 - silhouette: 0.348
### 07
    km03 - inercia: 19801.133 - silhouette: 0.375
    km05 - inercia: 13818.664 - silhouette: 0.311
    km10 - inercia: 8118.432 - silhouette: 0.285
    km15 - inercia: 5250.883 - silhouette: 0.307
    km20 - inercia: 3954.136 - silhouette: 0.280
    km25 - inercia: 3032.980 - silhouette: 0.275
### 08
    km03 - inercia: 43831.993 - silhouette: 0.434
    km05 - inercia: 25072.373 - silhouette: 0.383
    km10 - inercia: 13049.941 - silhouette: 0.351
    km15 - inercia: 8964.665 - silhouette: 0.289
    km20 - inercia: 7164.993 - silhouette: 0.265
    km25 - inercia: 5951.382 - silhouette: 0.257
### 09
    km03 - inercia: 54401.259 - silhouette: 0.516
    km05 - inercia: 18912.384 - silhouette: 0.483
    km10 - inercia: 8470.723 - silhouette: 0.370
    km15 - inercia: 4921.104 - silhouette: 0.391
    km20 - inercia: 3594.728 - silhouette: 0.339
    km25 - inercia: 2668.353 - silhouette: 0.337
### 10
    km03 - inercia: 21643.345 - silhouette: 0.335
    km05 - inercia: 13537.254 - silhouette: 0.353
    km10 - inercia: 6790.631 - silhouette: 0.345
    km15 - inercia: 4632.259 - silhouette: 0.315
    km20 - inercia: 3163.824 - silhouette: 0.317
    km25 - inercia: 2220.902 - silhouette: 0.344
### 11
    km03 - inercia: 10501.509 - silhouette: 0.454
    km05 - inercia: 4575.650 - silhouette: 0.458
    km10 - inercia: 1722.629 - silhouette: 0.306
    km15 - inercia: 1078.927 - silhouette: 0.304
    km20 - inercia: 743.967 - silhouette: 0.260
    km25 - inercia: 569.659 - silhouette: 0.248
### 12
    km03 - inercia: 38098.097 - silhouette: 0.429
    km05 - inercia: 22544.163 - silhouette: 0.397
    km10 - inercia: 9874.598 - silhouette: 0.397
    km15 - inercia: 7116.501 - silhouette: 0.328
    km20 - inercia: 5503.310 - silhouette: 0.291
    km25 - inercia: 4363.516 - silhouette: 0.283
### 13
    km03 - inercia: 37222.912 - silhouette: 0.361
    km05 - inercia: 26359.873 - silhouette: 0.316
    km10 - inercia: 15243.689 - silhouette: 0.308
    km15 - inercia: 10850.008 - silhouette: 0.300
    km20 - inercia: 7846.694 - silhouette: 0.292
    km25 - inercia: 6490.168 - silhouette: 0.301
### 14
    km03 - inercia: 16503.362 - silhouette: 0.367
    km05 - inercia: 9175.060 - silhouette: 0.386
    km10 - inercia: 4303.406 - silhouette: 0.337
    km15 - inercia: 2825.493 - silhouette: 0.347
    km20 - inercia: 1896.612 - silhouette: 0.327
    km25 - inercia: 1466.826 - silhouette: 0.297
### 15
    km03 - inercia: 40197.184 - silhouette: 0.335
    km05 - inercia: 27507.297 - silhouette: 0.283
    km10 - inercia: 16555.518 - silhouette: 0.283
    km15 - inercia: 11822.348 - silhouette: 0.287
    km20 - inercia: 9000.379 - silhouette: 0.267
    km25 - inercia: 6970.778 - silhouette: 0.262
### 16
    km03 - inercia: 33832.396 - silhouette: 0.372
    km05 - inercia: 21096.933 - silhouette: 0.341
    km10 - inercia: 11330.550 - silhouette: 0.335
    km15 - inercia: 7257.848 - silhouette: 0.320
    km20 - inercia: 5499.792 - silhouette: 0.317
    km25 - inercia: 4503.715 - silhouette: 0.297
### 17
    km03 - inercia: 71292.604 - silhouette: 0.355
    km05 - inercia: 53638.864 - silhouette: 0.294
    km10 - inercia: 31545.297 - silhouette: 0.290
    km15 - inercia: 22712.393 - silhouette: 0.266
    km20 - inercia: 18662.314 - silhouette: 0.264
    km25 - inercia: 15519.096 - silhouette: 0.268
### 18
    km03 - inercia: 12446.310 - silhouette: 0.521
    km05 - inercia: 5321.361 - silhouette: 0.460
    km10 - inercia: 2810.447 - silhouette: 0.351
    km15 - inercia: 1943.025 - silhouette: 0.337
    km20 - inercia: 1405.711 - silhouette: 0.340
    km25 - inercia: 1128.104 - silhouette: 0.323
### 19
    km03 - inercia: 53942.031 - silhouette: 0.304
    km05 - inercia: 36005.515 - silhouette: 0.342
    km10 - inercia: 18725.435 - silhouette: 0.306
    km15 - inercia: 13197.742 - silhouette: 0.266
    km20 - inercia: 9738.301 - silhouette: 0.261
    km25 - inercia: 7844.361 - silhouette: 0.245
### 20
    km03 - inercia: 7905.394 - silhouette: 0.286
    km05 - inercia: 4907.810 - silhouette: 0.323
    km10 - inercia: 2173.400 - silhouette: 0.283
    km15 - inercia: 1128.483 - silhouette: 0.225
    km20 - inercia: 465.274 - silhouette: 0.195
    km25 - inercia: 115.695 - silhouette: 0.119
### 21
    km03 - inercia: 156920.219 - silhouette: 0.273
    km05 - inercia: 108332.921 - silhouette: 0.295
    km10 - inercia: 59619.985 - silhouette: 0.277
    km15 - inercia: 43787.668 - silhouette: 0.243
    km20 - inercia: 33808.666 - silhouette: 0.269
    km25 - inercia: 29148.893 - silhouette: 0.256
### 22
    km03 - inercia: 46415.138 - silhouette: 0.501
    km05 - inercia: 31143.986 - silhouette: 0.454
    km10 - inercia: 17514.634 - silhouette: 0.425
    km15 - inercia: 11194.460 - silhouette: 0.353
    km20 - inercia: 8063.473 - silhouette: 0.366
    km25 - inercia: 6428.553 - silhouette: 0.307
### 23
    km03 - inercia: 37539.717 - silhouette: 0.433
    km05 - inercia: 22390.151 - silhouette: 0.375
    km10 - inercia: 11768.049 - silhouette: 0.338
    km15 - inercia: 8350.254 - silhouette: 0.323
    km20 - inercia: 6204.125 - silhouette: 0.314
    km25 - inercia: 4957.537 - silhouette: 0.308
### 24
    km03 - inercia: 4218.762 - silhouette: 0.422
    km05 - inercia: 1846.901 - silhouette: 0.442
    km10 - inercia: 540.949 - silhouette: 0.354
    km15 - inercia: 185.530 - silhouette: 0.296
    km20 - inercia: 50.942 - silhouette: 0.220
    km25 - inercia: 15.340 - silhouette: 0.079
## SeccionesXDistrito(codEleccion=e2021_generales,codCategoria=03)
### 01
    km03 - inercia: 169.067 - silhouette: 0.586
    km05 - inercia: 56.242 - silhouette: 0.527
    km10 - inercia: 4.839 - silhouette: 0.338
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 9313.627 - silhouette: 0.345
    km05 - inercia: 6440.292 - silhouette: 0.287
    km10 - inercia: 3879.889 - silhouette: 0.255
    km15 - inercia: 2739.149 - silhouette: 0.265
    km20 - inercia: 2124.930 - silhouette: 0.260
    km25 - inercia: 1751.546 - silhouette: 0.243
### 03
    km03 - inercia: 963.197 - silhouette: 0.380
    km05 - inercia: 454.269 - silhouette: 0.326
    km10 - inercia: 85.664 - silhouette: 0.271
    km15 - inercia: 4.165 - silhouette: 0.071
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 04
    km03 - inercia: 1018.956 - silhouette: 0.527
    km05 - inercia: 521.982 - silhouette: 0.342
    km10 - inercia: 149.575 - silhouette: 0.349
    km15 - inercia: 45.972 - silhouette: 0.267
    km20 - inercia: 10.296 - silhouette: 0.170
    km25 - inercia: 0.530 - silhouette: 0.041
### 05
    km03 - inercia: 1109.238 - silhouette: 0.495
    km05 - inercia: 347.610 - silhouette: 0.420
    km10 - inercia: 92.023 - silhouette: 0.380
    km15 - inercia: 27.876 - silhouette: 0.279
    km20 - inercia: 7.301 - silhouette: 0.145
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 06
    km03 - inercia: 693.320 - silhouette: 0.479
    km05 - inercia: 407.958 - silhouette: 0.293
    km10 - inercia: 132.418 - silhouette: 0.304
    km15 - inercia: 44.503 - silhouette: 0.221
    km20 - inercia: 9.215 - silhouette: 0.135
    (n_samples=24 should be >= n_clusters=25.)
### 07
    km03 - inercia: 913.571 - silhouette: 0.318
    km05 - inercia: 460.817 - silhouette: 0.298
    km10 - inercia: 128.438 - silhouette: 0.110
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 08
    km03 - inercia: 277.662 - silhouette: 0.480
    km05 - inercia: 120.709 - silhouette: 0.412
    km10 - inercia: 22.943 - silhouette: 0.263
    km15 - inercia: 2.091 - silhouette: 0.085
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 09
    km03 - inercia: 139.532 - silhouette: 0.466
    km05 - inercia: 36.151 - silhouette: 0.294
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 10
    km03 - inercia: 757.047 - silhouette: 0.381
    km05 - inercia: 365.242 - silhouette: 0.349
    km10 - inercia: 57.657 - silhouette: 0.301
    km15 - inercia: 1.407 - silhouette: 0.077
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 11
    km03 - inercia: 583.796 - silhouette: 0.511
    km05 - inercia: 299.764 - silhouette: 0.469
    km10 - inercia: 65.511 - silhouette: 0.473
    km15 - inercia: 11.373 - silhouette: 0.389
    km20 - inercia: 1.475 - silhouette: 0.079
    (n_samples=22 should be >= n_clusters=25.)
### 12
    km03 - inercia: 1784.187 - silhouette: 0.488
    km05 - inercia: 627.345 - silhouette: 0.446
    km10 - inercia: 87.429 - silhouette: 0.326
    km15 - inercia: 11.758 - silhouette: 0.120
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 13
    km03 - inercia: 1831.983 - silhouette: 0.429
    km05 - inercia: 710.913 - silhouette: 0.435
    km10 - inercia: 151.243 - silhouette: 0.338
    km15 - inercia: 12.667 - silhouette: 0.132
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 14
    km03 - inercia: 510.931 - silhouette: 0.379
    km05 - inercia: 225.416 - silhouette: 0.350
    km10 - inercia: 47.642 - silhouette: 0.296
    km15 - inercia: 3.042 - silhouette: 0.125
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 15
    km03 - inercia: 1520.464 - silhouette: 0.429
    km05 - inercia: 773.996 - silhouette: 0.302
    km10 - inercia: 115.259 - silhouette: 0.299
    km15 - inercia: 12.667 - silhouette: 0.068
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 16
    km03 - inercia: 386.353 - silhouette: 0.353
    km05 - inercia: 179.687 - silhouette: 0.336
    km10 - inercia: 19.950 - silhouette: 0.197
    (n_samples=13 should be >= n_clusters=15.)
    (n_samples=13 should be >= n_clusters=20.)
    (n_samples=13 should be >= n_clusters=25.)
### 17
    km03 - inercia: 2660.168 - silhouette: 0.298
    km05 - inercia: 1491.068 - silhouette: 0.355
    km10 - inercia: 454.299 - silhouette: 0.315
    km15 - inercia: 188.379 - silhouette: 0.133
    km20 - inercia: 29.964 - silhouette: 0.093
    (n_samples=23 should be >= n_clusters=25.)
### 18
    km03 - inercia: 479.345 - silhouette: 0.522
    km05 - inercia: 251.067 - silhouette: 0.402
    km10 - inercia: 66.448 - silhouette: 0.223
    km15 - inercia: 13.828 - silhouette: 0.112
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 330.693 - silhouette: 0.350
    km05 - inercia: 136.391 - silhouette: 0.215
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 239.078 - silhouette: 0.392
    km05 - inercia: 54.079 - silhouette: 0.249
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 21
    km03 - inercia: 544.684 - silhouette: 0.337
    km05 - inercia: 278.020 - silhouette: 0.340
    km10 - inercia: 96.155 - silhouette: 0.139
    km15 - inercia: 24.620 - silhouette: 0.073
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 22
    km03 - inercia: 2499.520 - silhouette: 0.486
    km05 - inercia: 1270.409 - silhouette: 0.465
    km10 - inercia: 363.185 - silhouette: 0.376
    km15 - inercia: 100.326 - silhouette: 0.344
    km20 - inercia: 27.615 - silhouette: 0.218
    km25 - inercia: 3.343 - silhouette: 0.037
### 23
    km03 - inercia: 715.047 - silhouette: 0.496
    km05 - inercia: 365.356 - silhouette: 0.436
    km10 - inercia: 65.836 - silhouette: 0.240
    km15 - inercia: 7.292 - silhouette: 0.046
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 24
    km03 - inercia: 34.921 - silhouette: 0.224
    (n_samples=4 should be >= n_clusters=5.)
    (n_samples=4 should be >= n_clusters=10.)
    (n_samples=4 should be >= n_clusters=15.)
    (n_samples=4 should be >= n_clusters=20.)
    (n_samples=4 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2021_generales,codCategoria=04)
### 22
    km03 - inercia: 43873.954 - silhouette: 0.517
    km05 - inercia: 30234.842 - silhouette: 0.414
    km10 - inercia: 16640.607 - silhouette: 0.352
    km15 - inercia: 10690.216 - silhouette: 0.373
    km20 - inercia: 8041.383 - silhouette: 0.300
    km25 - inercia: 6205.716 - silhouette: 0.310
## SeccionesXDistrito(codEleccion=e2021_generales,codCategoria=04)
### 22
    km03 - inercia: 2693.774 - silhouette: 0.472
    km05 - inercia: 1357.560 - silhouette: 0.462
    km10 - inercia: 404.093 - silhouette: 0.361
    km15 - inercia: 126.308 - silhouette: 0.380
    km20 - inercia: 29.916 - silhouette: 0.261
    km25 - inercia: 1.824 - silhouette: 0.066
## CircuitosXDistrito(codEleccion=e2021_generales,codCategoria=06)
### 01
    km03 - inercia: 5424.905 - silhouette: 0.487
    km05 - inercia: 2503.569 - silhouette: 0.455
    km10 - inercia: 1064.096 - silhouette: 0.357
    km15 - inercia: 680.489 - silhouette: 0.348
    km20 - inercia: 522.692 - silhouette: 0.333
    km25 - inercia: 398.773 - silhouette: 0.298
### 02
    km03 - inercia: 86638.158 - silhouette: 0.360
    km05 - inercia: 64773.587 - silhouette: 0.278
    km10 - inercia: 39562.397 - silhouette: 0.279
    km15 - inercia: 31759.899 - silhouette: 0.258
    km20 - inercia: 26510.411 - silhouette: 0.257
    km25 - inercia: 22203.373 - silhouette: 0.259
### 03
    km03 - inercia: 41077.283 - silhouette: 0.401
    km05 - inercia: 26465.929 - silhouette: 0.351
    km10 - inercia: 14819.379 - silhouette: 0.285
    km15 - inercia: 11032.976 - silhouette: 0.241
    km20 - inercia: 8567.197 - silhouette: 0.233
    km25 - inercia: 6368.332 - silhouette: 0.277
### 06
    km03 - inercia: 21478.701 - silhouette: 0.411
    km05 - inercia: 13824.593 - silhouette: 0.366
    km10 - inercia: 6908.941 - silhouette: 0.354
    km15 - inercia: 3979.694 - silhouette: 0.350
    km20 - inercia: 2796.759 - silhouette: 0.359
    km25 - inercia: 2187.227 - silhouette: 0.343
### 12
    km03 - inercia: 28279.003 - silhouette: 0.484
    km05 - inercia: 16026.744 - silhouette: 0.464
    km10 - inercia: 8894.620 - silhouette: 0.340
    km15 - inercia: 5450.736 - silhouette: 0.356
    km20 - inercia: 3875.916 - silhouette: 0.347
    km25 - inercia: 2831.422 - silhouette: 0.280
### 13
    km03 - inercia: 44891.872 - silhouette: 0.326
    km05 - inercia: 32941.023 - silhouette: 0.363
    km10 - inercia: 17688.058 - silhouette: 0.326
    km15 - inercia: 12015.270 - silhouette: 0.309
    km20 - inercia: 9254.288 - silhouette: 0.294
    km25 - inercia: 7277.354 - silhouette: 0.275
### 22
    km03 - inercia: 127608.136 - silhouette: 0.367
    km05 - inercia: 84417.940 - silhouette: 0.305
    km10 - inercia: 47563.454 - silhouette: 0.330
    km15 - inercia: 34588.371 - silhouette: 0.324
    km20 - inercia: 27332.729 - silhouette: 0.304
    km25 - inercia: 22871.381 - silhouette: 0.315
## SeccionesXDistrito(codEleccion=e2021_generales,codCategoria=06)
### 01
    km03 - inercia: 191.168 - silhouette: 0.589
    km05 - inercia: 54.597 - silhouette: 0.502
    km10 - inercia: 4.426 - silhouette: 0.369
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 4394.699 - silhouette: 0.363
    km05 - inercia: 3153.576 - silhouette: 0.283
    km10 - inercia: 1914.377 - silhouette: 0.251
    km15 - inercia: 1225.851 - silhouette: 0.249
    km20 - inercia: 808.948 - silhouette: 0.281
    km25 - inercia: 501.087 - silhouette: 0.293
### 03
    km03 - inercia: 1141.125 - silhouette: 0.274
    km05 - inercia: 591.061 - silhouette: 0.239
    km10 - inercia: 120.361 - silhouette: 0.212
    km15 - inercia: 4.779 - silhouette: 0.065
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 06
    km03 - inercia: 670.261 - silhouette: 0.475
    km05 - inercia: 390.203 - silhouette: 0.329
    km10 - inercia: 130.666 - silhouette: 0.315
    km15 - inercia: 42.247 - silhouette: 0.239
    km20 - inercia: 10.783 - silhouette: 0.089
    (n_samples=24 should be >= n_clusters=25.)
### 12
    km03 - inercia: 1931.970 - silhouette: 0.319
    km05 - inercia: 535.920 - silhouette: 0.178
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 13
    km03 - inercia: 2416.201 - silhouette: 0.380
    km05 - inercia: 1121.438 - silhouette: 0.427
    km10 - inercia: 220.880 - silhouette: 0.309
    km15 - inercia: 26.136 - silhouette: 0.161
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 22
    km03 - inercia: 4891.945 - silhouette: 0.286
    km05 - inercia: 3021.786 - silhouette: 0.289
    km10 - inercia: 1198.192 - silhouette: 0.305
    km15 - inercia: 453.534 - silhouette: 0.238
    km20 - inercia: 112.898 - silhouette: 0.218
    km25 - inercia: 15.353 - silhouette: 0.097
## CircuitosXDistrito(codEleccion=e2021_jujuy,codCategoria=06)
### 10
    km03 - inercia: 77664.633 - silhouette: 0.275
    km05 - inercia: 57160.353 - silhouette: 0.302
    km10 - inercia: 33283.031 - silhouette: 0.249
    km15 - inercia: 23407.867 - silhouette: 0.219
    km20 - inercia: 18423.885 - silhouette: 0.211
    km25 - inercia: 15373.554 - silhouette: 0.191
## SeccionesXDistrito(codEleccion=e2021_jujuy,codCategoria=06)
### 10
    km03 - inercia: 1626.881 - silhouette: 0.380
    km05 - inercia: 866.172 - silhouette: 0.250
    km10 - inercia: 220.163 - silhouette: 0.218
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2023_neuquen,codCategoria=04)
### 15
    km03 - inercia: 27180.581 - silhouette: 0.151
    km05 - inercia: 22546.880 - silhouette: 0.161
    km10 - inercia: 15881.363 - silhouette: 0.152
    km15 - inercia: 12165.034 - silhouette: 0.140
    km20 - inercia: 9896.314 - silhouette: 0.140
    km25 - inercia: 7802.823 - silhouette: 0.151
## SeccionesXDistrito(codEleccion=e2023_neuquen,codCategoria=04)
### 15
    km03 - inercia: 1445.546 - silhouette: 0.293
    km05 - inercia: 760.008 - silhouette: 0.250
    km10 - inercia: 244.212 - silhouette: 0.164
    km15 - inercia: 27.090 - silhouette: 0.049
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2023_neuquen,codCategoria=06)
### 15
    km03 - inercia: 30105.747 - silhouette: 0.155
    km05 - inercia: 25104.072 - silhouette: 0.157
    km10 - inercia: 16602.531 - silhouette: 0.159
    km15 - inercia: 13513.199 - silhouette: 0.142
    km20 - inercia: 10845.268 - silhouette: 0.142
    km25 - inercia: 8821.213 - silhouette: 0.136
## SeccionesXDistrito(codEleccion=e2023_neuquen,codCategoria=06)
### 15
    km03 - inercia: 1651.015 - silhouette: 0.243
    km05 - inercia: 896.932 - silhouette: 0.209
    km10 - inercia: 245.091 - silhouette: 0.164
    km15 - inercia: 34.436 - silhouette: 0.020
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2023_rionegro,codCategoria=04)
### 16
    km03 - inercia: 51099.268 - silhouette: 0.336
    km05 - inercia: 38761.064 - silhouette: 0.252
    km10 - inercia: 24426.222 - silhouette: 0.229
    km15 - inercia: 17427.797 - silhouette: 0.215
    km20 - inercia: 13034.586 - silhouette: 0.203
    km25 - inercia: 9769.293 - silhouette: 0.221
## SeccionesXDistrito(codEleccion=e2023_rionegro,codCategoria=04)
### 16
    km03 - inercia: 326.069 - silhouette: 0.145
    km05 - inercia: 124.943 - silhouette: 0.123
    (n_samples=8 should be >= n_clusters=10.)
    (n_samples=8 should be >= n_clusters=15.)
    (n_samples=8 should be >= n_clusters=20.)
    (n_samples=8 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2023_rionegro,codCategoria=06)
### 16
    km03 - inercia: 73649.134 - silhouette: 0.292
    km05 - inercia: 55298.870 - silhouette: 0.263
    km10 - inercia: 32555.289 - silhouette: 0.282
    km15 - inercia: 23713.183 - silhouette: 0.254
    km20 - inercia: 17967.622 - silhouette: 0.201
    km25 - inercia: 14406.060 - silhouette: 0.193
## SeccionesXDistrito(codEleccion=e2023_rionegro,codCategoria=06)
### 16
    km03 - inercia: 463.231 - silhouette: 0.121
    km05 - inercia: 206.578 - silhouette: 0.036
    (n_samples=8 should be >= n_clusters=10.)
    (n_samples=8 should be >= n_clusters=15.)
    (n_samples=8 should be >= n_clusters=20.)
    (n_samples=8 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2023_jujuy,codCategoria=04)
### 10
    km03 - inercia: 62059.677 - silhouette: 0.321
    km05 - inercia: 47929.222 - silhouette: 0.235
    km10 - inercia: 29304.959 - silhouette: 0.254
    km15 - inercia: 19323.934 - silhouette: 0.240
    km20 - inercia: 14488.379 - silhouette: 0.259
    km25 - inercia: 10793.648 - silhouette: 0.267
## SeccionesXDistrito(codEleccion=e2023_jujuy,codCategoria=04)
### 10
    km03 - inercia: 1777.331 - silhouette: 0.250
    km05 - inercia: 1009.078 - silhouette: 0.278
    km10 - inercia: 281.732 - silhouette: 0.188
    km15 - inercia: 23.703 - silhouette: 0.009
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2023_jujuy,codCategoria=06)
### 10
    km03 - inercia: 51985.506 - silhouette: 0.272
    km05 - inercia: 33511.931 - silhouette: 0.280
    km10 - inercia: 17242.216 - silhouette: 0.275
    km15 - inercia: 10210.950 - silhouette: 0.277
    km20 - inercia: 7094.620 - silhouette: 0.239
    km25 - inercia: 5072.451 - silhouette: 0.254
## SeccionesXDistrito(codEleccion=e2023_jujuy,codCategoria=06)
### 10
    km03 - inercia: 1614.295 - silhouette: 0.182
    km05 - inercia: 770.370 - silhouette: 0.198
    km10 - inercia: 106.576 - silhouette: 0.078
    (n_samples=12 should be >= n_clusters=15.)
    (n_samples=12 should be >= n_clusters=20.)
    (n_samples=12 should be >= n_clusters=25.)
## CircuitosXPais(codEleccion=e2023_paso,codCategoria=01)
    km03 - inercia: 3072845.666 - silhouette: 0.300
    km05 - inercia: 2147393.892 - silhouette: 0.316
    km10 - inercia: 1318349.665 - silhouette: 0.278
    km15 - inercia: 1042875.125 - silhouette: 0.253
    km20 - inercia: 873704.986 - silhouette: 0.237
    km25 - inercia: 770814.171 - silhouette: 0.228
## SeccionesXPais(codEleccion=e2023_paso,codCategoria=01)
    km03 - inercia: 127318.493 - silhouette: 0.328
    km05 - inercia: 83471.584 - silhouette: 0.341
    km10 - inercia: 47368.665 - silhouette: 0.273
    km15 - inercia: 35013.394 - silhouette: 0.262
    km20 - inercia: 28390.434 - silhouette: 0.270
    km25 - inercia: 24119.201 - silhouette: 0.255
## CircuitosXDistrito(codEleccion=e2023_paso,codCategoria=03)
### 01
    km03 - inercia: 6471.769 - silhouette: 0.466
    km05 - inercia: 3189.635 - silhouette: 0.423
    km10 - inercia: 1354.703 - silhouette: 0.385
    km15 - inercia: 858.994 - silhouette: 0.376
    km20 - inercia: 611.166 - silhouette: 0.367
    km25 - inercia: 449.924 - silhouette: 0.374
### 02
    km03 - inercia: 234496.921 - silhouette: 0.267
    km05 - inercia: 175845.534 - silhouette: 0.289
    km10 - inercia: 117527.802 - silhouette: 0.242
    km15 - inercia: 93638.277 - silhouette: 0.245
    km20 - inercia: 77724.597 - silhouette: 0.230
    km25 - inercia: 68754.594 - silhouette: 0.214
### 03
    km03 - inercia: 156131.210 - silhouette: 0.333
    km05 - inercia: 82233.664 - silhouette: 0.379
    km10 - inercia: 35667.736 - silhouette: 0.332
    km15 - inercia: 23380.893 - silhouette: 0.316
    km20 - inercia: 17294.264 - silhouette: 0.321
    km25 - inercia: 13170.437 - silhouette: 0.309
### 04
    km03 - inercia: 176886.135 - silhouette: 0.316
    km05 - inercia: 132446.005 - silhouette: 0.282
    km10 - inercia: 84181.596 - silhouette: 0.228
    km15 - inercia: 60565.190 - silhouette: 0.248
    km20 - inercia: 49382.691 - silhouette: 0.230
    km25 - inercia: 41903.795 - silhouette: 0.238
### 05
    km03 - inercia: 63579.596 - silhouette: 0.361
    km05 - inercia: 37151.685 - silhouette: 0.325
    km10 - inercia: 20178.577 - silhouette: 0.307
    km15 - inercia: 12440.842 - silhouette: 0.295
    km20 - inercia: 8318.860 - silhouette: 0.293
    km25 - inercia: 6177.420 - silhouette: 0.295
### 06
    km03 - inercia: 56867.670 - silhouette: 0.359
    km05 - inercia: 39422.483 - silhouette: 0.308
    km10 - inercia: 17662.826 - silhouette: 0.322
    km15 - inercia: 12565.740 - silhouette: 0.288
    km20 - inercia: 8638.183 - silhouette: 0.291
    km25 - inercia: 6115.648 - silhouette: 0.306
### 07
    km03 - inercia: 22649.716 - silhouette: 0.340
    km05 - inercia: 16306.674 - silhouette: 0.284
    km10 - inercia: 9880.666 - silhouette: 0.266
    km15 - inercia: 6582.715 - silhouette: 0.232
    km20 - inercia: 4818.497 - silhouette: 0.233
    km25 - inercia: 3831.938 - silhouette: 0.222
### 08
    km03 - inercia: 720245.295 - silhouette: 0.827
    km05 - inercia: 426695.749 - silhouette: 0.279
    km10 - inercia: 190018.140 - silhouette: 0.295
    km15 - inercia: 122795.363 - silhouette: 0.270
    km20 - inercia: 89149.205 - silhouette: 0.273
    km25 - inercia: 70079.516 - silhouette: 0.265
### 09
    km03 - inercia: 151912.676 - silhouette: 0.381
    km05 - inercia: 91713.377 - silhouette: 0.351
    km10 - inercia: 40815.382 - silhouette: 0.267
    km15 - inercia: 24262.392 - silhouette: 0.318
    km20 - inercia: 17476.234 - silhouette: 0.295
    km25 - inercia: 12283.676 - silhouette: 0.270
### 10
    km03 - inercia: 66575.446 - silhouette: 0.267
    km05 - inercia: 45715.889 - silhouette: 0.280
    km10 - inercia: 28619.342 - silhouette: 0.225
    km15 - inercia: 20092.207 - silhouette: 0.217
    km20 - inercia: 15040.821 - silhouette: 0.218
    km25 - inercia: 11541.698 - silhouette: 0.223
### 11
    km03 - inercia: 70612.813 - silhouette: 0.576
    km05 - inercia: 31805.387 - silhouette: 0.371
    km10 - inercia: 12684.411 - silhouette: 0.384
    km15 - inercia: 7769.518 - silhouette: 0.330
    km20 - inercia: 4711.356 - silhouette: 0.346
    km25 - inercia: 3053.262 - silhouette: 0.347
### 12
    km03 - inercia: 36756.165 - silhouette: 0.318
    km05 - inercia: 24730.133 - silhouette: 0.292
    km10 - inercia: 14596.698 - silhouette: 0.259
    km15 - inercia: 10642.651 - silhouette: 0.248
    km20 - inercia: 8164.290 - silhouette: 0.261
    km25 - inercia: 6491.983 - silhouette: 0.227
### 13
    km03 - inercia: 56673.022 - silhouette: 0.438
    km05 - inercia: 34499.665 - silhouette: 0.470
    km10 - inercia: 15099.089 - silhouette: 0.315
    km15 - inercia: 10231.111 - silhouette: 0.285
    km20 - inercia: 7640.911 - silhouette: 0.272
    km25 - inercia: 5896.522 - silhouette: 0.245
### 14
    km03 - inercia: 91116.420 - silhouette: 0.390
    km05 - inercia: 58371.391 - silhouette: 0.314
    km10 - inercia: 28997.104 - silhouette: 0.315
    km15 - inercia: 19071.136 - silhouette: 0.305
    km20 - inercia: 13422.251 - silhouette: 0.297
    km25 - inercia: 9329.269 - silhouette: 0.330
### 15
    km03 - inercia: 65152.984 - silhouette: 0.312
    km05 - inercia: 48958.119 - silhouette: 0.275
    km10 - inercia: 31533.154 - silhouette: 0.224
    km15 - inercia: 23007.508 - silhouette: 0.239
    km20 - inercia: 18404.983 - silhouette: 0.223
    km25 - inercia: 15295.152 - silhouette: 0.216
### 16
    km03 - inercia: 53551.763 - silhouette: 0.383
    km05 - inercia: 37335.718 - silhouette: 0.395
    km10 - inercia: 24114.951 - silhouette: 0.264
    km15 - inercia: 17354.594 - silhouette: 0.242
    km20 - inercia: 13389.906 - silhouette: 0.210
    km25 - inercia: 10831.368 - silhouette: 0.214
### 17
    km03 - inercia: 285065.733 - silhouette: 0.776
    km05 - inercia: 118640.286 - silhouette: 0.492
    km10 - inercia: 49809.343 - silhouette: 0.337
    km15 - inercia: 29217.987 - silhouette: 0.302
    km20 - inercia: 21802.947 - silhouette: 0.287
    km25 - inercia: 17363.674 - silhouette: 0.243
### 18
    km03 - inercia: 18142.753 - silhouette: 0.438
    km05 - inercia: 11958.605 - silhouette: 0.355
    km10 - inercia: 6609.793 - silhouette: 0.309
    km15 - inercia: 4598.147 - silhouette: 0.306
    km20 - inercia: 3453.553 - silhouette: 0.306
    km25 - inercia: 2633.448 - silhouette: 0.305
### 19
    km03 - inercia: 70325.952 - silhouette: 0.257
    km05 - inercia: 52402.035 - silhouette: 0.254
    km10 - inercia: 33005.325 - silhouette: 0.224
    km15 - inercia: 24539.144 - silhouette: 0.224
    km20 - inercia: 19083.501 - silhouette: 0.251
    km25 - inercia: 15247.488 - silhouette: 0.234
### 20
    km03 - inercia: 96219.251 - silhouette: 0.376
    km05 - inercia: 43627.929 - silhouette: 0.396
    km10 - inercia: 17788.138 - silhouette: 0.342
    km15 - inercia: 8030.312 - silhouette: 0.311
    km20 - inercia: 3174.220 - silhouette: 0.316
    km25 - inercia: 1240.882 - silhouette: 0.257
### 21
    km03 - inercia: 97870.834 - silhouette: 0.302
    km05 - inercia: 71016.256 - silhouette: 0.271
    km10 - inercia: 48914.791 - silhouette: 0.228
    km15 - inercia: 39171.656 - silhouette: 0.223
    km20 - inercia: 33286.204 - silhouette: 0.212
    km25 - inercia: 28599.051 - silhouette: 0.188
### 22
    km03 - inercia: 77108.658 - silhouette: 0.487
    km05 - inercia: 45862.546 - silhouette: 0.368
    km10 - inercia: 28523.767 - silhouette: 0.281
    km15 - inercia: 20392.314 - silhouette: 0.236
    km20 - inercia: 15435.064 - silhouette: 0.249
    km25 - inercia: 12560.257 - silhouette: 0.224
### 23
    km03 - inercia: 86210.596 - silhouette: 0.435
    km05 - inercia: 44084.371 - silhouette: 0.326
    km10 - inercia: 26036.980 - silhouette: 0.288
    km15 - inercia: 18643.708 - silhouette: 0.265
    km20 - inercia: 14127.696 - silhouette: 0.258
    km25 - inercia: 11788.343 - silhouette: 0.263
### 24
    km03 - inercia: 2982.700 - silhouette: 0.335
    km05 - inercia: 1348.273 - silhouette: 0.383
    km10 - inercia: 520.089 - silhouette: 0.296
    km15 - inercia: 245.153 - silhouette: 0.284
    km20 - inercia: 118.554 - silhouette: 0.232
    km25 - inercia: 48.356 - silhouette: 0.179
## SeccionesXDistrito(codEleccion=e2023_paso,codCategoria=03)
### 01
    km03 - inercia: 242.250 - silhouette: 0.503
    km05 - inercia: 86.238 - silhouette: 0.439
    km10 - inercia: 9.121 - silhouette: 0.299
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 11891.505 - silhouette: 0.303
    km05 - inercia: 8326.526 - silhouette: 0.294
    km10 - inercia: 5089.466 - silhouette: 0.266
    km15 - inercia: 3737.677 - silhouette: 0.254
    km20 - inercia: 2829.086 - silhouette: 0.259
    km25 - inercia: 2241.948 - silhouette: 0.258
### 03
    km03 - inercia: 2828.963 - silhouette: 0.277
    km05 - inercia: 1418.373 - silhouette: 0.285
    km10 - inercia: 250.787 - silhouette: 0.268
    km15 - inercia: 14.117 - silhouette: 0.079
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 04
    km03 - inercia: 819.813 - silhouette: 0.346
    km05 - inercia: 487.140 - silhouette: 0.296
    km10 - inercia: 187.349 - silhouette: 0.233
    km15 - inercia: 55.040 - silhouette: 0.218
    km20 - inercia: 14.431 - silhouette: 0.172
    km25 - inercia: 1.716 - silhouette: 0.026
### 05
    km03 - inercia: 1130.233 - silhouette: 0.373
    km05 - inercia: 639.164 - silhouette: 0.322
    km10 - inercia: 254.999 - silhouette: 0.252
    km15 - inercia: 84.276 - silhouette: 0.239
    km20 - inercia: 25.860 - silhouette: 0.145
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 06
    km03 - inercia: 1537.667 - silhouette: 0.447
    km05 - inercia: 810.893 - silhouette: 0.329
    km10 - inercia: 292.462 - silhouette: 0.227
    km15 - inercia: 108.160 - silhouette: 0.194
    km20 - inercia: 31.422 - silhouette: 0.118
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 07
    km03 - inercia: 838.642 - silhouette: 0.319
    km05 - inercia: 404.834 - silhouette: 0.297
    km10 - inercia: 60.131 - silhouette: 0.247
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 08
    km03 - inercia: 1728.578 - silhouette: 0.348
    km05 - inercia: 897.294 - silhouette: 0.304
    km10 - inercia: 195.072 - silhouette: 0.229
    km15 - inercia: 12.133 - silhouette: 0.110
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 09
    km03 - inercia: 224.732 - silhouette: 0.450
    km05 - inercia: 40.348 - silhouette: 0.430
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 10
    km03 - inercia: 1514.548 - silhouette: 0.293
    km05 - inercia: 739.675 - silhouette: 0.299
    km10 - inercia: 191.462 - silhouette: 0.194
    km15 - inercia: 11.053 - silhouette: 0.053
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 11
    km03 - inercia: 5263.916 - silhouette: 0.369
    km05 - inercia: 2303.607 - silhouette: 0.378
    km10 - inercia: 552.566 - silhouette: 0.319
    km15 - inercia: 102.395 - silhouette: 0.228
    km20 - inercia: 13.225 - silhouette: 0.072
    (n_samples=22 should be >= n_clusters=25.)
### 12
    km03 - inercia: 820.782 - silhouette: 0.325
    km05 - inercia: 441.464 - silhouette: 0.303
    km10 - inercia: 143.581 - silhouette: 0.224
    km15 - inercia: 31.023 - silhouette: 0.096
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 13
    km03 - inercia: 984.358 - silhouette: 0.320
    km05 - inercia: 468.505 - silhouette: 0.306
    km10 - inercia: 86.604 - silhouette: 0.280
    km15 - inercia: 12.021 - silhouette: 0.139
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 14
    km03 - inercia: 4741.473 - silhouette: 0.317
    km05 - inercia: 2299.392 - silhouette: 0.347
    km10 - inercia: 495.929 - silhouette: 0.254
    km15 - inercia: 51.939 - silhouette: 0.068
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 15
    km03 - inercia: 1095.093 - silhouette: 0.469
    km05 - inercia: 560.629 - silhouette: 0.358
    km10 - inercia: 108.202 - silhouette: 0.199
    km15 - inercia: 9.969 - silhouette: 0.022
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 16
    km03 - inercia: 765.535 - silhouette: 0.296
    km05 - inercia: 383.467 - silhouette: 0.257
    km10 - inercia: 37.609 - silhouette: 0.133
    (n_samples=13 should be >= n_clusters=15.)
    (n_samples=13 should be >= n_clusters=20.)
    (n_samples=13 should be >= n_clusters=25.)
### 17
    km03 - inercia: 1215.977 - silhouette: 0.414
    km05 - inercia: 671.741 - silhouette: 0.314
    km10 - inercia: 203.753 - silhouette: 0.243
    km15 - inercia: 68.185 - silhouette: 0.177
    km20 - inercia: 12.923 - silhouette: 0.074
    (n_samples=23 should be >= n_clusters=25.)
### 18
    km03 - inercia: 761.966 - silhouette: 0.464
    km05 - inercia: 412.692 - silhouette: 0.332
    km10 - inercia: 134.413 - silhouette: 0.227
    km15 - inercia: 27.939 - silhouette: 0.156
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 189.566 - silhouette: 0.362
    km05 - inercia: 75.931 - silhouette: 0.257
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 3127.059 - silhouette: 0.426
    km05 - inercia: 437.911 - silhouette: 0.316
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 21
    km03 - inercia: 298.860 - silhouette: 0.399
    km05 - inercia: 156.749 - silhouette: 0.324
    km10 - inercia: 47.612 - silhouette: 0.301
    km15 - inercia: 13.306 - silhouette: 0.187
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 22
    km03 - inercia: 1691.125 - silhouette: 0.407
    km05 - inercia: 806.123 - silhouette: 0.439
    km10 - inercia: 289.847 - silhouette: 0.323
    km15 - inercia: 141.336 - silhouette: 0.209
    km20 - inercia: 47.657 - silhouette: 0.185
    km25 - inercia: 5.376 - silhouette: 0.070
### 23
    km03 - inercia: 1082.177 - silhouette: 0.410
    km05 - inercia: 566.150 - silhouette: 0.347
    km10 - inercia: 101.674 - silhouette: 0.289
    km15 - inercia: 15.386 - silhouette: 0.090
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 24
    km03 - inercia: 74.042 - silhouette: 0.107
    (n_samples=4 should be >= n_clusters=5.)
    (n_samples=4 should be >= n_clusters=10.)
    (n_samples=4 should be >= n_clusters=15.)
    (n_samples=4 should be >= n_clusters=20.)
    (n_samples=4 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2023_paso,codCategoria=04)
### 02
    km03 - inercia: 252259.700 - silhouette: 0.269
    km05 - inercia: 187876.028 - silhouette: 0.302
    km10 - inercia: 124155.834 - silhouette: 0.234
    km15 - inercia: 95477.202 - silhouette: 0.225
    km20 - inercia: 78083.672 - silhouette: 0.236
    km25 - inercia: 67606.348 - silhouette: 0.225
### 03
    km03 - inercia: 107067.399 - silhouette: 0.399
    km05 - inercia: 63383.089 - silhouette: 0.346
    km10 - inercia: 32853.220 - silhouette: 0.283
    km15 - inercia: 21319.869 - silhouette: 0.292
    km20 - inercia: 14944.473 - silhouette: 0.304
    km25 - inercia: 11141.684 - silhouette: 0.304
### 20
    km03 - inercia: 9962.265 - silhouette: 0.487
    km05 - inercia: 4134.490 - silhouette: 0.468
    km10 - inercia: 1655.426 - silhouette: 0.355
    km15 - inercia: 756.816 - silhouette: 0.280
    km20 - inercia: 359.250 - silhouette: 0.272
    km25 - inercia: 148.309 - silhouette: 0.201
## SeccionesXDistrito(codEleccion=e2023_paso,codCategoria=04)
### 02
    km03 - inercia: 11912.486 - silhouette: 0.287
    km05 - inercia: 8013.015 - silhouette: 0.321
    km10 - inercia: 4830.465 - silhouette: 0.258
    km15 - inercia: 3524.054 - silhouette: 0.260
    km20 - inercia: 2725.849 - silhouette: 0.256
    km25 - inercia: 2057.301 - silhouette: 0.261
### 03
    km03 - inercia: 2354.980 - silhouette: 0.250
    km05 - inercia: 1214.735 - silhouette: 0.278
    km10 - inercia: 225.883 - silhouette: 0.232
    km15 - inercia: 6.132 - silhouette: 0.079
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 20
    km03 - inercia: 287.919 - silhouette: 0.363
    km05 - inercia: 31.737 - silhouette: 0.298
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2023_paso,codCategoria=06)
### 02
    km03 - inercia: 144022.403 - silhouette: 0.272
    km05 - inercia: 106966.647 - silhouette: 0.264
    km10 - inercia: 69054.846 - silhouette: 0.236
    km15 - inercia: 51732.094 - silhouette: 0.249
    km20 - inercia: 43124.021 - silhouette: 0.248
    km25 - inercia: 36069.235 - silhouette: 0.233
### 03
    km03 - inercia: 149784.768 - silhouette: 0.394
    km05 - inercia: 94453.507 - silhouette: 0.331
    km10 - inercia: 44195.591 - silhouette: 0.320
    km15 - inercia: 26663.557 - silhouette: 0.314
    km20 - inercia: 19106.961 - silhouette: 0.287
    km25 - inercia: 14672.093 - silhouette: 0.306
### 20
    km03 - inercia: 19573.848 - silhouette: 0.360
    km05 - inercia: 9749.771 - silhouette: 0.382
    km10 - inercia: 2627.459 - silhouette: 0.344
    km15 - inercia: 1243.665 - silhouette: 0.296
    km20 - inercia: 533.594 - silhouette: 0.274
    km25 - inercia: 200.525 - silhouette: 0.193
## SeccionesXDistrito(codEleccion=e2023_paso,codCategoria=06)
### 02
    km03 - inercia: 7033.926 - silhouette: 0.320
    km05 - inercia: 4725.626 - silhouette: 0.296
    km10 - inercia: 2443.824 - silhouette: 0.295
    km15 - inercia: 1571.399 - silhouette: 0.301
    km20 - inercia: 1084.611 - silhouette: 0.305
    km25 - inercia: 831.743 - silhouette: 0.281
### 03
    km03 - inercia: 2766.188 - silhouette: 0.251
    km05 - inercia: 1380.110 - silhouette: 0.269
    km10 - inercia: 273.612 - silhouette: 0.222
    km15 - inercia: 21.230 - silhouette: 0.057
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 20
    km03 - inercia: 409.402 - silhouette: 0.312
    km05 - inercia: 100.995 - silhouette: 0.208
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
## CircuitosXPais(codEleccion=e2023_generales,codCategoria=01)
    km03 - inercia: 2002362.845 - silhouette: 0.299
    km05 - inercia: 1267714.433 - silhouette: 0.316
    km10 - inercia: 793322.472 - silhouette: 0.286
    km15 - inercia: 612225.041 - silhouette: 0.269
    km20 - inercia: 515133.086 - silhouette: 0.262
    km25 - inercia: 450731.250 - silhouette: 0.249
## SeccionesXPais(codEleccion=e2023_generales,codCategoria=01)
    km03 - inercia: 90716.120 - silhouette: 0.321
    km05 - inercia: 53929.381 - silhouette: 0.350
    km10 - inercia: 27644.914 - silhouette: 0.312
    km15 - inercia: 20214.826 - silhouette: 0.299
    km20 - inercia: 15686.542 - silhouette: 0.282
    km25 - inercia: 13412.461 - silhouette: 0.268
## CircuitosXDistrito(codEleccion=e2023_generales,codCategoria=03)
### 01
    km03 - inercia: 7098.545 - silhouette: 0.471
    km05 - inercia: 3693.496 - silhouette: 0.408
    km10 - inercia: 1591.107 - silhouette: 0.378
    km15 - inercia: 1044.109 - silhouette: 0.366
    km20 - inercia: 713.555 - silhouette: 0.359
    km25 - inercia: 537.312 - silhouette: 0.336
### 02
    km03 - inercia: 195626.539 - silhouette: 0.296
    km05 - inercia: 139145.186 - silhouette: 0.273
    km10 - inercia: 91728.068 - silhouette: 0.253
    km15 - inercia: 72275.305 - silhouette: 0.240
    km20 - inercia: 60734.047 - silhouette: 0.247
    km25 - inercia: 53184.814 - silhouette: 0.240
### 03
    km03 - inercia: 112740.657 - silhouette: 0.275
    km05 - inercia: 53792.749 - silhouette: 0.349
    km10 - inercia: 29027.165 - silhouette: 0.330
    km15 - inercia: 19570.592 - silhouette: 0.323
    km20 - inercia: 14296.702 - silhouette: 0.313
    km25 - inercia: 11594.251 - silhouette: 0.281
### 04
    km03 - inercia: 156254.219 - silhouette: 0.295
    km05 - inercia: 111138.380 - silhouette: 0.248
    km10 - inercia: 69170.329 - silhouette: 0.249
    km15 - inercia: 52034.833 - silhouette: 0.241
    km20 - inercia: 43142.151 - silhouette: 0.231
    km25 - inercia: 36537.957 - silhouette: 0.223
### 05
    km03 - inercia: 32273.391 - silhouette: 0.331
    km05 - inercia: 20912.419 - silhouette: 0.337
    km10 - inercia: 10772.927 - silhouette: 0.319
    km15 - inercia: 7388.634 - silhouette: 0.315
    km20 - inercia: 5230.344 - silhouette: 0.292
    km25 - inercia: 3790.034 - silhouette: 0.307
### 06
    km03 - inercia: 29064.394 - silhouette: 0.416
    km05 - inercia: 15207.284 - silhouette: 0.398
    km10 - inercia: 6681.641 - silhouette: 0.345
    km15 - inercia: 4370.978 - silhouette: 0.343
    km20 - inercia: 2956.252 - silhouette: 0.338
    km25 - inercia: 2328.897 - silhouette: 0.336
### 07
    km03 - inercia: 17850.874 - silhouette: 0.320
    km05 - inercia: 13388.762 - silhouette: 0.282
    km10 - inercia: 7538.920 - silhouette: 0.277
    km15 - inercia: 5043.409 - silhouette: 0.274
    km20 - inercia: 3749.559 - silhouette: 0.270
    km25 - inercia: 2929.889 - silhouette: 0.240
### 08
    km03 - inercia: 130581.353 - silhouette: 0.336
    km05 - inercia: 92924.915 - silhouette: 0.244
    km10 - inercia: 56800.003 - silhouette: 0.251
    km15 - inercia: 42183.985 - silhouette: 0.251
    km20 - inercia: 33940.940 - silhouette: 0.249
    km25 - inercia: 27169.538 - silhouette: 0.239
### 09
    km03 - inercia: 54111.781 - silhouette: 0.344
    km05 - inercia: 38107.394 - silhouette: 0.354
    km10 - inercia: 22056.913 - silhouette: 0.270
    km15 - inercia: 14909.826 - silhouette: 0.266
    km20 - inercia: 10239.112 - silhouette: 0.287
    km25 - inercia: 7710.343 - silhouette: 0.286
### 10
    km03 - inercia: 46307.885 - silhouette: 0.246
    km05 - inercia: 31172.295 - silhouette: 0.282
    km10 - inercia: 20205.677 - silhouette: 0.257
    km15 - inercia: 14431.081 - silhouette: 0.249
    km20 - inercia: 10996.999 - silhouette: 0.238
    km25 - inercia: 9191.771 - silhouette: 0.244
### 11
    km03 - inercia: 52795.914 - silhouette: 0.458
    km05 - inercia: 27273.595 - silhouette: 0.419
    km10 - inercia: 9862.342 - silhouette: 0.418
    km15 - inercia: 4678.226 - silhouette: 0.422
    km20 - inercia: 2773.560 - silhouette: 0.394
    km25 - inercia: 1968.053 - silhouette: 0.386
### 12
    km03 - inercia: 25964.661 - silhouette: 0.412
    km05 - inercia: 12893.713 - silhouette: 0.368
    km10 - inercia: 7113.524 - silhouette: 0.284
    km15 - inercia: 4931.048 - silhouette: 0.277
    km20 - inercia: 3908.534 - silhouette: 0.274
    km25 - inercia: 3040.180 - silhouette: 0.272
### 13
    km03 - inercia: 25153.553 - silhouette: 0.444
    km05 - inercia: 15874.125 - silhouette: 0.359
    km10 - inercia: 9360.854 - silhouette: 0.275
    km15 - inercia: 6514.435 - silhouette: 0.286
    km20 - inercia: 4747.800 - silhouette: 0.266
    km25 - inercia: 3820.028 - silhouette: 0.265
### 14
    km03 - inercia: 44356.630 - silhouette: 0.374
    km05 - inercia: 26850.731 - silhouette: 0.344
    km10 - inercia: 13478.222 - silhouette: 0.310
    km15 - inercia: 8047.860 - silhouette: 0.357
    km20 - inercia: 5902.662 - silhouette: 0.360
    km25 - inercia: 3836.274 - silhouette: 0.378
### 15
    km03 - inercia: 57567.488 - silhouette: 0.338
    km05 - inercia: 42714.711 - silhouette: 0.244
    km10 - inercia: 26519.248 - silhouette: 0.263
    km15 - inercia: 19903.046 - silhouette: 0.221
    km20 - inercia: 16183.549 - silhouette: 0.212
    km25 - inercia: 13288.445 - silhouette: 0.230
### 16
    km03 - inercia: 45823.531 - silhouette: 0.302
    km05 - inercia: 31643.676 - silhouette: 0.323
    km10 - inercia: 18100.202 - silhouette: 0.279
    km15 - inercia: 12248.994 - silhouette: 0.262
    km20 - inercia: 9509.333 - silhouette: 0.257
    km25 - inercia: 7851.069 - silhouette: 0.252
### 17
    km03 - inercia: 51011.863 - silhouette: 0.418
    km05 - inercia: 29417.566 - silhouette: 0.392
    km10 - inercia: 13721.109 - silhouette: 0.315
    km15 - inercia: 10092.698 - silhouette: 0.282
    km20 - inercia: 8027.350 - silhouette: 0.261
    km25 - inercia: 6866.568 - silhouette: 0.237
### 18
    km03 - inercia: 15377.910 - silhouette: 0.460
    km05 - inercia: 8640.932 - silhouette: 0.384
    km10 - inercia: 4666.627 - silhouette: 0.331
    km15 - inercia: 3348.920 - silhouette: 0.312
    km20 - inercia: 2536.662 - silhouette: 0.309
    km25 - inercia: 2075.951 - silhouette: 0.294
### 19
    km03 - inercia: 52834.902 - silhouette: 0.352
    km05 - inercia: 36539.455 - silhouette: 0.285
    km10 - inercia: 21591.387 - silhouette: 0.233
    km15 - inercia: 16395.249 - silhouette: 0.256
    km20 - inercia: 12529.414 - silhouette: 0.247
    km25 - inercia: 10136.120 - silhouette: 0.215
### 20
    km03 - inercia: 43104.963 - silhouette: 0.507
    km05 - inercia: 24015.597 - silhouette: 0.333
    km10 - inercia: 6744.242 - silhouette: 0.376
    km15 - inercia: 2176.234 - silhouette: 0.356
    km20 - inercia: 954.186 - silhouette: 0.313
    km25 - inercia: 310.157 - silhouette: 0.251
### 21
    km03 - inercia: 83208.672 - silhouette: 0.350
    km05 - inercia: 58329.569 - silhouette: 0.288
    km10 - inercia: 38942.802 - silhouette: 0.268
    km15 - inercia: 28937.210 - silhouette: 0.247
    km20 - inercia: 23643.587 - silhouette: 0.244
    km25 - inercia: 19668.516 - silhouette: 0.247
### 22
    km03 - inercia: 62142.809 - silhouette: 0.556
    km05 - inercia: 38473.027 - silhouette: 0.437
    km10 - inercia: 19265.217 - silhouette: 0.314
    km15 - inercia: 13140.434 - silhouette: 0.314
    km20 - inercia: 8728.040 - silhouette: 0.312
    km25 - inercia: 6370.070 - silhouette: 0.347
### 23
    km03 - inercia: 34580.543 - silhouette: 0.457
    km05 - inercia: 19626.128 - silhouette: 0.347
    km10 - inercia: 11307.636 - silhouette: 0.296
    km15 - inercia: 8320.392 - silhouette: 0.271
    km20 - inercia: 6802.545 - silhouette: 0.245
    km25 - inercia: 5634.952 - silhouette: 0.245
### 24
    km03 - inercia: 2375.928 - silhouette: 0.431
    km05 - inercia: 1209.475 - silhouette: 0.407
    km10 - inercia: 506.252 - silhouette: 0.259
    km15 - inercia: 204.896 - silhouette: 0.291
    km20 - inercia: 110.670 - silhouette: 0.220
    km25 - inercia: 50.276 - silhouette: 0.148
## SeccionesXDistrito(codEleccion=e2023_generales,codCategoria=03)
### 01
    km03 - inercia: 270.597 - silhouette: 0.560
    km05 - inercia: 90.457 - silhouette: 0.454
    km10 - inercia: 7.821 - silhouette: 0.372
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 02
    km03 - inercia: 8730.768 - silhouette: 0.320
    km05 - inercia: 6164.363 - silhouette: 0.295
    km10 - inercia: 3661.484 - silhouette: 0.271
    km15 - inercia: 2532.675 - silhouette: 0.285
    km20 - inercia: 1869.485 - silhouette: 0.294
    km25 - inercia: 1420.734 - silhouette: 0.305
### 03
    km03 - inercia: 2198.161 - silhouette: 0.341
    km05 - inercia: 1271.075 - silhouette: 0.304
    km10 - inercia: 169.769 - silhouette: 0.351
    km15 - inercia: 9.151 - silhouette: 0.079
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 04
    km03 - inercia: 769.866 - silhouette: 0.442
    km05 - inercia: 443.616 - silhouette: 0.278
    km10 - inercia: 150.919 - silhouette: 0.212
    km15 - inercia: 58.390 - silhouette: 0.168
    km20 - inercia: 18.817 - silhouette: 0.135
    km25 - inercia: 1.786 - silhouette: 0.028
### 05
    km03 - inercia: 1063.881 - silhouette: 0.359
    km05 - inercia: 581.430 - silhouette: 0.338
    km10 - inercia: 186.482 - silhouette: 0.364
    km15 - inercia: 63.715 - silhouette: 0.318
    km20 - inercia: 16.735 - silhouette: 0.191
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 06
    km03 - inercia: 548.906 - silhouette: 0.383
    km05 - inercia: 332.589 - silhouette: 0.331
    km10 - inercia: 100.672 - silhouette: 0.420
    km15 - inercia: 33.779 - silhouette: 0.315
    km20 - inercia: 7.827 - silhouette: 0.215
    (Number of labels is 25. Valid values are 2 to n_samples - 1 (inclusive))
### 07
    km03 - inercia: 538.891 - silhouette: 0.276
    km05 - inercia: 312.928 - silhouette: 0.255
    km10 - inercia: 62.996 - silhouette: 0.129
    (Number of labels is 15. Valid values are 2 to n_samples - 1 (inclusive))
    (n_samples=15 should be >= n_clusters=20.)
    (n_samples=15 should be >= n_clusters=25.)
### 08
    km03 - inercia: 647.336 - silhouette: 0.377
    km05 - inercia: 347.605 - silhouette: 0.353
    km10 - inercia: 84.309 - silhouette: 0.254
    km15 - inercia: 7.679 - silhouette: 0.124
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 09
    km03 - inercia: 193.181 - silhouette: 0.481
    km05 - inercia: 46.643 - silhouette: 0.282
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 10
    km03 - inercia: 1255.494 - silhouette: 0.323
    km05 - inercia: 590.878 - silhouette: 0.327
    km10 - inercia: 165.470 - silhouette: 0.158
    km15 - inercia: 13.109 - silhouette: 0.027
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 11
    km03 - inercia: 3524.246 - silhouette: 0.438
    km05 - inercia: 1337.179 - silhouette: 0.466
    km10 - inercia: 313.601 - silhouette: 0.278
    km15 - inercia: 71.229 - silhouette: 0.223
    km20 - inercia: 6.733 - silhouette: 0.110
    (n_samples=22 should be >= n_clusters=25.)
### 12
    km03 - inercia: 456.578 - silhouette: 0.383
    km05 - inercia: 238.982 - silhouette: 0.312
    km10 - inercia: 58.683 - silhouette: 0.360
    km15 - inercia: 7.882 - silhouette: 0.178
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 13
    km03 - inercia: 950.032 - silhouette: 0.291
    km05 - inercia: 462.609 - silhouette: 0.348
    km10 - inercia: 101.072 - silhouette: 0.272
    km15 - inercia: 15.745 - silhouette: 0.115
    (n_samples=18 should be >= n_clusters=20.)
    (n_samples=18 should be >= n_clusters=25.)
### 14
    km03 - inercia: 2914.943 - silhouette: 0.313
    km05 - inercia: 1174.207 - silhouette: 0.367
    km10 - inercia: 252.104 - silhouette: 0.279
    km15 - inercia: 9.825 - silhouette: 0.139
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 15
    km03 - inercia: 979.324 - silhouette: 0.462
    km05 - inercia: 511.474 - silhouette: 0.365
    km10 - inercia: 100.733 - silhouette: 0.306
    km15 - inercia: 5.367 - silhouette: 0.062
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
### 16
    km03 - inercia: 916.143 - silhouette: 0.337
    km05 - inercia: 420.760 - silhouette: 0.317
    km10 - inercia: 44.748 - silhouette: 0.175
    (n_samples=13 should be >= n_clusters=15.)
    (n_samples=13 should be >= n_clusters=20.)
    (n_samples=13 should be >= n_clusters=25.)
### 17
    km03 - inercia: 1027.902 - silhouette: 0.440
    km05 - inercia: 411.926 - silhouette: 0.382
    km10 - inercia: 137.194 - silhouette: 0.286
    km15 - inercia: 45.925 - silhouette: 0.244
    km20 - inercia: 6.642 - silhouette: 0.127
    (n_samples=23 should be >= n_clusters=25.)
### 18
    km03 - inercia: 688.848 - silhouette: 0.481
    km05 - inercia: 320.233 - silhouette: 0.411
    km10 - inercia: 82.447 - silhouette: 0.256
    km15 - inercia: 16.315 - silhouette: 0.123
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 19
    km03 - inercia: 152.360 - silhouette: 0.368
    km05 - inercia: 63.697 - silhouette: 0.229
    (n_samples=9 should be >= n_clusters=10.)
    (n_samples=9 should be >= n_clusters=15.)
    (n_samples=9 should be >= n_clusters=20.)
    (n_samples=9 should be >= n_clusters=25.)
### 20
    km03 - inercia: 666.673 - silhouette: 0.417
    km05 - inercia: 117.606 - silhouette: 0.304
    (n_samples=7 should be >= n_clusters=10.)
    (n_samples=7 should be >= n_clusters=15.)
    (n_samples=7 should be >= n_clusters=20.)
    (n_samples=7 should be >= n_clusters=25.)
### 21
    km03 - inercia: 332.016 - silhouette: 0.348
    km05 - inercia: 179.264 - silhouette: 0.330
    km10 - inercia: 58.019 - silhouette: 0.244
    km15 - inercia: 11.828 - silhouette: 0.169
    (n_samples=19 should be >= n_clusters=20.)
    (n_samples=19 should be >= n_clusters=25.)
### 22
    km03 - inercia: 2775.969 - silhouette: 0.418
    km05 - inercia: 1229.237 - silhouette: 0.420
    km10 - inercia: 244.451 - silhouette: 0.384
    km15 - inercia: 73.100 - silhouette: 0.238
    km20 - inercia: 13.701 - silhouette: 0.221
    km25 - inercia: 0.841 - silhouette: 0.104
### 23
    km03 - inercia: 805.232 - silhouette: 0.408
    km05 - inercia: 406.443 - silhouette: 0.332
    km10 - inercia: 58.652 - silhouette: 0.314
    km15 - inercia: 4.946 - silhouette: 0.114
    (n_samples=17 should be >= n_clusters=20.)
    (n_samples=17 should be >= n_clusters=25.)
### 24
    km03 - inercia: 93.105 - silhouette: 0.093
    (n_samples=4 should be >= n_clusters=5.)
    (n_samples=4 should be >= n_clusters=10.)
    (n_samples=4 should be >= n_clusters=15.)
    (n_samples=4 should be >= n_clusters=20.)
    (n_samples=4 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2023_generales,codCategoria=04)
### 02
    km03 - inercia: 186345.050 - silhouette: 0.308
    km05 - inercia: 130986.364 - silhouette: 0.279
    km10 - inercia: 86270.327 - silhouette: 0.254
    km15 - inercia: 67549.032 - silhouette: 0.243
    km20 - inercia: 56623.139 - silhouette: 0.230
    km25 - inercia: 49259.148 - silhouette: 0.229
### 03
    km03 - inercia: 65425.970 - silhouette: 0.317
    km05 - inercia: 40158.553 - silhouette: 0.357
    km10 - inercia: 21199.754 - silhouette: 0.320
    km15 - inercia: 14429.747 - silhouette: 0.311
    km20 - inercia: 10836.897 - silhouette: 0.318
    km25 - inercia: 9098.044 - silhouette: 0.278
## SeccionesXDistrito(codEleccion=e2023_generales,codCategoria=04)
### 02
    km03 - inercia: 8158.053 - silhouette: 0.339
    km05 - inercia: 5637.942 - silhouette: 0.310
    km10 - inercia: 3287.092 - silhouette: 0.293
    km15 - inercia: 2357.674 - silhouette: 0.267
    km20 - inercia: 1768.180 - silhouette: 0.285
    km25 - inercia: 1358.268 - silhouette: 0.267
### 03
    km03 - inercia: 1678.957 - silhouette: 0.387
    km05 - inercia: 842.068 - silhouette: 0.438
    km10 - inercia: 124.451 - silhouette: 0.335
    km15 - inercia: 7.236 - silhouette: 0.097
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
## CircuitosXDistrito(codEleccion=e2023_generales,codCategoria=06)
### 02
    km03 - inercia: 106518.445 - silhouette: 0.285
    km05 - inercia: 77339.834 - silhouette: 0.261
    km10 - inercia: 50657.530 - silhouette: 0.250
    km15 - inercia: 38500.398 - silhouette: 0.245
    km20 - inercia: 32131.812 - silhouette: 0.240
    km25 - inercia: 27424.441 - silhouette: 0.240
### 03
    km03 - inercia: 106692.334 - silhouette: 0.328
    km05 - inercia: 53482.262 - silhouette: 0.349
    km10 - inercia: 27179.413 - silhouette: 0.305
    km15 - inercia: 18165.874 - silhouette: 0.305
    km20 - inercia: 13452.376 - silhouette: 0.301
    km25 - inercia: 10508.341 - silhouette: 0.290
## SeccionesXDistrito(codEleccion=e2023_generales,codCategoria=06)
### 02
    km03 - inercia: 5336.072 - silhouette: 0.301
    km05 - inercia: 3570.094 - silhouette: 0.320
    km10 - inercia: 1894.192 - silhouette: 0.315
    km15 - inercia: 1109.418 - silhouette: 0.321
    km20 - inercia: 735.335 - silhouette: 0.338
    km25 - inercia: 522.034 - silhouette: 0.321
### 03
    km03 - inercia: 2066.926 - silhouette: 0.363
    km05 - inercia: 1210.575 - silhouette: 0.385
    km10 - inercia: 138.020 - silhouette: 0.382
    km15 - inercia: 9.869 - silhouette: 0.094
    (n_samples=16 should be >= n_clusters=20.)
    (n_samples=16 should be >= n_clusters=25.)
## CircuitosXPais(codEleccion=e2023_2vuelta,codCategoria=01)
    km03 - inercia: 707436.839 - silhouette: 0.528
    km05 - inercia: 321199.969 - silhouette: 0.496
    km10 - inercia: 119131.645 - silhouette: 0.467
    km15 - inercia: 66272.486 - silhouette: 0.431
    km20 - inercia: 47910.925 - silhouette: 0.395
    km25 - inercia: 37909.455 - silhouette: 0.377
## SeccionesXPais(codEleccion=e2023_2vuelta,codCategoria=01)
    km03 - inercia: 32489.509 - silhouette: 0.530
    km05 - inercia: 12428.172 - silhouette: 0.527
    km10 - inercia: 3783.827 - silhouette: 0.493
    km15 - inercia: 1780.723 - silhouette: 0.482
    km20 - inercia: 1145.581 - silhouette: 0.436
    km25 - inercia: 833.022 - silhouette: 0.400
