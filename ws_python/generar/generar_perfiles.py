import asyncpg
import pandas as pd
import os
from numpy.linalg import norm
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.metrics import mean_squared_error
from modelo.elecciones import ResultadosCircuitos, Distritos, ResultadosSecciones
from modelo.bd_elecciones import BD_ELECCIONES

async def bd(codEleccion):
    conn = await asyncpg.connect(dsn=os.environ.get("DB_DSN"),database=codEleccion)
    return conn

def siglas_agrupacion(agrupacion):
    ignorar = ["LA", "LAS", "DEL", "DE", "EL", "LOS", "+", "-", "POR", "UN", "UNA", "Y"]
    if agrupacion.strip() == "HACEMOS POR NUESTRO PAIS":
        return 'H'
    palabras = agrupacion.split()
    # Si la primera palabra es "ALIANZA", la eliminamos
    if palabras[0].upper() == "ALIANZA":
        palabras = palabras[1:]    
    siglas = []
    # Iterar sobre cada palabra
    for i, palabra in enumerate(palabras):
        # Saltar palabras que comienzan con caracteres no alfanuméricos
        if palabra[0] in ['(']:
            continue
        palabra_mayus = palabra.upper()
        # Si es una palabra de la lista ignorar_palabras y no está al principio, no la consideramos
        if palabra_mayus in ignorar and i != 0 and i <=15:
            continue
        # Si es un número, lo agregamos tal cual
        if palabra_mayus.isdigit():
            siglas.append(palabra)
        else:
            # Si es una palabra, agregamos su primera letra en mayúsculas
            siglas.append(palabra[0].upper())
    return ''.join(siglas)

def obtener_agrupaciones_cluster(cluster, mean_porcentajes, std_porcentajes):
    if cluster not in mean_porcentajes.index:
        print(f"Cluster {cluster} no encontrado.")
        return
    # Obtener los valores de media para el cluster especificado
    mean_values = mean_porcentajes.loc[cluster]    
    # print(f"Cluster {cluster}:")
    sorted_agrupaciones = mean_values.sort_values(ascending=False)
    agrupaciones = ""
    nom_perfil= ""
    # Recorre cada agrupación dentro del cluster, hasta 10
    for agrupacion in sorted_agrupaciones.index[:10]:    
        mean_value = mean_porcentajes.loc[cluster, agrupacion]
        std_value = std_porcentajes.loc[cluster, agrupacion]
        # Imprime la media y el desvío estándar de la agrupación en el cluster
        agrupaciones += f'({agrupacion.strip()},{mean_value:.2f},{std_value:.3f}),'
        #print(f"    Agrupación: {agrupacion} - Media: {mean_value:.4f}, Desvío estándar: {std_value:.4f}")
        if agrupacion.strip() not in ["nulos","comando","recurridos","impugnados"]:
            nom_perfil += f"{siglas_agrupacion(agrupacion)}-{str(round(mean_value))} "
    #print (nom_perfil[:-1])
    return nom_perfil[:-1]+",{"+agrupaciones[:-1]+"}"

def generar_clusters(values, codEleccion, index_cols, n_clusters, elementos, archClasif, archPerfil, clave):

    # Convierte los resultados en un DataFrame de pandas
    df = pd.DataFrame(values, columns=values[0].keys())
    # Crear una tabla pivote con porcentaje
    pivot = pd.pivot_table(df, 
                        values='porcentaje', 
                        index=index_cols, 
                        columns='nom_agrupacion', 
                        aggfunc='mean', 
                        fill_value=0)
    # Preparar los datos para el clustering
    X = pivot.values
    try:
        # Aplicar KMeans
        kmeans = KMeans(n_clusters=n_clusters, init='k-means++', random_state=50)
        kmeans.fit(X)
        # Calcular el Silhouette Score: mide que tan bien separados están los clusters
        silhouette_avg = silhouette_score(X, kmeans.labels_)
    except Exception as e:
        print(f"    ({e})")
        return  # Sale de la función si hay una excepción

    # Inercia: suma de las distancias cuadráticas dentro de cada cluster
    inercia = kmeans.inertia_
    # Añadir los clusters al DataFrame
    pivot['Cluster'] = kmeans.labels_
    # Calcular tamaño de cada cluster:  nro de puntos en cada cluster
    cluster_sizes = pd.Series(kmeans.labels_).value_counts()
    # Media de los porcentajes de cada agrupación por cluster
    mean_porcentajes = pivot.groupby('Cluster').mean()
    # Desvío estándar: variabilidad dentro de cada cluster
    std_porcentajes = pivot.groupby('Cluster').std()

    # Genero cod_algoritmo
    cod_algoritmo = 'km' + str(n_clusters).zfill(2) 
    print(f"    {cod_algoritmo} - inercia: {inercia:.3f} - silhouette: {silhouette_avg:.3f}")
    # Guardar CLASIFICACION_PAIS / DISTRITO
    archClasif.write(f"{codEleccion},{clave},{cod_algoritmo},{elementos},{inercia:.3f},{silhouette_avg:.3f}\n")

    for cluster in sorted(cluster_sizes.index):
        # Tamaño del cluster
        size = cluster_sizes[cluster]
        # Calcular varianza intra-cluster
        cluster_points = X[kmeans.labels_ == cluster]
        centroid = kmeans.cluster_centers_[cluster]
        varianza = mean_squared_error(cluster_points, [centroid] * len(cluster_points))
        # Calcular distancia media al centroide
        distancia = norm(cluster_points - centroid, axis=1).mean()
        #print(f"    {elementos} {cluster} | {size} | {varianza:.3f} | {distancia:.3f}")
        # Guardar PERFIL PAIS / DISTRITO
        archPerfil.write(f"{codEleccion},{clave},{cod_algoritmo},{elementos},{cluster},{size},{varianza:.3f},{distancia:.3f},{obtener_agrupaciones_cluster(cluster, mean_porcentajes, std_porcentajes)}\n")

    # Reiniciar el índice para que los índices sean columnas
    pivot_reset = pivot.reset_index()
    # Seleccionar solo las columnas del índice y el cluster
    columns_to_save = index_cols + ['Cluster']
    pivot_to_save = pivot_reset[columns_to_save]
    # Agregar columna cod_eleccion y cod_algoritmo (a la última columna)
    pivot_to_save.insert(0, 'cod_eleccion', codEleccion)
    pivot_to_save.insert(len(pivot_to_save.columns) - 1, 'cod_algoritmo', cod_algoritmo)
    # Guardar en archivo CSV
    pivot_to_save.to_csv('perfiles_' + elementos + '.csv', mode='a', header=False, index=False)

def generar_n_clusters(values, codEleccion, index_cols, elementos, archClasif, archPerfil, clave):
    for n in (3, 5, 10, 15, 20, 25):
        generar_clusters(values, codEleccion, index_cols, n, elementos, archClasif, archPerfil, clave)

async def recorrerCircuitosXPais(codEleccion, codCategoria):
    archClasif = open('clasificacion_pais.csv', 'a') 
    archPerfil = open('perfil_pais.csv', 'a') 
    print('## CircuitosXPais(codEleccion='+codEleccion+',codCategoria='+codCategoria+')')
    conn = await bd(codEleccion)
    values = await conn.fetch(ResultadosCircuitos(codCategoria).xCategoriaDistritoSeccionCircuitoVotos())
    if not values:
        print(f"No se existen datos para {codEleccion} and categoría {codCategoria}")
        return
    generar_n_clusters(values, codEleccion, ['cod_categoria', 'cod_distrito', 'cod_seccion', 'cod_circuito'], 'circuitos', archClasif, archPerfil, codCategoria)
    await conn.close()
    archClasif.close()
    archPerfil.close()
    return values

async def recorrerSeccionesXPais(codEleccion, codCategoria):
    archClasif = open('clasificacion_pais.csv', 'a') 
    archPerfil = open('perfil_pais.csv', 'a') 
    print('## SeccionesXPais(codEleccion='+codEleccion+',codCategoria='+codCategoria+')')
    conn = await bd(codEleccion)
    values = await conn.fetch(ResultadosSecciones(codCategoria).xCategoriaDistritoSeccionVotos())
    if not values:
        print(f"No se existen datos para {codEleccion} and categoría {codCategoria}")
        return
    generar_n_clusters(values, codEleccion, ['cod_categoria', 'cod_distrito', 'cod_seccion'], 'secciones', archClasif, archPerfil, codCategoria)
    await conn.close()
    archClasif.close()
    archPerfil.close()
    return values

async def recorrerSeccionesXDistrito(codEleccion, codCategoria):
    archClasif = open('clasificacion_distrito.csv', 'a') 
    archPerfil = open('perfil_distrito.csv', 'a') 
    print('## SeccionesXDistrito(codEleccion='+codEleccion+',codCategoria='+codCategoria+')')    
    conn = await bd(codEleccion)
    distritos = await conn.fetch(Distritos(codCategoria).xCodDistrito())
    cod_distritos = [d['cod_distrito'] for d in distritos]
    
    for cod_distrito in cod_distritos:
        print('### '+cod_distrito)
        values = await conn.fetch(ResultadosSecciones(codCategoria, cod_distrito).xCategoriaDistritoSeccionVotos())
        if not values:
            print(f"No se existen datos para {codEleccion} and categoría {codCategoria}")
            return
        generar_n_clusters(values, codEleccion, ['cod_categoria', 'cod_distrito', 'cod_seccion'], 'secciones', archClasif, archPerfil, codCategoria + ',' + cod_distrito)

    await conn.close()
    archClasif.close()
    archPerfil.close()
    return values

async def recorrerCircuitosXDistrito(codEleccion, codCategoria):
    archClasif = open('clasificacion_distrito.csv', 'a') 
    archPerfil = open('perfil_distrito.csv', 'a') 
    print('## CircuitosXDistrito(codEleccion='+codEleccion+',codCategoria='+codCategoria+')')    
    conn = await bd(codEleccion)
    distritos = await conn.fetch(Distritos(codCategoria).xCodDistrito())
    cod_distritos = [d['cod_distrito'] for d in distritos]
    
    for cod_distrito in cod_distritos:
        print('### '+cod_distrito)
        values = await conn.fetch(ResultadosCircuitos(codCategoria,cod_distrito).xCategoriaDistritoSeccionCircuitoVotos())
        if not values:
            print(f"No se existen datos para {codEleccion} and categoría {codCategoria}")
            return
        generar_n_clusters(values, codEleccion, ['cod_categoria', 'cod_distrito', 'cod_seccion', 'cod_circuito'], 'circuitos', archClasif, archPerfil, codCategoria + ',' + cod_distrito)

    await conn.close()
    archClasif.close()
    archPerfil.close()
    return values

if __name__ == "__main__":
    import asyncio

    print('# Informe de generación de perfiles')
    # Para cada elección o base de datos, se genera un archivo para los perfiles de secciones y un archivo para los de circuitos
    for eleccion in BD_ELECCIONES:
    #if eleccion.cod_eleccion == "e2023_generales":
        # Presidente por país, sólo esta categoría me permite clasificar por país porque los distritos tienen las mismas agrupaciones
        if eleccion.cod_eleccion in ["e2015_paso", "e2015_generales", "e2019_paso", "e2019_generales", "e2023_paso", "e2023_generales", "e2023_2vuelta"]:
            asyncio.run(recorrerCircuitosXPais(eleccion.cod_eleccion,"01"))
            asyncio.run(recorrerSeccionesXPais(eleccion.cod_eleccion,"01"))

        #  Dip Nac por Distrito, excluyo elecciones provinciales
        if eleccion.cod_eleccion not in ["e2021_jujuy", "e2023_jujuy", "e2023_neuquen", "e2023_rionegro", "e2023_2vuelta"]:
            asyncio.run(recorrerCircuitosXDistrito(eleccion.cod_eleccion,"03"))
            asyncio.run(recorrerSeccionesXDistrito(eleccion.cod_eleccion,"03"))

        #  Gob por Distrito, incluyo elecciones provinciales
        if eleccion.cod_eleccion not in ["e2017_paso","e2017_generales","e2021_paso","e2021_jujuy","e2023_2vuelta"]:
            asyncio.run(recorrerCircuitosXDistrito(eleccion.cod_eleccion,"04"))
            asyncio.run(recorrerSeccionesXDistrito(eleccion.cod_eleccion,"04"))

        #  Dip Prov por Distrito, incluyo elecciones provinciales
        if eleccion.cod_eleccion not in ["e2017_generales","e2023_2vuelta"]:
            asyncio.run(recorrerCircuitosXDistrito(eleccion.cod_eleccion,"06"))
            asyncio.run(recorrerSeccionesXDistrito(eleccion.cod_eleccion,"06"))
