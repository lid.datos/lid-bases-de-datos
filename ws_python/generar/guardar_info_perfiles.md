# Sobre las clasificaciones y perfiles (clusterización y clusters)

- **Lo único que vamos a estar clasificando son secciones y circuitos**
- **Se puede clasificar por país y por distrito**

Las clasificaciones pueden hacer para todo el país para el caso de la única categoría nacional (presidente) o por distrito para el resto de las categorías que son propias de cada distrito.

## Ejemplo de info de un perfil/cluster

Ver informe con todos los perfiles generados: [Informe de generación de perfiles](../../informe_generacion_perfiles.md)

Donde un perfil/cluster en particular tiene información como esta:
```
Cluster 02
Tamaño: 11 puntos
Varianza: 0.2514655647382918
Distancia media al centroide: 1.4459668128208008
    Agrupación: JUNTOS POR EL CAMBIO - Media: 42.3591, Desvío estándar: 0.8824
    Agrupación: UNION POR LA PATRIA - Media: 34.3773, Desvío estándar: 1.0004
    Agrupación: LA LIBERTAD AVANZA - Media: 16.7818, Desvío estándar: 0.6909
    Agrupación: FRENTE DE IZQUIERDA Y DE TRABAJADORES - UNIDAD - Media: 6.4800, Desvío estándar: 0.3567
    Agrupación: blancos - Media: 5.6655, Desvío estándar: 0.3126
    Agrupación: nulos - Media: 0.6673, Desvío estándar: 0.0751
    Agrupación: comando - Media: 0.0727, Desvío estándar: 0.0420
    Agrupación: recurridos - Media: 0.0264, Desvío estándar: 0.0150
    Agrupación: impugnados - Media: 0.0064, Desvío estándar: 0.0150
```
Donde las agrupaciones se guardan en la tabla correspondiente en un array de longitud variable.

## Tablas para guardar información 

Por clasificación y perfil (es decir clusterización/cluster)

### La info de todos los clusters lo guardo en clasificación:
```sql
CREATE TABLE IF NOT EXISTS CLASIFICACION_PAIS (
COD_ELECCION  VARCHAR(20)   NOT NULL,
COD_CATEGORIA VARCHAR(2)    NOT NULL,
COD_ALGORITMO VARCHAR(4)    NOT NULL,
ELEMENTOS     VARCHAR(10)   NOT NULL,
INERCIA       NUMERIC(11,3) NOT NULL,
SILHOUETTE    NUMERIC(4,3)  NOT NULL);

ALTER TABLE CLASIFICACION_PAIS ADD PRIMARY KEY(COD_ELECCION,COD_CATEGORIA,COD_ALGORITMO,ELEMENTOS);

CREATE TABLE IF NOT EXISTS CLASIFICACION_DISTRITO (
COD_ELECCION  VARCHAR(20) NOT NULL,
COD_CATEGORIA VARCHAR(2)  NOT NULL,
COD_DISTRITO  VARCHAR(2)  NOT NULL,
COD_ALGORITMO VARCHAR(4)  NOT NULL,
ELEMENTOS     VARCHAR(10) NOT NULL,
INERCIA       NUMERIC(11,3) NOT NULL,
SILHOUETTE    NUMERIC(4,3)  NOT NULL);

ALTER TABLE CLASIFICACION_DISTRITO ADD PRIMARY KEY(COD_ELECCION,COD_CATEGORIA,COD_DISTRITO,COD_ALGORITMO,ELEMENTOS);
```

### La info por cada cluster la guardo en perfil:
Creamos un tipo de datos para la info por agrupación:
```sql
CREATE TYPE PERFIL_AGRUPACION AS (
    AGRUPACION VARCHAR(255),
    MEDIA NUMERIC(5,2),
    DESVIO_STD NUMERIC(6,3)
);

CREATE TABLE IF NOT EXISTS PERFIL_PAIS (
COD_ELECCION  VARCHAR(20) NOT NULL,
COD_CATEGORIA VARCHAR(2)  NOT NULL,
COD_ALGORITMO VARCHAR(4)  NOT NULL,
ELEMENTOS     VARCHAR(10) NOT NULL,
PERFIL        VARCHAR(2)  NOT NULL,
CANTIDAD      INTEGER     NOT NULL,
VARIANZA      NUMERIC(7,3) NOT NULL,
DIST_MEDIA_CENTRO NUMERIC(6,3) NOT NULL,
DESC_BREVE    VARCHAR(100)     NOT NULL,
AGRUPACIONES PERFIL_AGRUPACION[]);

ALTER TABLE PERFIL_PAIS ADD PRIMARY KEY(COD_ELECCION,COD_CATEGORIA,COD_ALGORITMO,ELEMENTOS,PERFIL);


CREATE TABLE IF NOT EXISTS PERFIL_DISTRITO (
COD_ELECCION  VARCHAR(20) NOT NULL,
COD_CATEGORIA VARCHAR(2)  NOT NULL,
COD_DISTRITO  VARCHAR(2)  NOT NULL,
COD_ALGORITMO VARCHAR(4)  NOT NULL,
ELEMENTOS     VARCHAR(10) NOT NULL,
PERFIL        VARCHAR(2)  NOT NULL,
CANTIDAD      INTEGER     NOT NULL,
VARIANZA      NUMERIC(7,3) NOT NULL,
DIST_MEDIA_CENTRO NUMERIC(6,3) NOT NULL,
DESC_BREVE        VARCHAR(100) NOT NULL,
AGRUPACIONES PERFIL_AGRUPACION[]);

ALTER TABLE PERFIL_DISTRITO ADD PRIMARY KEY(COD_ELECCION,COD_CATEGORIA,COD_DISTRITO,COD_ALGORITMO,ELEMENTOS,PERFIL);
```
