# Generar Perfiles

Dado un conjunto de bases de datos de resultados de elecciones, se generan una serie de "Perfiles de votos" por Sección y Circuito.

Para generar estos perfiles utilizaremos el método de clusterización, o agrupamiento, una técnica de aprendizaje no supervisado utilizada en la ciencia de datos y el aprendizaje automático para agrupar objetos o datos en conjuntos llamados "clusters" (o grupos), de modo que los objetos dentro de un mismo cluster sean más similares entre sí que a los de otros clusters. Esta técnica es ampliamente utilizada para identificar patrones y estructuras ocultas en datos sin etiquetas. Para eso usamos el algoritmo K-Means con la librería Pandas de Python.

## Cantidad de Perfiles

Se eligieron arbitrariamente la siguiente cantidad de perfiles para agrupar los resultados de cada universo. La clasificaciones por país se realizan sólo para la categoría presidente.

| Generaciones de perfiles    | Cantidad de perfiles |
|-----------------------------|----------------------:|
| [Circuitos x Pais](generar_perfiles.py#L50) | 10 |
| [Secciones x Pais](generar_perfiles.py#L63) | 10 |
| [Secciones x Distrito](generar_perfiles.py#L76) | 3 |
| [Circuitos x Distrito](generar_perfiles.py#L99) | 10 |

# Generación de Perfiles

- Para cada elección realizamos la consulta de resultados a nivel de Sección y Circuito y clasificamos los resultados en clusters (perfiles).
- En el caso de la categoría Presidente (01), la clasificación de los resultados la hacemos para todo el país.
- Para el resto de las categorías, hacemos la clasificación a nivel distrito, porque las listas son diferentes en cada distrito.

Acá se pueden ver, como se generaron los perfiles en el archivo [generar_perfiles.py](generar_perfiles.py)
```python
    # Para cada elección o base de datos, se genera un archivo para los perfiles de secciones y un archivo para los de circuitos
    for eleccion in BD_ELECCIONES:
        # Presidente por país, sólo esta categoría me permite clasificar por país porque los distritos tienen las mismas agrupaciones
        if eleccion.cod_eleccion in ["e2015_paso", "e2015_generales", "e2019_paso", "e2019_generales", "e2023_paso", "e2023_generales", "e2023_2vuelta"]:
            asyncio.run(recorrerCircuitosXPais(eleccion.cod_eleccion,"01"))
            asyncio.run(recorrerSeccionesXPais(eleccion.cod_eleccion,"01"))

        #  Dip Nac por Distrito, excluyo elecciones provinciales
        if eleccion.cod_eleccion not in ["e2021_jujuy", "e2023_jujuy", "e2023_neuquen", "e2023_rionegro", "e2023_2vuelta"]:
            asyncio.run(recorrerCircuitosXDistrito(eleccion.cod_eleccion,"03"))
            asyncio.run(recorrerSeccionesXDistrito(eleccion.cod_eleccion,"03"))

        #  Gob por Distrito, incluyo elecciones provinciales
        if eleccion.cod_eleccion not in ["e2017_paso","e2017_generales","e2021_paso","e2021_jujuy","e2023_2vuelta"]:
            asyncio.run(recorrerCircuitosXDistrito(eleccion.cod_eleccion,"04"))
            asyncio.run(recorrerSeccionesXDistrito(eleccion.cod_eleccion,"04"))

        #  Dip Prov por Distrito, incluyo elecciones provinciales
        if eleccion.cod_eleccion not in ["e2017_generales","e2023_2vuelta"]:
            asyncio.run(recorrerCircuitosXDistrito(eleccion.cod_eleccion,"06"))
            asyncio.run(recorrerSeccionesXDistrito(eleccion.cod_eleccion,"06"))
```

## Referencias
- [Instalación de librerías, ejecución en docker y unificación de csv para tablas](instalacion.md)
- [Análisis de K-Means y obtención de métricas por cluster](kmeans.md)
- [Informe de generación de perfiles](../../informe_generacion_perfiles.md)

# Analizando Perfiles: Paso a paso

Para hacer una análisis de los perfiles por sección, detallamos los pasos seguidos a partir de los perfiles generados.

Tomamos como ejemplo los Perfiles por Sección para las Elecciones Generales 2023, categoría Presidente. Esto mismo puede realizarse por ejemplo para los análisis de los Perfiles por Circuito.

## 1. Descargamos consulta de perfiles

1. Descargamos la consulta del sitio web del Observatorio de Datos y la subimos a siguiente hoja de cálculos: 
- [Perfiles por Sección (10) - Elecciones generales 2023 - Categoría Presidente](https://docs.google.com/spreadsheets/d/1Sh-FuqIwKw8WSdmsXadeyDl0W14hY8Ts2C1vHw4AE5E)

Se pueden encontrar dos pestañas: Perfiles por Sección y Resultados por Pefil

## 2. Calculamos los Resultados de cada Perfil

Para un análisis más detallado de cada perfil, explicamos como generamos los Resultados por Perfil.

2.1) Para cada perfil (secciones con comportamiento similar), sumamos la cantidad de votos y calculamos el porcentaje correspondiente.

2.2) De esta manera obtenemos el Resultado por Perfil, como si dividieramos al país en 10 provincias, según su comportamiento, y le calcularamos el total de votos y su porcentaje.

2.3) Los perfiles están hechos por porcentajes, independientemente de la cantidad de votos.

2.4) Para los Resultados por Perfil, se suman los votos de las secciones de cada perfil, con lo cual adquieren más peso las secciones con más electores, en la incidencia del porcentaje calculado.

2.5) Con esto podemos ver como es el comportamiento por perfil y sacar algunas conclusiones.

Los pasos técnicos que realizamos para generar esto resultados, están detallados en la siguiente documentación [Consultando las Bases Datos](../../consultando_las_bases_de_datos.md), que requiere tener la infraestructura instalada y conocimientos técnicos avanzados.

## 3. Le ponemos nombres y colores a los perfiles
Le ponemos un nombre al perfil asociado a las regiones más representativas en donde se puede encontrar. Y le asociamos el color correspondiente a cada perfil.

## 4. Analizamos los resultados de cada perfil

|Perfil|color|Nombre|Descripción|
|------|-----|------|-----------|
|00|#4328e7|Misiones|Alto nivel de LLA y Uxp y bajos niveles de JxC|
|01|#9654e5|CABA Centro Norte|Alto nivel de JxC, luego UxP y luego LLA|
|02|#ff6283|Santiago del Estero|Muy alto nivel de UxP, muy por debajo LLA y luego JxC|
|03|#ff8800|Córdoba|Se diferencia x voto Scharetti, ordenados x LLA, HACEMOS, JxC y UxP|
|04|#ffc502|Interior PBA y Entre Ríos|Alto nivel de UxP, luego LLA y JxC parejos|
|05|#007d8e|GBA, Corrientes y Chaco|Encabeza LLA, pero sin mucha diferencia con JxC y UxP|
|06|#1aa7ee|La Rioja, Tucumán, Río Negro y GBA|Bastante alto nivel de UxP, por debajo LLA y luego JxC. Similar al 02 pero menos acentuado.|
|07|#29dae4|Cuyo y Santa Fé|LLA casi duplica a UxP y JxC|
|08|#88e99a|Jujuy, Salta, Sta Cruz y Río Negro|Encabeza UxP, un poco por detrás LLA, la mitad JxC|
|09|#019c00|Catamarca, Chubut, PBA, Sur CABA|Encabeza UxP, bastante por detrás LLA y JxC|

Aclaración: Los colores elegidos (códigos de color), los tomamos de una opción por defecto del mapa (en flourish, paleta de colores), para que se puedan visualizar bien en el mapa.





