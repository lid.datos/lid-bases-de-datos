# KMeans
El algoritmo **KMeans NO es completamente determinista**, a menos que establezcas una semilla para la inicialización de los centroides. Por defecto, KMeans elige los centroides iniciales de manera aleatoria, lo que puede llevar a diferentes resultados en diferentes ejecuciones, incluso si usas las mismas muestras.

## Causa de la variabilidad en KMeans
El algoritmo puede producir diferentes resultados en cada ejecución debido a:
- **Inicialización de los centroides**: Los puntos iniciales seleccionados pueden variar, y eso afecta cómo se forman los clusters.
- **Aleatoriedad**: La fase inicial de asignación de los centroides se basa en un proceso aleatorio, lo que explica las diferencias entre ejecuciones.

## Hacer KMeans determinista
Para hacer KMeans determinista y obtener siempre los mismos resultados con las mismas muestras, puedes usar el parámetro ```random_state```. Esto fija la semilla del generador aleatorio, asegurando que la inicialización de los centroides sea siempre la misma.

Ejemplo en Python con ```random_state```:
```python
from sklearn.cluster import KMeans
import pandas as pd

# Definir el número de clusters
n_clusters = 5

# Crear el modelo KMeans con una semilla fija
kmeans = KMeans(n_clusters=n_clusters, random_state=42)

# Ajustar el modelo a los datos
kmeans.fit(data)

# Obtener los clusters
clusters = kmeans.predict(data)
```

En este caso, ```random_state=42``` asegura que la inicialización sea siempre la misma, por lo que las ejecuciones sucesivas del algoritmo sobre las mismas muestras deberían generar resultados consistentes.

Resumen:
- **KMeans no es determinista por defecto**, ya que la inicialización de los centroides es aleatoria.
- **Usar** ```random_state``` en el modelo de KMeans asegura que los resultados sean repetibles.

---

# Estrategias de Inicialización en KMeans
La inicialización en el algoritmo **KMeans** es crucial para obtener buenos resultados, ya que influye directamente en la calidad de los clusters formados. Si bien el algoritmo no garantiza encontrar la mejor solución global, existen métodos para mejorar las posibilidades de encontrar una inicialización óptima.

- **Inicialización Aleatoria (KMeans clásico)**: En el método clásico, los centroides iniciales se seleccionan de forma aleatoria entre los puntos de datos. Esto puede llevar a resultados muy diferentes en diferentes ejecuciones, y a veces, a una convergencia en un óptimo local no deseado.
- **KMeans++ (el valor predeterminado en la mayoría de las implementaciones)**: 
    - **KMeans++** es una mejora sobre la inicialización aleatoria, que elige los centroides iniciales de manera que estén lo más alejados posible entre sí. Esto mejora significativamente las posibilidades de encontrar una mejor solución inicial y reduce el riesgo de caer en óptimos locales.
    - **KMeans++** es una estrategia recomendada porque, en muchos casos, se acerca a la inicialización óptima. Está activado por defecto en la mayoría de las implementaciones de KMeans, incluidas las de ```scikit-learn```.

Ejemplo en Python con KMeans++ (configuración por defecto):
```python
from sklearn.cluster import KMeans

# Usar KMeans++ para inicialización
kmeans = KMeans(n_clusters=5, init='k-means++', random_state=42)

# Ajustar el modelo
kmeans.fit(data)
```

## Evaluar la calidad de la inicialización
Aunque no puedes saber con certeza si la inicialización es "óptima" en un sentido global, puedes evaluar la calidad de la clusterización con las siguientes métricas:

### 1) Inercia (Within-Cluster Sum of Squares - WCSS)
Es la suma de las distancias cuadráticas entre los puntos y sus centroides. Una menor inercia indica clusters más compactos y una mejor solución. Puedes usar la inercia para comparar diferentes inicializaciones:
```python
print(kmeans.inertia_)
```
### 2) Número de reinicios (n_init)
Para aumentar las posibilidades de encontrar una inicialización cercana al óptimo, puedes ejecutar el algoritmo varias veces con diferentes inicializaciones y seleccionar la mejor:
```python
kmeans = KMeans(n_clusters=5, n_init=10, random_state=42)
kmeans.fit(data)
```
El parámetro ```n_init``` determina cuántas veces se ejecuta el algoritmo con diferentes inicializaciones. El mejor resultado, en términos de inercia, se selecciona automáticamente.

### 3) El método del codo (Elbow Method)
Puedes ejecutar KMeans con diferentes números de clusters y calcular la inercia para cada uno. Luego, graficas el número de clusters frente a la inercia y buscas un "codo" en la curva. Este punto sugiere el número óptimo de clusters.
```python
import matplotlib.pyplot as plt

distortions = []
K = range(1, 10)
for k in K:
    kmeans = KMeans(n_clusters=k, random_state=42)
    kmeans.fit(data)
    distortions.append(kmeans.inertia_)

plt.plot(K, distortions, 'bx-')
plt.xlabel('Número de Clusters')
plt.ylabel('Inercia')
plt.title('Método del Codo para encontrar el número óptimo de Clusters')
plt.show()
```
### Resumen
- **KMeans++** es una estrategia avanzada de inicialización que mejora las posibilidades de encontrar buenos clusters.
- No puedes garantizar una inicialización óptima en todos los casos, pero puedes evaluar la calidad de los clusters usando métricas como **inercia** y ejecutando varias inicializaciones con ```n_init```.
- Usa el **método del codo** para determinar el número de clusters y asegurarte de que los resultados sean estables.

---

# Conclusión final

### 1) Fijamos ```random_state```
Fijando el random_state hacemos que el algoritmo sea determinista, es decir que de siempre el resultado para distintas corridas, sin afectar la calidad de la clusterización, pero que nos permite mejorar la trazabilidad de los resultados, es decir que cualquiera pueda volver a generarlos obteniendo mismos resultados.

### 2) Tomamos ```init='k-means++'```
Por lo visto, sugieren usar k-means++ que mejora la clasificación para un número determinado de clusters.

Con lo que nos queda:
```python
kmeans = KMeans(n_clusters=n_clusters, init='k-means++', random_state=50)
```

### 3) Agregamos métricas
En cuanto a la evaluación de la calidad de la clusterización sugieren ambas métricas: **Inércia (para Método del Codo)** y **Silhouette Score**, la idea será entonces generar en otro csv por clasificación con estas métricas: 
```python
print(f"Clusters: {n_clusters} | Inercia: {inertia} | Silhouette Score: {silhouette_avg}")
```
Esto lo integraremos a una nueva tabla.

## Ejemplo de código

Este código iterará sobre diferentes números de clusters, calculará la inercia y el Silhouette Score, y te permitirá visualizar los resultados para que puedas tomar una decisión sobre la cantidad óptima de clusters.

```python
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score

# Supongamos que tienes un DataFrame de pandas o un array de numpy con tus datos
# data = ... (Carga o genera tus datos)

# Definir el rango de número de clusters a evaluar
range_clusters = range(2, 11)

# Inicializar listas para almacenar las métricas
inertia_values = []
silhouette_scores = []

# Ejecutar KMeans para diferentes números de clusters y calcular las métricas
for n_clusters in range_clusters:
    kmeans = KMeans(n_clusters=n_clusters, init='k-means++', random_state=50)
    
    # Ajustar el modelo KMeans
    kmeans.fit(data)
    
    # Calcular la inercia (Within-Cluster Sum of Squares)
    inertia = kmeans.inertia_
    inertia_values.append(inertia)
    
    # Calcular Silhouette Score
    labels = kmeans.labels_
    silhouette_avg = silhouette_score(data, labels)
    silhouette_scores.append(silhouette_avg)
    
    print(f"Clusters: {n_clusters} | Inercia: {inertia} | Silhouette Score: {silhouette_avg}")

# Graficar la inercia y el Silhouette Score
plt.figure(figsize=(12, 6))

# Gráfica de la inercia (método del codo)
plt.subplot(1, 2, 1)
plt.plot(range_clusters, inertia_values, 'bx-')
plt.xlabel('Número de Clusters')
plt.ylabel('Inercia (WCSS)')
plt.title('Método del Codo (Inercia)')

# Gráfica del Silhouette Score
plt.subplot(1, 2, 2)
plt.plot(range_clusters, silhouette_scores, 'bx-')
plt.xlabel('Número de Clusters')
plt.ylabel('Silhouette Score')
plt.title('Silhouette Score para cada número de clusters')

plt.tight_layout()
plt.show()
```

## Explicación del código

### 1) Iteración sobre el rango de clusters (```range(2, 11)```)
Estamos evaluando números de clusters desde 2 hasta 10. Puedes ajustar este rango según lo que te interese evaluar.

### 2) Cálculo de la inercia (```kmeans.inertia_```)
La inercia mide cuán compactos están los clusters. Menor inercia significa que los puntos están más cerca de sus centroides, pero un valor muy bajo puede implicar un sobreajuste.

### 3) Cálculo del Silhouette Score (```silhouette_score```):
El Silhouette Score mide la cohesión y separación entre clusters. Un valor cercano a **1** indica que los clusters están bien separados, mientras que un valor cercano a **-1** indica que los clusters se superponen.

### 4) Visualización de los resultados:
La primera gráfica muestra el **método del codo**, donde puedes buscar el punto donde la inercia deja de disminuir significativamente.
La segunda gráfica muestra el **Silhouette Score**, donde un valor más alto indica una mejor separación entre clusters.

## Interpretación de los resultados:
- **Método del codo**: Busca el "codo" en la curva de inercia, donde la disminución se desacelera. Ese punto sugiere el número óptimo de clusters.
- **Silhouette Score**: Busca el número de clusters donde el Silhouette Score sea más alto, lo que indica una mejor separación de los clusters.

## Resumen
El código evalúa inercia y Silhouette Score para un rango de valores de clusters.
Usa ambas métricas para determinar el número óptimo de clusters, dependiendo de si prefieres un análisis visual del codo o una separación mejor con Silhouette Score.

# Agregamos más métricas

- **Inercia**: La suma de las distancias cuadráticas dentro de cada cluster.
- **Silhouette Score**: Mide cómo de bien separados están los clusters.
- **Tamaño de cada cluster**: El número de puntos en cada cluster.
- **Media de porcentajes por cluster**: Calcula la media de los porcentajes de cada agrupación por cluster.
- **Desviación estándar por cluster**: La variabilidad dentro de cada cluster.
- **Varianza intra-cluster**: Mide la dispersión dentro de cada cluster.