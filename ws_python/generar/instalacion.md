# Avances en implementación de Machine Learning

1) Se agregaron librerías en requirements.txt
2) Pero puede ser necesario agregarlas a mano desde el contenedor.
3) Para eso es necesario levantar los contenedores e ingresar para instalarlo.
4) Desde el contenedor levantado, se puede correr el ejemplo que lee circuitos de la Comuna 1 desde la base de datos.

### Versiones de librerías agregadas en requirements.txt
- pandas - Version: 2.1.1
- numpy - Version: 1.22.4
- scikit-learn - Version: 0.24.2

### Acceder a contenedor para probar
```
docker ps
docker exec -it web /bin/bash
```
### Chequear versiones de librerías instaladas
```
pip show pandas
pip show numpy
pip show scikit-learn
```
### Agregar manualmente en caso de ser necesario
Puede ser que no tome bien las actualizaciones de librerías de requirements.txt, por lo que puede ser necesario agregarlas a mano desde el contenedor.
```
pip install pandas==2.1.1
pip install numpy==1.22.4
pip install scikit-learn==0.24.2
```
### Correr ejemplo en el contenedor
```
docker exec -it web /bin/bash
cd generar
python generar_perfiles.py
```
### Unificar CSVs
```
cat circuitos*.csv | sort > perfiles_circuitos.csv
cat secciones*.csv | sort > perfiles_secciones.csv
```
### Comprimir archivos csv
```
tar -czf perfiles.tar.gz *.csv
```

