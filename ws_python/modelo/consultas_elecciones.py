# Archivo para probar objetos de elecciones.py
from calendar import c
from pickle import TRUE
import elecciones

def ingresarOpciones():
    print("1) categorias")
    print("2) distritos")
    print("3) secciones")
    print("4) secciones proviniciales")
    print("5) circuitos")
    print("6) mesas")
    print("7) agrupaciones")
    print("8) agrupaciones sublistas")
    print("11) resultados totales")
    print("12) resultados totales sublistas")
    print("13) resultados totales mesas")
    print("14) resultados distritos")
    print("15) resultados distritos sublistas")
    print("16) resultados distritos mesas")
    print("17) resultados secciones")
    print("18) resultados secciones sublistas")
    print("19) resultados secciones mesas")
    print("20) resultados circuitos")
    print("21) resultados circuitos sublistas")
    print("22) resultados circuitos mesas")
    print("23) resultados mesas")
    print("24) resultados mesas sublistas")
    print("25) resultados mesas mesa")
    print("26) resultados distritos x Col")
    print("27) resultados secciones x Col")
    print("28) resultados circuitos x Col")
    print("0) Salir")
    op=input("Ingresar opción: ")
    if op=="": return 0
    return op

opcion=ingresarOpciones()
while opcion != "0":
    if opcion == "1":
        print(elecciones.Categorias().xCodCategoria())
        #print(elecciones.Categorias("01").xCodCategoria())
        #print(elecciones.Categorias("02","002").xCodCategoria())
    if opcion == "2":
        print(elecciones.Distritos().xCodDistrito())
        #print(elecciones.Distritos("03").q)
        #print(elecciones.Distritos(True).q)
    if opcion == "3":
        print(elecciones.Secciones("01").xCodSeccion())
        #print(elecciones.Secciones("02",None,"3").xCodSeccionProv())
        #print(elecciones.Secciones("02").xCodSeccionProv())
    if opcion == "4":
        print(elecciones.SeccionesProvinciales("01").xCodSeccionProv())
        #print(elecciones.SeccionesProvinciales("02").xCodSeccionProv())
    if opcion == "5":
        print(elecciones.Circuitos("01").xCodCircuito())
        #print(elecciones.Circuitos("02","001").xCodCircuito())
    if opcion == "6":
        print(elecciones.Mesas("02","001","00001").xCodMesa())
    if opcion == "7":
        print(elecciones.Agrupaciones("01","03").xCodAgrupacion())
    if opcion == "8":
        print(elecciones.AgrupacionesSublistas("01","03").xCodAgrupacionCodLista())
    if opcion == "11":
        print(elecciones.ResultadosTotales("01").xCategoriaVotos())
    if opcion == "12":
        print(elecciones.ResultadosTotalesSublistas("01").xCategoriaVotos())
    if opcion == "13":
        print(elecciones.ResultadosTotalesMesas("01").xCategoria())
    if opcion == "14":
        print(elecciones.ResultadosDistritos("03","01").xCategoriaDistritoVotos())
    if opcion == "15":
        print(elecciones.ResultadosDistritosSublistas("03","01").xDistritoCategoriaVotos())
        #print(elecciones.ResultadosDistritosSublistas("03",True,"0503").xDistritoCategoriaVotos())
    if opcion == "16":
        print(elecciones.ResultadosDistritosMesas("03","01").xCategoriaDistritoVotos())
    if opcion == "17":
        print(elecciones.ResultadosSecciones("03","01","001").xCategoriaDistritoSeccionVotos())
        #print(elecciones.ResultadosSecciones("03","01","001","0503").xCategoriaDistritoSeccionVotos())
    if opcion == "18":
        print(elecciones.ResultadosSeccionesSublistas("03","01","001").xCategoriaDistritoSeccionVotos())
    if opcion == "19":
        print(elecciones.ResultadosSeccionesMesas("03","01").xCategoriaDistritoSeccionesVotos())
    if opcion == "20":
        #print(elecciones.ResultadosCircuitos("03","01","001","00001").xCategoriaDistritoSeccionCircuitoVotos())
        print(elecciones.ResultadosCircuitos("03","01","001").xCategoriaDistritoSeccionCircuitoVotos())
    if opcion == "21":
        print(elecciones.ResultadosCircuitosSublistas("03","01","001","00001").xCategoriaDistritoSeccionCircuitoVotos())
    if opcion == "22":
        print(elecciones.ResultadosCircuitosMesas("03","01","001","00001").xCategoriaDistritoSeccionesCircuitosVotos())
    if opcion == "23":
        print(elecciones.ResultadosMesas("01","001","00001","03","00001X").xMesaCategoriaVotos())
        #print(elecciones.ResultadosMesas("02","001","00001","03").xMesaCategoriaVotos())
    if opcion == "24":
        print(elecciones.ResultadosMesasSublistas("01","001","00001","03","00001X").xMesaCategoriaVotos())
        #print(elecciones.ResultadosMesasSublistas("02","001","00001","03").xMesaCategoriaVotos())
    if opcion == "25":
        print(elecciones.ResultadosMesasMesa("01","001","00001","03","00001X").xMesaCategoriaVotos())
    if opcion == "26":
        print(elecciones.AgrupacionesDistritosxCol("01").xVotos())
        print(elecciones.ResultadosDistritosxCol("01").xCategoriaDistrito())
    if opcion == "27":
        print(elecciones.AgrupacionesSeccionesxCol("03","01","001").xVotos())
        print(elecciones.ResultadosSeccionesxCol("03","01","001").xCategoriaDistritoSeccion())
    if opcion == "28":
        print(elecciones.AgrupacionesCircuitosxCol("03","01","001","00001").xVotos())
        print(elecciones.ResultadosCircuitosxCol("03","01","001","00001").xCategoriaDistritoSeccionCircuito())
    print()
    opcion=ingresarOpciones()
