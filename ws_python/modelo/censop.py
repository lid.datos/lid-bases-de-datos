def condicion(self,columna,valor):
    if "WHERE" in self.q:
        condicion="AND"
    else:
        condicion="WHERE"
    if (valor is not None):
        self.q+="\n"
        if isinstance(valor,str):
            self.q+=condicion+" "+columna+" = '"+valor+"'"
        else:
            self.i+=1
            self.q+=condicion+" "+columna+" = $"+str(self.i)
def like(self,columna,valor):
    if "WHERE" in self.q:
        condicion="AND"
    else:
        condicion="WHERE"
    if (valor is not None):
        self.q+="\n"
        if isinstance(valor,str):
            self.q+=condicion+" "+columna+" LIKE '"+valor+"'"
        else:
            self.i+=1
            self.q+=condicion+" "+columna+" LIKE $"+str(self.i)

class Provincias:
    def __init__(self,provincia=None):
        self.i = 0
        self.q = """
SELECT cod_provincia,nom_provincia
FROM provincias"""
        condicion(self,"cod_provincia",provincia)
    def xCodigo(self):
        return self.q+"\nORDER BY cod_provincia"
    def xNom(self):
        return self.q+"\nORDER BY nom_provincia"
class Departamentos:
    def __init__(self,provincia=None,departamento=None):
        self.i = 0
        self.q = """
SELECT cod_provincia,cod_departamento,nom_departamento
FROM departamentos"""
        condicion(self,"cod_provincia",provincia)
        condicion(self,"cod_departamento",departamento)
    def xCodigo(self):
        return self.q+"\nORDER BY cod_provincia,cod_departamento"
    def xNom(self):
        return self.q+"\nORDER BY nom_departamento"
class Censo:
    def __init__(self,provincia=None,departamento=None):
        self.i = 0
        self.q = """
SELECT c.cod_provincia
     , p.nom_provincia
     , c.cod_departamento
     , d.nom_departamento
     , c.viviendas_particulares
     , c.viviendas_colectivas
     , c.poblacion_viviendas_particulares
     , c.poblacion_viviendas_colectivas
     , c.poblacion_situacion_calle
     , c.mujeres
     , c.varones
     , c.generox
FROM censo c, provincias p, departamentos d
WHERE c.cod_provincia = p.cod_provincia
AND c.cod_provincia = d.cod_provincia
AND c.cod_departamento = d.cod_departamento"""
        condicion(self,"c.cod_provincia",provincia)
        condicion(self,"c.cod_departamento",departamento)
    def xCodigo(self):
        return self.q+"\nORDER BY c.cod_provincia,c.cod_departamento"
