# perfiles.py

### PerfilesCircuitosPerfiles ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None

### PerfilesCircuitosCircuitos ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None

### PerfilesCircuitosSecciones ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None

### PerfilesCircuitosDistritos ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None

### PerfilesCircuitosCategorias ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None

### PerfilesCircuitosElecciones ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None

### PerfilesCircuitosAlgoritmos ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None

### PerfilesCircuitos ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None

### PerfilesSeccionesPerfiles ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None

### PerfilesSeccionesSecciones ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None

### PerfilesSeccionesDistritos ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None

### PerfilesSeccionesCategorias ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None

### PerfilesSeccionesElecciones ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None

### PerfilesSeccionesAlgoritmos ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None

### PerfilesSecciones ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None

### ClasificacionPais ###
    cod_eleccion=None,cod_categoria=None,cod_algoritmo=None,elementos=None

### ClasificacionDistrito ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_algoritmo=None,elementos=None

### PerfilPais ###
    cod_eleccion=None,cod_categoria=None,cod_algoritmo=None,elementos=None,perfil=None

### PerfilDistrito ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_algoritmo=None,elementos=None,perfil=None

### ClasificacionPerfilPais ###
    cod_eleccion=None,cod_categoria=None,cod_algoritmo=None,elementos=None,perfil=None

### ClasificacionPerfilDistrito ###
    cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_algoritmo=None,elementos=None,perfil=None
