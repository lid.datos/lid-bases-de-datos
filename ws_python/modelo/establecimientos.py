def condicion(self,columna,valor):
    if "WHERE" in self.q:
        condicion="AND"
    else:
        condicion="WHERE"
    if (valor is not None):
        self.q+="\n"
        if isinstance(valor,str):
            self.q+=condicion+" "+columna+" = '"+valor+"'"
        else:
            self.i+=1
            self.q+=condicion+" "+columna+" = $"+str(self.i)
def like(self,columna,valor):
    if "WHERE" in self.q:
        condicion="AND"
    else:
        condicion="WHERE"
    if (valor is not None):
        self.q+="\n"
        if isinstance(valor,str):
            self.q+=condicion+" "+columna+" LIKE '"+valor+"'"
        else:
            self.i+=1
            self.q+=condicion+" "+columna+" LIKE $"+str(self.i)

class Clae2:
    def __init__(self,cod_clae2=None):
        self.i = 0
        self.q = """
SELECT cod_clae2,nom_clae2
FROM clae2"""
        condicion(self,"cod_clae2",cod_clae2)
    def xCodigo(self):
        return self.q+"\nORDER BY cod_clae2"
    def xNom(self):
        return self.q+"\nORDER BY nom_clae2"
class Clae6:
    def __init__(self,cod_clae6=None):
        self.i = 0
        self.q = """
SELECT cod_clae6,nom_clae6,desc_clae6
FROM clae6"""
        condicion(self,"cod_clae6",cod_clae6)
    def xCodigo(self):
        return self.q+"\nORDER BY cod_clae6"
    def xNom(self):
        return self.q+"\nORDER BY nom_clae6"
class ClaeLetra:
    def __init__(self,cod_letra=None):
        self.i = 0
        self.q = """
SELECT cod_letra,nom_letra
FROM clae_letra"""
        condicion(self,"cod_letra",cod_letra)
    def xCodigo(self):
        return self.q+"\nORDER BY cod_letra"
    def xNom(self):
        return self.q+"\nORDER BY nom_letra"
class Rangos:
    def __init__(self,cod_rango=None):
        self.i = 0
        self.q = """
SELECT cod_rango,nom_rango
FROM rangos"""
        condicion(self,"cod_rango",cod_rango)
    def xCodigo(self):
        return self.q+"\nORDER BY cod_rango"
    def xNom(self):
        return self.q+"\nORDER BY nom_rango"
class Provincias:
    def __init__(self,cod_provincia=None):
        self.i = 0
        self.q = """
SELECT cod_provincia,nom_provincia
FROM provincias"""
        condicion(self,"cod_provincia",cod_provincia)
    def xCodigo(self):
        return self.q+"\nORDER BY cod_provincia"
    def xNom(self):
        return self.q+"\nORDER BY nom_provincia"
class Departamentos:
    def __init__(self,cod_provincia,cod_departamento=None):
        self.i = 0
        self.q = """
SELECT cod_departamento,nom_departamento
FROM departamentos"""
        if cod_provincia:
            like(self,"cod_departamento",cod_provincia+"___")
        condicion(self,"cod_departamento",cod_departamento)
    def xCodigo(self):
        return self.q+"\nORDER BY cod_departamento"
    def xNom(self):
        return self.q+"\nORDER BY nom_departamento"
class Establecimientos:
    def __init__(self,cod_provincia=None,cod_departamento=None,cod_clae2=None,cod_clae6=None,cod_letra=None,cod_rango=None):
        self.i = 0
        self.q = """
SELECT e.cod_provincia
     , p.nom_provincia
     , e.cod_departamento
     , d.nom_departamento
     , e.cod_letra
     , l.nom_letra
     , e.cod_clae2
     , c2.nom_clae2
     , e.cod_clae6
     , c6.nom_clae6
     , e.cod_rango
     , r.nom_rango
     , e.latitud
     , e.longitud
     , e.cod_establecimiento
     , e.tipo_coordenada
FROM public.establecimientos e, public.provincias p, public.departamentos d, public.rangos r, public.clae_letra l, public.clae2 c2, public.clae6 c6
WHERE e.cod_provincia = p.cod_provincia
AND e.cod_departamento = d.cod_departamento
AND e.cod_rango = r.cod_rango
AND e.cod_letra = l.cod_letra
AND e.cod_clae2 = c2.cod_clae2
AND e.cod_clae6 = c6.cod_clae6"""
        condicion(self,"p.cod_provincia",cod_provincia)
        condicion(self,"e.cod_departamento",cod_departamento)
        condicion(self,"e.cod_clae2",cod_clae2)
        condicion(self,"e.cod_clae6",cod_clae6)
        condicion(self,"e.cod_letra",cod_letra)
        condicion(self,"e.cod_rango",cod_rango)
    def xCodigo(self):
        return self.q+"\nORDER BY e.cod_establecimiento"
    def xCodProvinciaCodDepartamento(self):
        return self.q+"\nORDER BY e.cod_provincia,e.cod_departamento,e.cod_letra,e.cod_clae2,e.cod_clae6,e.cod_rango,e.cod_establecimiento"
