# Archivo para probar objetos de perfiles.py
from calendar import c
from pickle import TRUE
import perfiles

def ingresarOpciones():
    print("1) PerfilesSeccionesPerfiles")
    print("2) PerfilesSeccionesSecciones")
    print("3) PerfilesSeccionesDistritos")
    print("4) PerfilesSeccionesCategorias")
    print("5) PerfilesSeccionesAlgoritmos")
    print("6) PerfilesSeccionesElecciones")
    print("7) PerfilesSecciones")
    print("11) PerfilesCircuitosPerfiles")
    print("12) PerfilesCircuitosCircuitos")
    print("13) PerfilesCircuitosSecciones")
    print("14) PerfilesCircuitosDistritos")
    print("15) PerfilesCircuitosCategorias")
    print("16) PerfilesCircuitosElecciones")
    print("17) PerfilesCircuitosAlgoritmos")
    print("18) PerfilesCircuitos")
    print("21) ClasificacionPais")
    print("22) ClasificacionDistrito")
    print("23) PerfilPais")
    print("24) PerfilDistrito")
    print("25) ClasificacionPerfilPais")
    print("26) ClasificacionPerfilDistrito")
    print("0) Salir")
    op=input("Ingresar opción: ")
    if op=="": return 0
    return op

opcion=ingresarOpciones()
while opcion != "0":
    if opcion == "1":
        print(perfiles.PerfilesSeccionesPerfiles().xPerfil())
    if opcion == "2":
        print(perfiles.PerfilesSeccionesSecciones().xCodSeccion())
    if opcion == "3":
        print(perfiles.PerfilesSeccionesDistritos().xCodDistrito())
    if opcion == "4":
        print(perfiles.PerfilesSeccionesCategorias().xCodCategoria())
    if opcion == "5":
        print(perfiles.PerfilesSeccionesAlgoritmos().xCodAlgoritmo())
    if opcion == "6":
        print(perfiles.PerfilesSeccionesElecciones().xCodEleccion())
    if opcion == "7":
        print(perfiles.PerfilesSecciones("e2023_generales","01","02").xCodCategoriaCodDistritoCodSeccionPerfil())
    if opcion == "11":
        print(perfiles.PerfilesCircuitosPerfiles().xPerfil())
    if opcion == "12":
        print(perfiles.PerfilesSeccionesSecciones().xCodSeccion())
    if opcion == "13":
        print(perfiles.PerfilesCircuitosCircuitos().xCodCircuito())
    if opcion == "14":
        print(perfiles.PerfilesCircuitosDistritos().xCodDistrito())
    if opcion == "15":
        print(perfiles.PerfilesCircuitosCategorias().xCodCategoria())
    if opcion == "16":
        print(perfiles.PerfilesCircuitosElecciones().xCodEleccion())
    if opcion == "17":
        print(perfiles.PerfilesCircuitosAlgoritmos().xCodAlgoritmo())
    if opcion == "18":
        print(perfiles.PerfilesCircuitos().xCodCategoriaCodDistritoCodSeccionCodCircuitoPerfil())
    if opcion == "21":
        print(perfiles.ClasificacionPais("e2023_generales","01","km10").xEleccionCategoriaAlgoritmoElementos())
        print(perfiles.ClasificacionPais("e2023_generales","01","km03","secciones").xEleccionCategoriaAlgoritmoElementos())
    if opcion == "22":
        print(perfiles.ClasificacionDistrito("e2023_generales","03","02","km10").xEleccionCategoriaDistritoAlgoritmoElementos())
        print(perfiles.ClasificacionDistrito("e2023_generales","03","02","km03","secciones").xEleccionCategoriaDistritoAlgoritmoElementos())
    if opcion == "23":
        print(perfiles.PerfilPais("e2023_generales","01","km10").xEleccionCategoriaAlgoritmoElementos())
    if opcion == "24":
        print(perfiles.PerfilDistrito("e2023_generales","03","02","km10","secciones").xEleccionCategoriaDistritoAlgoritmoElementos())
    if opcion == "25":
        print(perfiles.ClasificacionPerfilPais("e2023_generales","01","km10").xEleccionCategoriaAlgoritmoElementos())
    if opcion == "26":
        print(perfiles.ClasificacionPerfilDistrito("e2023_generales","03","02","km10","secciones").xEleccionCategoriaDistritoAlgoritmoElementos())
    print()
    opcion=ingresarOpciones()
