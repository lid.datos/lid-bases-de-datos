# Archivo para probar objetos de establecimientos.py
from calendar import c
from pickle import TRUE
import censop

def ingresarOpciones():
    print("1) provincias")
    print("2) departamentos")
    print("3) censo")
    print("0) Salir")
    op=input("Ingresar opción: ")
    if op=="": return 0
    return op

opcion=ingresarOpciones()
while opcion != "0":
    if opcion == "1":
        print(censop.Provincias().xNom())
        print(censop.Provincias("2").xCodigo())
    if opcion == "2":
        print(censop.Departamentos("2").xNom())
    if opcion == "3":
        print(censop.Censo("2").xCodigo())
    print()
    opcion=ingresarOpciones()
