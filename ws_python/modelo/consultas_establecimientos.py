# Archivo para probar objetos de establecimientos.py
from calendar import c
from pickle import TRUE
import establecimientos

def ingresarOpciones():
    print("1) clae2")
    print("2) clae6")
    print("3) clae_letra")
    print("4) rangos")
    print("5) provincias")
    print("6) departamentos")
    print("7) establecimientos")
    print("0) Salir")
    op=input("Ingresar opción: ")
    if op=="": return 0
    return op

opcion=ingresarOpciones()
while opcion != "0":
    if opcion == "1":
        print(establecimientos.Clae2().xNom())
        print(establecimientos.Clae2("2").xCodigo())
    if opcion == "2":
        print(establecimientos.Clae6().xNom())
        print(establecimientos.Clae6("101011").xCodigo())
    if opcion == "3":
        print(establecimientos.ClaeLetra().xCodigo())
        print(establecimientos.ClaeLetra("A").xCodigo())
    if opcion == "4":
        print(establecimientos.Rangos().xNom())
        print(establecimientos.Rangos("a").xCodigo())
    if opcion == "5":
        print(establecimientos.Provincias().xNom())
        print(establecimientos.Provincias("34").xCodigo())
    if opcion == "6":
        #print(establecimientos.Departamentos().xNom())
        #print(establecimientos.Departamentos("10007").xCodigo())
        print(establecimientos.Departamentos("22").xNom())
    if opcion == "7":
        print(establecimientos.Establecimientos(None,"10007").xCodProvinciaCodDepartamento())
        print(establecimientos.Establecimientos(None,None,"2",None,"A","a").xCodigo())
    print()
    opcion=ingresarOpciones()
