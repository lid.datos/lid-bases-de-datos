# elecciones.py

### Categorias ###
    cod_distrito=None,cod_seccion=None,cod_circuito=None

### Distritos ###
    cod_categoria=None

### Secciones ###
    cod_distrito,cod_seccion=None,cod_seccion_prov=None

### SeccionesProvinciales ###
    cod_distrito=None,cod_seccion_prov=None

### Circuitos ###
    cod_distrito,cod_seccion=None,cod_circuito=None

### Mesas ###
    cod_distrito,cod_seccion=None,cod_circuito=None,cod_mesa=None

### Agrupaciones ###
    cod_distrito=None,cod_categoria=None,cod_agrupacion=None

### AgrupacionesSublistas ###
    cod_distrito=None,cod_categoria=None,cod_agrupacion=None

### ResultadosTotales ###
    cod_categoria=None

### ResultadosTotalesSublistas ###
    cod_categoria=None

### ResultadosTotalesMesas ###
    cod_categoria=None

### ResultadosDistritos ###
    cod_categoria=None,cod_distrito=None,cod_agrupacion=None

### ResultadosDistritosSublistas ###
    cod_categoria=None,cod_distrito=None,cod_agrupacion=None,cod_lista=None

### ResultadosDistritosMesas ###
    cod_categoria=None,cod_distrito=None

### ResultadosSecciones ###
    cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_agrupacion=None

### ResultadosSeccionesSublistas ###
    cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_agrupacion=None

### ResultadosSeccionesMesas ###
    cod_categoria=None,cod_distrito=None,cod_seccion=None

### ResultadosCircuitos ###
    cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_agrupacion=None

### ResultadosCircuitosSublistas ###
    cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_agrupacion=None

### ResultadosCircuitosMesas ###
    cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None

### ResultadosMesas ###
    cod_distrito,cod_seccion,cod_circuito=None,cod_categoria=None,cod_mesa=None,cod_agrupacion=None

### ResultadosMesasSublistas ###
    cod_distrito,cod_seccion,cod_circuito=None,cod_categoria=None,cod_mesa=None,cod_agrupacion=None,cod_lista=None

### ResultadosMesasMesa ###
    cod_distrito,cod_seccion,cod_circuito=None,cod_categoria=None,cod_mesa=None,cod_agrupacion=None

### ResultadosDistritosxCol ###
    cod_categoria=None,cod_distrito=None

### AgrupacionesDistritosxCol ###
    cod_categoria=None,cod_distrito=None

### ResultadosSeccionesxCol ###
    cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_agrupacion=None

### AgrupacionesSeccionesxCol ###
    cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_agrupacion=None

### ResultadosCircuitosxCol ###
    cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_agrupacion=None

### AgrupacionesCircuitosxCol ###
    cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_agrupacion=None
