def condicion(self,columna,valor):
    if "WHERE" in self.q:
        condicion="AND"
    else:
        condicion="WHERE"
    if (valor is not None):
        self.q+="\n"
        if isinstance(valor,str):
            self.q+=condicion+" "+columna+" = '"+valor+"'"
        else:
            self.i+=1
            self.q+=condicion+" "+columna+" = $"+str(self.i)
def like(self,columna,valor):
    if "WHERE" in self.q:
        condicion="AND"
    else:
        condicion="WHERE"
    if (valor is not None):
        self.q+="\n"
        if isinstance(valor,str):
            self.q+=condicion+" "+columna+" LIKE '"+valor+"'"
        else:
            self.i+=1
            self.q+=condicion+" "+columna+" LIKE $"+str(self.i)
class PerfilesCircuitosPerfiles:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT c.perfil, c.perfil nom_perfil
FROM perfiles_circuitos c"""
        condicion(self,"c.cod_eleccion",cod_eleccion)
        condicion(self,"c.cod_categoria",cod_categoria)
        condicion(self,"c.cod_distrito",cod_distrito)
        condicion(self,"c.cod_seccion",cod_seccion)
        condicion(self,"c.cod_circuito",cod_circuito)
        condicion(self,"c.cod_algoritmo",cod_algoritmo)
        condicion(self,"c.perfil",perfil)
    def xPerfil(self):
        return self.q+"\nORDER BY c.perfil"
class PerfilesCircuitosCircuitos:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT c.cod_circuito, c.cod_circuito nom_circuito
FROM perfiles_circuitos c"""
        condicion(self,"c.cod_eleccion",cod_eleccion)
        condicion(self,"c.cod_categoria",cod_categoria)
        condicion(self,"c.cod_distrito",cod_distrito)
        condicion(self,"c.cod_seccion",cod_seccion)
        condicion(self,"c.cod_circuito",cod_circuito)
        condicion(self,"c.cod_algoritmo",cod_algoritmo)
        condicion(self,"c.perfil",perfil)
    def xCodCircuito(self):
        return self.q+"\nORDER BY c.cod_circuito"
class PerfilesCircuitosSecciones:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT s.cod_seccion, e.nom_seccion || ' (' || s.cod_seccion || ')' AS nom_seccion
FROM perfiles_circuitos c FULL OUTER JOIN secciones e
ON c.cod_distrito = e.cod_distrito AND c.cod_seccion = e.cod_seccion"""
        condicion(self,"c.cod_eleccion",cod_eleccion)
        condicion(self,"c.cod_categoria",cod_categoria)
        condicion(self,"c.cod_distrito",cod_distrito)
        condicion(self,"c.cod_seccion",cod_seccion)
        condicion(self,"c.cod_circuito",cod_circuito)
        condicion(self,"c.cod_algoritmo",cod_algoritmo)
        condicion(self,"c.perfil",perfil)
    def xCodSeccion(self):
        return self.q+"\nORDER BY nom_seccion"
class PerfilesCircuitosDistritos:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT c.cod_distrito, d.nom_distrito
FROM perfiles_circuitos c FULL OUTER JOIN distritos d
ON c.cod_distrito = d.cod_distrito"""
        condicion(self,"c.cod_eleccion",cod_eleccion)
        condicion(self,"c.cod_categoria",cod_categoria)
        condicion(self,"c.cod_distrito",cod_distrito)
        condicion(self,"c.cod_seccion",cod_seccion)
        condicion(self,"c.cod_circuito",cod_circuito)
        condicion(self,"c.cod_algoritmo",cod_algoritmo)
        condicion(self,"c.perfil",perfil)
    def xCodDistrito(self):
        return self.q+"\nORDER BY d.nom_distrito"
class PerfilesCircuitosCategorias:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT c.cod_categoria, t.nom_categoria
FROM perfiles_circuitos c FULL OUTER JOIN categorias t
ON c.cod_categoria = t.cod_categoria"""
        condicion(self,"c.cod_eleccion",cod_eleccion)
        condicion(self,"c.cod_categoria",cod_categoria)
        condicion(self,"c.cod_distrito",cod_distrito)
        condicion(self,"c.cod_seccion",cod_seccion)
        condicion(self,"c.cod_circuito",cod_circuito)
        condicion(self,"c.cod_algoritmo",cod_algoritmo)
        condicion(self,"c.perfil",perfil)
    def xCodCategoria(self):
        return self.q+"\nORDER BY c.cod_categoria"
class PerfilesCircuitosElecciones:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT c.cod_eleccion, e.nom_eleccion
FROM perfiles_circuitos c, elecciones e
WHERE c.cod_eleccion = e.cod_eleccion"""
        condicion(self,"c.cod_eleccion",cod_eleccion)
        condicion(self,"c.cod_categoria",cod_categoria)
        condicion(self,"c.cod_distrito",cod_distrito)
        condicion(self,"c.cod_seccion",cod_seccion)
        condicion(self,"c.cod_circuito",cod_circuito)
        condicion(self,"c.cod_algoritmo",cod_algoritmo)
        condicion(self,"c.perfil",perfil)
    def xCodEleccion(self):
        return self.q+"\nORDER BY c.cod_eleccion"
class PerfilesCircuitosAlgoritmos:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT c.cod_algoritmo, c.cod_algoritmo AS nom_algoritmo
FROM perfiles_circuitos c"""
        condicion(self,"c.cod_eleccion",cod_eleccion)
        condicion(self,"c.cod_categoria",cod_categoria)
        condicion(self,"c.cod_distrito",cod_distrito)
        condicion(self,"c.cod_seccion",cod_seccion)
        condicion(self,"c.cod_circuito",cod_circuito)
        condicion(self,"c.cod_algoritmo",cod_algoritmo)
        condicion(self,"c.perfil",perfil)
    def xCodAlgoritmo(self):
        return self.q+"\nORDER BY c.cod_algoritmo"
class PerfilesCircuitos:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT c.cod_eleccion
     , e.nom_eleccion
     , c.cod_categoria
     , t.nom_categoria
     , c.cod_distrito
     , d.nom_distrito
     , c.cod_seccion
     , s.nom_seccion
     , c.cod_circuito
     , c.cod_algoritmo
     , c.perfil
FROM perfiles_circuitos c, elecciones e, categorias t, distritos d, secciones s
WHERE c.cod_eleccion = e.cod_eleccion
AND c.cod_categoria = t.cod_categoria
AND c.cod_distrito = d.cod_distrito
AND c.cod_distrito = s.cod_distrito
AND c.cod_seccion = s.cod_seccion"""
        condicion(self,"c.cod_eleccion",cod_eleccion)
        condicion(self,"c.cod_categoria",cod_categoria)
        condicion(self,"c.cod_distrito",cod_distrito)
        condicion(self,"c.cod_seccion",cod_seccion)
        condicion(self,"c.cod_circuito",cod_circuito)
        condicion(self,"c.cod_algoritmo",cod_algoritmo)
        condicion(self,"c.perfil",perfil)
    def xCodCategoriaCodDistritoCodSeccionCodCircuitoPerfil(self):
        return self.q+"\nORDER BY c.cod_eleccion,c.cod_categoria,c.cod_distrito,c.cod_seccion,c.cod_circuito,c.cod_algoritmo,c.perfil"
class PerfilesSeccionesPerfiles:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT s.perfil, s.perfil nom_perfil
FROM perfiles_secciones s"""
        condicion(self,"s.cod_eleccion",cod_eleccion)
        condicion(self,"s.cod_categoria",cod_categoria)
        condicion(self,"s.cod_distrito",cod_distrito)
        condicion(self,"s.cod_seccion",cod_seccion)
        condicion(self,"s.cod_algoritmo",cod_algoritmo)
        condicion(self,"s.perfil",perfil)
    def xPerfil(self):
        return self.q+"\nORDER BY s.perfil"
class PerfilesSeccionesSecciones:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT s.cod_seccion, e.nom_seccion || ' (' || s.cod_seccion || ')' AS nom_seccion
FROM perfiles_secciones s FULL OUTER JOIN secciones e
ON s.cod_distrito = e.cod_distrito AND s.cod_seccion = e.cod_seccion"""
        condicion(self,"s.cod_eleccion",cod_eleccion)
        condicion(self,"s.cod_categoria",cod_categoria)
        condicion(self,"s.cod_distrito",cod_distrito)
        condicion(self,"s.cod_seccion",cod_seccion)
        condicion(self,"s.cod_algoritmo",cod_algoritmo)
        condicion(self,"s.perfil",perfil)
    def xCodSeccion(self):
        return self.q+"\nORDER BY nom_seccion"
class PerfilesSeccionesDistritos:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT s.cod_distrito, d.nom_distrito
FROM perfiles_secciones s FULL OUTER JOIN distritos d
ON s.cod_distrito = d.cod_distrito"""
        condicion(self,"s.cod_eleccion",cod_eleccion)
        condicion(self,"s.cod_categoria",cod_categoria)
        condicion(self,"s.cod_distrito",cod_distrito)
        condicion(self,"s.cod_seccion",cod_seccion)
        condicion(self,"s.cod_algoritmo",cod_algoritmo)
        condicion(self,"s.perfil",perfil)
    def xCodDistrito(self):
        return self.q+"\nORDER BY d.nom_distrito"
class PerfilesSeccionesCategorias:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT s.cod_categoria, t.nom_categoria
FROM perfiles_secciones s FULL OUTER JOIN categorias t
ON s.cod_categoria = t.cod_categoria"""
        condicion(self,"s.cod_eleccion",cod_eleccion)
        condicion(self,"s.cod_categoria",cod_categoria)
        condicion(self,"s.cod_distrito",cod_distrito)
        condicion(self,"s.cod_seccion",cod_seccion)
        condicion(self,"s.cod_algoritmo",cod_algoritmo)
        condicion(self,"s.perfil",perfil)
    def xCodCategoria(self):
        return self.q+"\nORDER BY s.cod_categoria"
class PerfilesSeccionesElecciones:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT s.cod_eleccion, e.nom_eleccion
FROM perfiles_secciones s, elecciones e
WHERE s.cod_eleccion = e.cod_eleccion"""
        condicion(self,"s.cod_eleccion",cod_eleccion)
        condicion(self,"s.cod_categoria",cod_categoria)
        condicion(self,"s.cod_distrito",cod_distrito)
        condicion(self,"s.cod_seccion",cod_seccion)
        condicion(self,"s.cod_algoritmo",cod_algoritmo)
        condicion(self,"s.perfil",perfil)
    def xCodEleccion(self):
        return self.q+"\nORDER BY s.cod_eleccion"
class PerfilesSeccionesAlgoritmos:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT DISTINCT s.cod_algoritmo, s.cod_algoritmo AS nom_algoritmo
FROM perfiles_secciones s"""
        condicion(self,"s.cod_eleccion",cod_eleccion)
        condicion(self,"s.cod_categoria",cod_categoria)
        condicion(self,"s.cod_distrito",cod_distrito)
        condicion(self,"s.cod_seccion",cod_seccion)
        condicion(self,"s.cod_algoritmo",cod_algoritmo)
        condicion(self,"s.perfil",perfil)
    def xCodAlgoritmo(self):
        return self.q+"\nORDER BY s.cod_algoritmo"
class PerfilesSecciones:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_algoritmo=None,perfil=None):
        self.i = 0
        self.q = """
SELECT s.cod_eleccion
     , e.nom_eleccion
     , s.cod_categoria
     , c.nom_categoria
     , s.cod_distrito
     , d.nom_distrito
     , s.cod_seccion
     , t.nom_seccion
     , s.cod_algoritmo
     , s.perfil
FROM perfiles_secciones s, elecciones e, categorias c, distritos d, secciones t
WHERE s.cod_eleccion = e.cod_eleccion
AND s.cod_categoria = c.cod_categoria
AND s.cod_distrito = d.cod_distrito
AND s.cod_distrito = t.cod_distrito
AND s.cod_seccion = t.cod_seccion"""
        condicion(self,"s.cod_eleccion",cod_eleccion)
        condicion(self,"s.cod_categoria",cod_categoria)
        condicion(self,"s.cod_distrito",cod_distrito)
        condicion(self,"s.cod_seccion",cod_seccion)
        condicion(self,"s.cod_algoritmo",cod_algoritmo)
        condicion(self,"s.perfil",perfil)
    def xCodCategoriaCodDistritoCodSeccionPerfil(self):
        return self.q+"\nORDER BY s.cod_eleccion,s.cod_categoria,s.cod_distrito,s.cod_seccion,s.cod_algoritmo,s.perfil"
class ClasificacionPais:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_algoritmo=None,elementos=None):
        self.i = 0
        self.q = """
SELECT cp.cod_eleccion
     , e.nom_eleccion
     , cp.cod_categoria
     , c.nom_categoria
     , cp.cod_algoritmo
     , cp.elementos
     , cp.inercia
     , cp.silhouette
FROM clasificacion_pais cp, elecciones e, categorias c
WHERE cp.cod_eleccion = e.cod_eleccion
AND cp.cod_categoria = c.cod_categoria"""
        condicion(self,"cp.cod_eleccion",cod_eleccion)
        condicion(self,"cp.cod_categoria",cod_categoria)
        condicion(self,"cp.cod_algoritmo",cod_algoritmo)
        condicion(self,"cp.elementos",elementos)
    def xEleccionCategoriaAlgoritmoElementos(self):
        return self.q+"\nORDER BY cp.cod_eleccion,cp.cod_categoria,cp.cod_algoritmo,cp.elementos"
class ClasificacionDistrito:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_algoritmo=None,elementos=None):
        self.i = 0
        self.q = """
SELECT cd.cod_eleccion
     , e.nom_eleccion
     , cd.cod_categoria
     , c.nom_categoria
     , cd.cod_distrito
     , d.nom_distrito
     , cd.cod_algoritmo
     , cd.elementos
     , cd.inercia
     , cd.silhouette
FROM clasificacion_distrito cd, elecciones e, categorias c, distritos d
WHERE cd.cod_eleccion = e.cod_eleccion
AND cd.cod_categoria = c.cod_categoria
AND cd.cod_distrito = d.cod_distrito"""
        condicion(self,"cd.cod_eleccion",cod_eleccion)
        condicion(self,"cd.cod_categoria",cod_categoria)
        condicion(self,"cd.cod_distrito",cod_distrito)
        condicion(self,"cd.cod_algoritmo",cod_algoritmo)
        condicion(self,"cd.elementos",elementos)
    def xEleccionCategoriaDistritoAlgoritmoElementos(self):
        return self.q+"\nORDER BY cd.cod_eleccion,cd.cod_categoria,cd.cod_distrito,cd.cod_algoritmo,cd.elementos"
class PerfilPais:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_algoritmo=None,elementos=None,perfil=None):
        self.i = 0
        self.q = """
SELECT pp.cod_eleccion
     , e.nom_eleccion
     , pp.cod_categoria
     , c.nom_categoria
     , pp.cod_algoritmo
     , pp.elementos
     , pp.perfil
     , pp.cantidad
     , pp.varianza
     , pp.dist_media_centro
     , pp.desc_breve
     , pp.agrupaciones
FROM perfil_pais pp, elecciones e, categorias c
WHERE pp.cod_eleccion = e.cod_eleccion
AND pp.cod_categoria = c.cod_categoria"""
        condicion(self,"pp.cod_eleccion",cod_eleccion)
        condicion(self,"pp.cod_categoria",cod_categoria)
        condicion(self,"pp.cod_algoritmo",cod_algoritmo)
        condicion(self,"pp.elementos",elementos)
        condicion(self,"pp.perfil",perfil)
    def xEleccionCategoriaAlgoritmoElementos(self):
        return self.q+"\nORDER BY pp.cod_eleccion,pp.cod_categoria,pp.cod_algoritmo,pp.elementos"
class PerfilDistrito:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_algoritmo=None,elementos=None,perfil=None):
        self.i = 0
        self.q = """
SELECT pd.cod_eleccion
     , e.nom_eleccion
     , pd.cod_categoria
     , c.nom_categoria
     , pd.cod_distrito
     , d.nom_distrito
     , pd.cod_algoritmo
     , pd.elementos
     , pd.perfil
     , pd.cantidad
     , pd.varianza
     , pd.dist_media_centro
     , pd.desc_breve
     , pd.agrupaciones
FROM perfil_distrito pd, elecciones e, categorias c, distritos d
WHERE pd.cod_eleccion = e.cod_eleccion
AND pd.cod_categoria = c.cod_categoria
AND pd.cod_distrito = d.cod_distrito"""
        condicion(self,"pd.cod_eleccion",cod_eleccion)
        condicion(self,"pd.cod_categoria",cod_categoria)
        condicion(self,"pd.cod_distrito",cod_distrito)
        condicion(self,"pd.cod_algoritmo",cod_algoritmo)
        condicion(self,"pd.elementos",elementos)
        condicion(self,"pd.perfil",perfil)
    def xEleccionCategoriaDistritoAlgoritmoElementos(self):
        return self.q+"\nORDER BY pd.cod_eleccion,pd.cod_categoria,pd.cod_distrito,pd.cod_algoritmo,pd.elementos"
class ClasificacionPerfilPais:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_algoritmo=None,elementos=None,perfil=None):
        self.i = 0
        self.q = """
SELECT cp.cod_eleccion
     , e.nom_eleccion
     , cp.cod_categoria
     , c.nom_categoria
     , cp.cod_algoritmo
     , cp.elementos
     , cp.inercia
     , cp.silhouette
     , pp.perfil
     , pp.cantidad
     , pp.varianza
     , pp.dist_media_centro
     , pp.desc_breve
     , pp.agrupaciones
FROM clasificacion_pais cp, perfil_pais pp, elecciones e, categorias c
WHERE cp.cod_eleccion = e.cod_eleccion
AND cp.cod_categoria  = c.cod_categoria
AND pp.cod_eleccion   = e.cod_eleccion
AND pp.cod_categoria  = c.cod_categoria
AND cp.cod_algoritmo  = pp.cod_algoritmo
AND cp.elementos      = pp.elementos"""
        condicion(self,"cp.cod_eleccion",cod_eleccion)
        condicion(self,"cp.cod_categoria",cod_categoria)
        condicion(self,"cp.cod_algoritmo",cod_algoritmo)
        condicion(self,"cp.elementos",elementos)
        condicion(self,"pp.perfil",perfil)
    def xEleccionCategoriaAlgoritmoElementos(self):
        return self.q+"\nORDER BY cp.cod_eleccion,cp.cod_categoria,cp.cod_algoritmo,cp.elementos,pp.perfil"
class ClasificacionPerfilDistrito:
    def __init__(self,cod_eleccion=None,cod_categoria=None,cod_distrito=None,cod_algoritmo=None,elementos=None,perfil=None):
        self.i = 0
        self.q = """
SELECT cd.cod_eleccion
     , e.nom_eleccion
     , cd.cod_categoria
     , c.nom_categoria
     , cd.cod_distrito
     , d.nom_distrito
     , cd.cod_algoritmo
     , cd.elementos
     , cd.inercia
     , cd.silhouette
     , pd.perfil
     , pd.cantidad
     , pd.varianza
     , pd.dist_media_centro
     , pd.desc_breve
     , pd.agrupaciones
FROM clasificacion_distrito cd, perfil_distrito pd, elecciones e, categorias c, distritos d
WHERE cd.cod_eleccion = e.cod_eleccion
AND cd.cod_categoria  = c.cod_categoria
AND cd.cod_distrito   = d.cod_distrito
AND pd.cod_eleccion   = e.cod_eleccion
AND pd.cod_categoria  = c.cod_categoria
AND pd.cod_distrito   = d.cod_distrito
AND cd.cod_algoritmo  = pd.cod_algoritmo
AND cd.elementos = pd.elementos"""
        condicion(self,"cd.cod_eleccion",cod_eleccion)
        condicion(self,"cd.cod_categoria",cod_categoria)
        condicion(self,"cd.cod_distrito",cod_distrito)
        condicion(self,"cd.cod_algoritmo",cod_algoritmo)
        condicion(self,"cd.elementos",elementos)
        condicion(self,"pd.perfil",perfil)
    def xEleccionCategoriaDistritoAlgoritmoElementos(self):
        return self.q+"\nORDER BY cd.cod_eleccion,cd.cod_categoria,pd.cod_distrito,pd.cod_algoritmo,pd.elementos,pd.perfil"
    