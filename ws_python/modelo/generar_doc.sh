generar_doc_arch(){
# Uso: generar_doc.sh nom_archivo
# De un archivo .py con clases genera una doc básica en md
# Ejemplo, para generar elecciones.md: /.generar_doc.sh eleciones.py 
TEMP="temp.md"
grep -A 1 'class ' $1.py > $TEMP
sed -i 's/class/###/' $TEMP
sed -i 's/    def __init__(self,/    /' $TEMP
sed -i 's/    def __init__(self/    Sin parámetros/' $TEMP
sed -i 's/)://' $TEMP
sed -i 's/--//' $TEMP
sed -i 's/:/ ###/' $TEMP
echo "# $1.py" > $1.md
echo >> $1.md
cat $TEMP >> $1.md
rm $TEMP
}

generar_doc_arch elecciones
generar_doc_arch establecimientos
generar_doc_arch censop
generar_doc_arch perfiles