def condicion(self,columna,valor):
    if "WHERE" in self.q:
        condicion="AND"
    else:
        condicion="WHERE"
    if (valor is not None):
        self.q+="\n"
        if isinstance(valor,str):
            self.q+=condicion+" "+columna+" = '"+valor+"'"
        else:
            self.i+=1
            self.q+=condicion+" "+columna+" = $"+str(self.i)
class Categorias:
    def __init__(self,cod_distrito=None,cod_seccion=None,cod_circuito=None):
        self.i = 0
        self.q = """
SELECT DISTINCT c.cod_categoria,c.nom_categoria
FROM mesas_circuitos m, categorias c
WHERE c.cod_categoria = m.cod_categoria"""
        condicion(self,"m.cod_distrito",cod_distrito)
        condicion(self,"m.cod_seccion",cod_seccion)
        condicion(self,"m.cod_circuito",cod_circuito)
    def xCodCategoria(self):
        return self.q+"\nORDER BY c.cod_categoria"
class Distritos:
    def __init__(self,cod_categoria=None):
        self.i = 0
        self.q = """
SELECT DISTINCT d.cod_distrito, d.nom_distrito
FROM distritos d, mesas_distritos m
WHERE d.cod_distrito = m.cod_distrito"""
        condicion(self,"m.cod_categoria",cod_categoria)
    def xCodDistrito(self):
        return self.q+"\nORDER BY d.cod_distrito"
    def xNomDistrito(self):
        return self.q+"\nORDER BY d.nom_distrito"
class Secciones:
    def __init__(self,cod_distrito,cod_seccion=None,cod_seccion_prov=None):
        self.i = 0
        self.q = """
SELECT cod_seccion,nom_seccion,cod_seccion_prov
FROM secciones"""
        condicion(self,"cod_distrito",cod_distrito)
        condicion(self,"cod_seccion",cod_seccion)
        condicion(self,"cod_seccion_prov",cod_seccion_prov)
    def xCodSeccion(self):
        return self.q+"\nORDER BY cod_distrito,cod_seccion"
    def xCodSeccionProv(self):
        return self.q+"\nORDER BY cod_distrito,cod_seccion_prov,cod_seccion"
class SeccionesProvinciales:
    def __init__(self,cod_distrito=None,cod_seccion_prov=None):
        self.i = 0
        self.q = """
SELECT cod_seccion_prov,nom_seccion_prov
FROM secciones_provinciales"""
        condicion(self,"cod_distrito",cod_distrito)
        condicion(self,"cod_seccion_prov",cod_seccion_prov)
    def xCodSeccionProv(self):
        return self.q+"\nORDER BY cod_distrito,cod_seccion_prov"
class Circuitos:
    def __init__(self,cod_distrito,cod_seccion=None,cod_circuito=None):
        self.i = 0
        self.q = """
SELECT cod_circuito,nom_circuito
FROM circuitos"""
        condicion(self,"cod_distrito",cod_distrito)
        condicion(self,"cod_seccion",cod_seccion)
        condicion(self,"cod_circuito",cod_circuito)
    def xCodCircuito(self):
        return self.q+"\nORDER BY cod_distrito,cod_seccion,cod_circuito"
class Mesas:
    def __init__(self,cod_distrito,cod_seccion=None,cod_circuito=None,cod_mesa=None):
        self.i = 0
        self.q = """
SELECT cod_mesa,cod_mesa nom_mesa
FROM mesas"""
        condicion(self,"cod_distrito",cod_distrito)
        condicion(self,"cod_seccion",cod_seccion)
        condicion(self,"cod_circuito",cod_circuito)
        condicion(self,"cod_mesa",cod_mesa)
    def xCodMesa(self):
        return self.q+"\nORDER BY cod_distrito,cod_seccion,cod_circuito,cod_mesa"
class Agrupaciones:
    def __init__(self,cod_distrito=None,cod_categoria=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT DISTINCT v.cod_agrupacion_int
     , a.cod_agrupacion
     , a.nom_agrupacion
FROM votos_distritos v,agrupaciones a,categorias c,tipos_voto t
WHERE v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_categoria = c.cod_categoria
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_agrupacion_int != '0000'"""
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"a.cod_agrupacion",cod_agrupacion)
    def xCodAgrupacion(self):
        return self.q+"\nORDER BY a.cod_agrupacion"
class AgrupacionesSublistas:
    def __init__(self,cod_distrito=None,cod_categoria=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT DISTINCT v.cod_agrupacion_int
     , a.cod_agrupacion
     , a.nom_agrupacion
     , l.cod_lista
     , l.nom_lista
FROM votos_distritos v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t
WHERE v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_categoria = c.cod_categoria
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_agrupacion_int != '0000'"""
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"a.cod_agrupacion",cod_agrupacion)
    def xCodAgrupacionCodLista(self):
        return self.q+"\nORDER BY a.cod_agrupacion,l.cod_lista"
class ResultadosTotales:
    def __init__(self,cod_categoria=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/(SELECT SUM(md.votos_positivos) FROM mesas_distritos md WHERE md.cod_categoria = v.cod_categoria)*100,2) porcentaje
FROM votos_distritos v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_lista = l.cod_lista"""
        condicion(self,"v.cod_categoria",cod_categoria)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,nom_agrupacion"
    def xCategoriaVotos(self):
        return self.q+"\nORDER BY v.cod_categoria,orden,votos DESC"
class ResultadosTotalesSublistas:
    def __init__(self,cod_categoria=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , v.cod_agrupacion_int
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , l.cod_lista
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE l.nom_lista END nom_lista
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/SUM(mv.votos_positivos)*100,2) porcentaje
FROM votos_distritos v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,mesas_distritos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_distrito = mv.cod_distrito
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_lista = l.cod_lista"""
        condicion(self,"v.cod_categoria",cod_categoria)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,nom_agrupacion,l.cod_lista, nom_lista"
    def xCategoriaVotos(self):
        return self.q+"\nORDER BY v.cod_categoria,orden,votos DESC"
class ResultadosTotalesMesas:
    def __init__(self,cod_categoria=None):
        self.i = 0
        self.q = """
SELECT m.cod_categoria
     , c.nom_categoria
     , SUM(m.cant_electores) cant_electores
     , SUM(m.votos) votos
     , SUM(m.votos_positivos) votos_positivos
     , SUM(m.cant_mesas) cant_mesas
FROM mesas_distritos m, categorias c
WHERE m.cod_categoria = c.cod_categoria"""
        condicion(self,"m.cod_categoria",cod_categoria)
        self.q+="\nGROUP BY m.cod_categoria,c.nom_categoria"
    def xCategoria(self):
        return self.q+"\nORDER BY m.cod_categoria"
class ResultadosDistritos:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/NULLIF(mv.votos_positivos,0)*100,2) porcentaje
FROM votos_distritos v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,mesas_distritos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND v.cod_distrito = mv.cod_distrito
AND v.cod_categoria = mv.cod_categoria"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"a.cod_agrupacion",cod_agrupacion)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,a.nom_agrupacion,mv.votos_positivos"
    def xCategoriaDistritoVotos(self):
        return self.q+"\nORDER BY v.cod_categoria,v.cod_distrito,orden,votos DESC"
    def xDistritoCategoriaVotos(self):
        return self.q+"\nORDER BY v.cod_distrito,v.cod_categoria,orden,votos DESC"
class ResultadosDistritosSublistas:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_agrupacion=None,cod_lista=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , v.cod_agrupacion_int
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , l.cod_lista
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE l.nom_lista END nom_lista
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , v.votos
     , ROUND(CAST(v.votos AS DECIMAL)/NULLIF(mv.votos_positivos,0)*100,2) porcentaje
FROM votos_distritos v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,mesas_distritos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND v.cod_distrito = mv.cod_distrito
AND v.cod_categoria = mv.cod_categoria"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"a.cod_agrupacion",cod_agrupacion)
        condicion(self,"l.cod_lista",cod_lista)
    def xCategoriaDistritoVotos(self):
        return self.q+"\nORDER BY v.cod_categoria,v.cod_distrito,orden,v.votos DESC"
    def xDistritoCategoriaVotos(self):
        return self.q+"\nORDER BY v.cod_distrito,v.cod_categoria,orden,v.votos DESC"
class ResultadosDistritosMesas:
    def __init__(self,cod_categoria=None,cod_distrito=None):
        self.i = 0
        self.q = """
SELECT m.cod_categoria
     , c.nom_categoria
     , m.cod_distrito
     , d.nom_distrito
     , m.cant_electores
     , m.votos
     , m.votos_positivos
     , m.cant_mesas
FROM mesas_distritos m,categorias c,distritos d
WHERE m.cod_categoria = c.cod_categoria
AND m.cod_distrito = d.cod_distrito"""
        condicion(self,"m.cod_categoria",cod_categoria)
        condicion(self,"m.cod_distrito",cod_distrito)
    def xCategoriaDistritoVotos(self):
        return self.q+"\nORDER BY m.cod_categoria,m.cod_distrito,m.votos DESC"
    def xDistritoCategoriaVotos(self):
        return self.q+"\nORDER BY m.cod_distrito,m.cod_categoria,m.votos DESC"
class ResultadosSecciones:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/NULLIF(mv.votos_positivos,0)*100,2) porcentaje
FROM votos_secciones v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,secciones s,mesas_secciones mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito  = d.cod_distrito
AND v.cod_distrito  = s.cod_distrito
AND v.cod_seccion   = s.cod_seccion
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_distrito  = mv.cod_distrito
AND v.cod_seccion   = mv.cod_seccion
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        condicion(self,"a.cod_agrupacion",cod_agrupacion)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,v.cod_seccion,s.nom_seccion,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,nom_agrupacion,mv.votos_positivos"
    def xCategoriaDistritoSeccionVotos(self):
        return self.q+"\nORDER BY v.cod_categoria,v.cod_distrito,v.cod_seccion,orden,votos DESC"
    def xDistritoSeccionCategoriaVotos(self):
        return self.q+"\nORDER BY v.cod_distrito,v.cod_seccion,v.cod_categoria,orden,votos DESC"
class ResultadosSeccionesSublistas:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , v.cod_agrupacion_int
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , l.cod_lista
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE l.nom_lista END nom_lista
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , v.votos
     , ROUND(CAST(v.votos AS DECIMAL)/NULLIF(mv.votos_positivos,0)*100,2) porcentaje
FROM votos_secciones v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,secciones s,mesas_secciones mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_distrito = s.cod_distrito
AND v.cod_seccion  = s.cod_seccion
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_distrito  = mv.cod_distrito
AND v.cod_seccion   = mv.cod_seccion
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        condicion(self,"a.cod_agrupacion",cod_agrupacion)
    def xCategoriaDistritoSeccionVotos(self):
        return self.q+"\nORDER BY v.cod_categoria,v.cod_distrito,v.cod_seccion,orden,votos DESC"
    def xDistritoSeccionCategoriaVotos(self):
        return self.q+"\nORDER BY v.cod_distrito,v.cod_seccion,v.cod_categoria,orden,votos DESC"
class ResultadosSeccionesMesas:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_seccion=None):
        self.i = 0
        self.q = """
SELECT m.cod_categoria
     , c.nom_categoria
     , m.cod_distrito
     , d.nom_distrito
     , m.cod_seccion
     , s.nom_seccion
     , m.cant_electores
     , m.votos
     , m.votos_positivos
     , m.cant_mesas
FROM mesas_secciones m,categorias c,distritos d,secciones s
WHERE m.cod_categoria = c.cod_categoria
AND m.cod_distrito = d.cod_distrito
AND m.cod_distrito = s.cod_distrito
AND m.cod_seccion = s.cod_seccion"""
        condicion(self,"m.cod_categoria",cod_categoria)
        condicion(self,"m.cod_distrito",cod_distrito)
        condicion(self,"m.cod_seccion",cod_seccion)
    def xCategoriaDistritoSeccionesVotos(self):
        return self.q+"\nORDER BY m.cod_categoria,m.cod_distrito,m.cod_seccion,m.votos DESC"
    def xDistritoSeccionesCategoriaVotos(self):
        return self.q+"\nORDER BY m.cod_distrito,m.cod_seccion,m.cod_categoria,m.votos DESC"
class ResultadosCircuitos:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     , v.cod_circuito
     , i.nom_circuito
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , v.cod_agrupacion_int
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/NULLIF(mv.votos_positivos,0)*100,2) porcentaje
FROM votos_circuitos v,agrupaciones a,categorias c,tipos_voto t,distritos d,secciones s,circuitos i,mesas_circuitos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_distrito = s.cod_distrito
AND v.cod_seccion  = s.cod_seccion
AND v.cod_distrito = i.cod_distrito
AND v.cod_seccion  = i.cod_seccion
AND v.cod_circuito = i.cod_circuito
AND v.cod_distrito = mv.cod_distrito
AND v.cod_seccion  = mv.cod_seccion
AND v.cod_circuito = mv.cod_circuito
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_tipo_voto = t.cod_tipo_voto"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        condicion(self,"v.cod_circuito",cod_circuito)
        condicion(self,"a.cod_agrupacion",cod_agrupacion)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,v.cod_seccion,s.nom_seccion,v.cod_circuito,i.nom_circuito,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,nom_agrupacion,mv.votos_positivos"
    def xCategoriaDistritoSeccionCircuitoVotos(self):
        return self.q+"\nORDER BY v.cod_categoria,v.cod_distrito,v.cod_seccion,v.cod_circuito,orden,votos DESC"
    def xDistritoSeccionCircuitoCategoriaVotos(self):
        return self.q+"\nORDER BY v.cod_distrito,v.cod_seccion,v.cod_circuito,v.cod_categoria,orden,votos DESC"
class ResultadosCircuitosSublistas:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     , v.cod_circuito
     , i.nom_circuito
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , v.cod_agrupacion_int
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , l.cod_lista
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE l.nom_lista END nom_lista
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , v.votos
     , ROUND(CAST(v.votos AS DECIMAL)/NULLIF(mv.votos_positivos,0)*100,2) porcentaje
FROM votos_circuitos v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,secciones s,circuitos i,mesas_circuitos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_distrito = s.cod_distrito
AND v.cod_seccion  = s.cod_seccion
AND v.cod_distrito = i.cod_distrito
AND v.cod_seccion  = i.cod_seccion
AND v.cod_circuito = i.cod_circuito
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND v.cod_categoria = mv.cod_categoria
AND v.cod_distrito  = mv.cod_distrito
AND v.cod_seccion   = mv.cod_seccion
AND v.cod_circuito  = mv.cod_circuito
AND v.cod_tipo_voto = t.cod_tipo_voto"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        condicion(self,"v.cod_circuito",cod_circuito)
        condicion(self,"a.cod_agrupacion",cod_agrupacion)
    def xCategoriaDistritoSeccionCircuitoVotos(self):
        return self.q+"\nORDER BY v.cod_categoria,v.cod_distrito,v.cod_seccion,v.cod_circuito,orden,votos DESC"
    def xDistritoSeccionCircuitoCategoriaVotos(self):
        return self.q+"\nORDER BY v.cod_distrito,v.cod_seccion,v.cod_circuito,v.cod_categoria,orden,votos DESC"
class ResultadosCircuitosMesas:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None):
        self.i = 0
        self.q = """
SELECT m.cod_categoria
     , c.nom_categoria
     , m.cod_distrito
     , d.nom_distrito
     , m.cod_seccion
     , s.nom_seccion
     , m.cod_circuito
     , i.nom_circuito
     , m.cant_electores
     , m.votos
     , m.votos_positivos
     , m.cant_mesas
FROM mesas_circuitos m,categorias c,distritos d,secciones s,circuitos i
WHERE m.cod_categoria = c.cod_categoria
AND m.cod_distrito = d.cod_distrito
AND m.cod_distrito = s.cod_distrito
AND m.cod_seccion  = s.cod_seccion
AND m.cod_distrito = i.cod_distrito
AND m.cod_seccion  = i.cod_seccion
AND m.cod_circuito = i.cod_circuito"""
        condicion(self,"m.cod_categoria",cod_categoria)
        condicion(self,"m.cod_distrito",cod_distrito)
        condicion(self,"m.cod_seccion",cod_seccion)
        condicion(self,"m.cod_circuito",cod_circuito)
    def xCategoriaDistritoSeccionesCircuitosVotos(self):
        return self.q+"\nORDER BY m.cod_categoria,m.cod_distrito,m.cod_seccion,m.cod_circuito,m.votos DESC"
    def xDistritoSeccionesCircuitosCategoriaVotos(self):
        return self.q+"\nORDER BY m.cod_distrito,m.cod_seccion,m.cod_circuito,m.cod_categoria,m.votos DESC"
class ResultadosMesas:
    def __init__(self,cod_distrito,cod_seccion,cod_circuito=None,cod_categoria=None,cod_mesa=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     , m.cod_circuito
     , i.nom_circuito
     , m.cod_lugar
     , m.cod_mesa
     , m.cod_mesa nom_mesa
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , v.cod_agrupacion_int
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/NULLIF(mv.votos_positivos,0)*100,2) porcentaje
     , COUNT(v.cod_lista) cant_sublistas 
FROM votos v,mesas m,tipos_voto t,agrupaciones a,listas_agrupaciones l,categorias c,distritos d,secciones s,circuitos i,mesas_votos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_distrito = s.cod_distrito
AND v.cod_seccion  = s.cod_seccion
AND v.cod_distrito = m.cod_distrito
AND v.cod_seccion  = m.cod_seccion
AND v.cod_mesa     = m.cod_mesa
AND v.cod_distrito = i.cod_distrito
AND v.cod_seccion  = i.cod_seccion
AND m.cod_circuito = i.cod_circuito
AND v.cod_distrito = mv.cod_distrito
AND v.cod_seccion  = mv.cod_seccion
AND v.cod_mesa     = mv.cod_mesa
AND v.cod_categoria = mv.cod_categoria
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND a.cod_agrupacion_int = l.cod_agrupacion_int"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        condicion(self,"m.cod_circuito",cod_circuito)
        condicion(self,"a.cod_agrupacion",cod_agrupacion)
        condicion(self,"m.cod_mesa",cod_mesa)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,v.cod_seccion,s.nom_seccion,m.cod_circuito,i.nom_circuito,nom_seccion,m.cod_lugar,m.cod_mesa,v.cod_categoria,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,nom_agrupacion,mv.votos_positivos"
    def xMesaCategoriaVotos(self):
        return self.q+"\nORDER BY m.cod_mesa,v.cod_categoria,orden,votos DESC"
class ResultadosMesasSublistas:
    def __init__(self,cod_distrito,cod_seccion,cod_circuito=None,cod_categoria=None,cod_mesa=None,cod_agrupacion=None,cod_lista=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     , m.cod_circuito
     , i.nom_circuito
     , m.cod_lugar
     , m.cod_mesa
     , m.cod_mesa nom_mesa
     , v.cod_tipo_voto
     , t.nom_tipo_voto
     , v.cod_agrupacion_int
     , a.cod_agrupacion
     , CASE WHEN v.cod_agrupacion_int = '0000' THEN t.nom_tipo_voto ELSE a.nom_agrupacion END nom_agrupacion
     , v.cod_lista
     , l.nom_lista
     , CASE WHEN v.cod_tipo_voto = 'p' THEN 0 ELSE 1 END orden
     , SUM(v.votos) votos
     , ROUND(CAST(SUM(v.votos) AS DECIMAL)/NULLIF(SUM(mv.votos_positivos),0)*100,2) porcentaje
FROM votos v,mesas m,tipos_voto t,agrupaciones a,listas_agrupaciones l,categorias c,distritos d,secciones s,circuitos i,mesas_votos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_distrito = s.cod_distrito
AND v.cod_seccion  = s.cod_seccion
AND v.cod_distrito = m.cod_distrito
AND v.cod_seccion  = m.cod_seccion
AND v.cod_mesa     = m.cod_mesa
AND v.cod_distrito = i.cod_distrito
AND v.cod_seccion  = i.cod_seccion
AND m.cod_circuito = i.cod_circuito
AND v.cod_distrito = mv.cod_distrito
AND v.cod_seccion  = mv.cod_seccion
AND v.cod_mesa     = mv.cod_mesa
AND v.cod_categoria = mv.cod_categoria
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND a.cod_agrupacion_int = l.cod_agrupacion_int"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        condicion(self,"m.cod_circuito",cod_circuito)
        condicion(self,"a.cod_agrupacion",cod_agrupacion)
        condicion(self,"v.cod_lista",cod_lista)
        condicion(self,"m.cod_mesa",cod_mesa)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,v.cod_seccion,s.nom_seccion,m.cod_circuito,i.nom_circuito,m.cod_lugar,m.cod_mesa,v.cod_tipo_voto,t.nom_tipo_voto,v.cod_agrupacion_int,a.cod_agrupacion,nom_agrupacion,v.cod_lista,l.nom_lista"
    def xMesaCategoriaVotos(self):
        return self.q+"\nORDER BY m.cod_mesa,v.cod_categoria,orden,votos DESC"
class ResultadosMesasMesa:
    def __init__(self,cod_distrito,cod_seccion,cod_circuito=None,cod_categoria=None,cod_mesa=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     , m.cod_circuito
     , i.nom_circuito
     , m.cod_lugar
     , m.cod_mesa
     , m.cod_mesa nom_mesa
     , m.cant_electores
     , SUM(v.votos) votos
     , SUM(CASE WHEN v.cod_tipo_voto='p' THEN v.votos END) votos_positivos
     , 1 cant_mesas
FROM votos v,mesas m,tipos_voto t,categorias c,distritos d,secciones s,circuitos i
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito  = d.cod_distrito
AND v.cod_distrito  = s.cod_distrito
AND v.cod_seccion   = s.cod_seccion
AND v.cod_distrito  = m.cod_distrito
AND v.cod_seccion   = m.cod_seccion
AND v.cod_mesa      = m.cod_mesa
AND m.cod_distrito  = i.cod_distrito
AND m.cod_seccion   = i.cod_seccion
AND m.cod_circuito  = i.cod_circuito
AND v.cod_tipo_voto = t.cod_tipo_voto"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        condicion(self,"m.cod_circuito",cod_circuito)
        condicion(self,"m.cod_mesa",cod_mesa)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,v.cod_seccion,s.nom_seccion,m.cod_circuito,i.nom_circuito,m.cod_lugar,m.cod_mesa,m.cant_electores"
    def xMesaCategoriaVotos(self):
        return self.q+"\nORDER BY m.cod_mesa,v.cod_categoria,votos DESC"

############################ Resultados x Col ############################

class ResultadosDistritosxCol:
    def __init__(self,cod_categoria=None,cod_distrito=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     -- FILTROS
     , mv.votos
     , mv.votos_positivos
     , mv.cant_electores
     , mv.cant_mesas
FROM votos_distritos v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,mesas_distritos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND v.cod_distrito = mv.cod_distrito
AND v.cod_categoria = mv.cod_categoria
AND v.cod_categoria = mv.cod_categoria"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,mv.votos,mv.votos_positivos,mv.cant_electores,mv.cant_mesas"
    def xCategoriaDistrito(self):
        return self.q+"\nORDER BY v.cod_categoria,v.cod_distrito"
    def xDistritoCategoria(self):
        return self.q+"\nORDER BY v.cod_distrito,v.cod_categoria"
class AgrupacionesDistritosxCol:
    def __init__(self,cod_categoria=None,cod_distrito=None):
        self.i = 0
        self.q = """
SELECT a.cod_agrupacion
     , a.nom_agrupacion
     , SUM(v.votos) votos
FROM votos_distritos v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,mesas_distritos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND v.cod_distrito = mv.cod_distrito
AND v.cod_categoria = mv.cod_categoria
AND v.cod_categoria = mv.cod_categoria
AND a.cod_agrupacion != '0000'"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        self.q+="\nGROUP BY a.cod_agrupacion,a.nom_agrupacion"
    def xVotos(self):
        return self.q+"\nORDER BY votos DESC"
class ResultadosSeccionesxCol:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     -- FILTROS
     , mv.votos
     , mv.votos_positivos
     , mv.cant_electores
     , mv.cant_mesas
FROM votos_secciones v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,secciones s,mesas_secciones mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito  = d.cod_distrito
AND v.cod_distrito  = s.cod_distrito
AND v.cod_seccion   = s.cod_seccion
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_distrito  = mv.cod_distrito
AND v.cod_seccion   = mv.cod_seccion
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,v.cod_seccion,s.nom_seccion,mv.votos,mv.votos_positivos,mv.cant_electores,mv.cant_mesas"
    def xCategoriaDistritoSeccion(self):
        return self.q+"\nORDER BY v.cod_categoria,v.cod_distrito,v.cod_seccion"
    def xDistritoSeccionCategoria(self):
        return self.q+"\nORDER BY v.cod_distrito,v.cod_seccion,v.cod_categoria"
class AgrupacionesSeccionesxCol:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT a.cod_agrupacion
     , a.nom_agrupacion
     , SUM(v.votos) votos
FROM votos_secciones v,agrupaciones a,listas_agrupaciones l,categorias c,tipos_voto t,distritos d,secciones s,mesas_secciones mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito  = d.cod_distrito
AND v.cod_distrito  = s.cod_distrito
AND v.cod_seccion   = s.cod_seccion
AND v.cod_tipo_voto = t.cod_tipo_voto
AND v.cod_distrito  = mv.cod_distrito
AND v.cod_seccion   = mv.cod_seccion
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_agrupacion_int = l.cod_agrupacion_int
AND v.cod_lista = l.cod_lista
AND a.cod_agrupacion != '0000'"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        self.q+="\nGROUP BY a.cod_agrupacion,a.nom_agrupacion"
    def xVotos(self):
        return self.q+"\nORDER BY votos DESC"
class ResultadosCircuitosxCol:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT v.cod_categoria
     , c.nom_categoria
     , v.cod_distrito
     , d.nom_distrito
     , v.cod_seccion
     , s.nom_seccion
     , v.cod_circuito
     , i.nom_circuito
     -- FILTROS
     , mv.votos
     , mv.votos_positivos
     , mv.cant_electores
     , mv.cant_mesas
FROM votos_circuitos v,agrupaciones a,categorias c,tipos_voto t,distritos d,secciones s,circuitos i,mesas_circuitos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_distrito = s.cod_distrito
AND v.cod_seccion  = s.cod_seccion
AND v.cod_distrito = i.cod_distrito
AND v.cod_seccion  = i.cod_seccion
AND v.cod_circuito = i.cod_circuito
AND v.cod_distrito = mv.cod_distrito
AND v.cod_seccion  = mv.cod_seccion
AND v.cod_circuito = mv.cod_circuito
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_tipo_voto = t.cod_tipo_voto"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        condicion(self,"v.cod_circuito",cod_circuito)
        self.q+="\nGROUP BY v.cod_categoria,c.nom_categoria,v.cod_distrito,d.nom_distrito,v.cod_seccion,s.nom_seccion,v.cod_circuito,i.nom_circuito,mv.votos,mv.votos_positivos,mv.cant_electores,mv.cant_mesas"
    def xCategoriaDistritoSeccionCircuito(self):
        return self.q+"\nORDER BY v.cod_categoria,v.cod_distrito,v.cod_seccion,v.cod_circuito"
    def xDistritoSeccionCircuitoCategoria(self):
        return self.q+"\nORDER BY v.cod_distrito,v.cod_seccion,v.cod_circuito,v.cod_categoria"
class AgrupacionesCircuitosxCol:
    def __init__(self,cod_categoria=None,cod_distrito=None,cod_seccion=None,cod_circuito=None,cod_agrupacion=None):
        self.i = 0
        self.q = """
SELECT a.cod_agrupacion
     , a.nom_agrupacion
     , SUM(v.votos) votos
FROM votos_circuitos v,agrupaciones a,categorias c,tipos_voto t,distritos d,secciones s,circuitos i,mesas_circuitos mv
WHERE v.cod_categoria = c.cod_categoria
AND v.cod_distrito = d.cod_distrito
AND v.cod_distrito = s.cod_distrito
AND v.cod_seccion  = s.cod_seccion
AND v.cod_distrito = i.cod_distrito
AND v.cod_seccion  = i.cod_seccion
AND v.cod_circuito = i.cod_circuito
AND v.cod_distrito = mv.cod_distrito
AND v.cod_seccion  = mv.cod_seccion
AND v.cod_circuito = mv.cod_circuito
AND v.cod_categoria = mv.cod_categoria
AND v.cod_agrupacion_int = a.cod_agrupacion_int
AND v.cod_tipo_voto = t.cod_tipo_voto
AND a.cod_agrupacion != '0000'"""
        condicion(self,"v.cod_categoria",cod_categoria)
        condicion(self,"v.cod_distrito",cod_distrito)
        condicion(self,"v.cod_seccion",cod_seccion)
        condicion(self,"v.cod_circuito",cod_circuito)
        self.q+="\nGROUP BY a.cod_agrupacion,a.nom_agrupacion"
    def xVotos(self):
        return self.q+"\nORDER BY votos DESC"
