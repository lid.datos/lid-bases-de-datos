from pydantic import BaseModel

class Elecciones(BaseModel):
    cod_eleccion: str
    nom_eleccion: str

BD_ELECCIONES = [
    Elecciones(cod_eleccion="e2015_paso", nom_eleccion="Elecciones 2015 PASO"),
    Elecciones(cod_eleccion="e2015_generales", nom_eleccion="Elecciones 2015 Generales"),
    Elecciones(cod_eleccion="e2017_paso", nom_eleccion="Elecciones 2017 PASO"),
    Elecciones(cod_eleccion="e2017_generales", nom_eleccion="Elecciones 2017 Generales"),
    Elecciones(cod_eleccion="e2019_paso", nom_eleccion="Elecciones 2019 PASO"),
    Elecciones(cod_eleccion="e2019_generales", nom_eleccion="Elecciones 2019 Generales"),
    Elecciones(cod_eleccion="e2021_paso", nom_eleccion="Elecciones 2021 PASO"),
    Elecciones(cod_eleccion="e2021_generales", nom_eleccion="Elecciones 2021 Generales"),
    Elecciones(cod_eleccion="e2021_jujuy", nom_eleccion="Elecciones 2021 Jujuy"),
    Elecciones(cod_eleccion="e2023_neuquen", nom_eleccion="Elecciones 2023 Neuquén"),
    Elecciones(cod_eleccion="e2023_rionegro", nom_eleccion="Elecciones 2023 Río Negro"),
    Elecciones(cod_eleccion="e2023_jujuy", nom_eleccion="Elecciones 2023 Jujuy"),
    Elecciones(cod_eleccion="e2023_paso", nom_eleccion="Elecciones 2023 PASO"),
    Elecciones(cod_eleccion="e2023_generales", nom_eleccion="Elecciones 2023 Generales"),
    Elecciones(cod_eleccion="e2023_2vuelta", nom_eleccion="Elecciones 2023 2da vuelta"),
]