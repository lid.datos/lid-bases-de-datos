from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from controlador.controlador_elecciones import elecciones, categorias, distritos, secciones, circuitos, mesas, agrupaciones, agrupacionesSublistas \
   , resultadosTotales, resultadosTotalesSublistas, resultadosDistritos, resultadosDistritosSublistas, resultadosSecciones, resultadosSeccionesSublistas \
   , resultadosCircuitos, resultadosCircuitosSublistas, resultadosMesas, resultadosMesasMesa, resultadosMesasSublistas \
   , resultadosTotalesMesas, resultadosDistritosMesas, resultadosSeccionesMesas, resultadosCircuitosMesas, resultadosMesas \
   , resultadosDistritosxCol, resultadosSeccionesxCol, resultadosCircuitosxCol 
from controlador.controlador_establecimientos import clae2, clae6, claeLetra, provincias, departamentos, establecimientos, rangos
from controlador.controlador_censop import provincias_censo, departamentos_censo, censo
from controlador.controlador_perfiles import eleccionesSeccionesPerfiles, eleccionesCircuitosPerfiles, categoriasSeccionesPerfiles, categoriasCircuitosPerfiles \
   , distritosSeccionesPerfiles, distritosCircuitosPerfiles, seccionesSeccionesPerfiles, seccionesCircuitosPerfiles, circuitosCircuitosPerfiles \
   , perfilesSeccionesPerfiles, perfilesCircuitosPerfiles, perfilesSecciones, perfilesCircuitos, algoritmosSeccionesPerfiles, algoritmosCircuitosPerfiles \
   , clasificacionPais, clasificacionDistrito, perfilPais, perfilDistrito, clasificacionPerfilPais, clasificacionPerfilDistrito

def http_exception(func):

    def inner_function(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as e:
            raise HTTPException(status_code=400, detail=e.message)
    return inner_function

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

############################ API Elecciones ############################

app.get("/api/elecciones",
   description="devuelve una lista de las bases disponibles\n"
   )(elecciones)

app.get("/api/categorias",
   description= """ ejemplo url consulta: \n
                     /api/categorias?eleccion=e2021_generales \n
                """
   )(categorias)

app.get("/api/distritos",
   description= """ ejemplo url consulta: \n
                     /api/distritos?eleccion=e2021_generales&categoria=01 \n
                """
   )(distritos)

app.get("/api/secciones",
   description=""" ejemplo url consulta:\n
                     /api/secciones?eleccion=e2021_generales&distrito=01 \n
               """
   )(secciones)

app.get("/api/circuitos",
   description=""" ejemplo url consulta:\n
                     /api/circuitos?eleccion=e2021_generales&distrito=01&seccion=001 \n
               """
   )(circuitos)

app.get("/api/mesas",
   description=""" ejemplo url consulta:\n
                     /api/mesas?eleccion=e2021_generales&distrito=01&seccion=001&circuito=00001 \n
               """
   )(mesas)

app.get("/api/agrupaciones",
   description=""" ejemplo url consulta:\n
                     /api/agrupaciones?eleccion=e2021_generales&distrito=01&categoria=03 \n
               """
   )(agrupaciones)

app.get("/api/agrupacionesSublistas",
   description=""" ejemplo url consulta:\n
                     /api/agrupacionesSublistas?eleccion=e2021_generales&distrito=01&categoria=03 \n
               """
   )(agrupacionesSublistas)

app.get("/api/resultadosTotales",
   description = """ ejemplo url consulta:\n
                     /api/resultadosTotales?eleccion=e2019_generales&categoria=01\n
                 """
   )(resultadosTotales)

app.get("/api/resultadosTotalesSublistas",
   description = """ ejemplo url consulta:\n
                     /api/resultadosTotalesSublistas?eleccion=e2019_paso&categoria=01\n
                 """
   )(resultadosTotalesSublistas)

app.get("/api/resultadosTotalesMesas",
   description = """ ejemplo url consulta:\n
                     /api/resultadosTotalesMesas?eleccion=e2019_generales&categoria=01\n
                 """
   )(resultadosTotalesMesas)

app.get("/api/resultadosDistritos",
   description = """ ejemplo url consulta:\n
                     /api/resultadosDistritos?eleccion=e2021_generales&categoria=03&distrito=01\n
                 """
   )(resultadosDistritos)

app.get("/api/resultadosDistritosSublistas",
   description = """ ejemplo url consulta:\n
                     /api/resultadosDistritosSublistas?eleccion=e2021_generales&categoria=03&distrito=01\n
                 """
   )(resultadosDistritosSublistas)

app.get("/api/resultadosDistritosMesas",
   description=""" ejemplo url consulta:\n
                     /api/resultadosDistritosMesas?eleccion=e2021_generales\n 
                     /api/resultadosDistritosMesas?eleccion=e2021_generales&distrito=23\n
               """
   )(resultadosDistritosMesas)

app.get("/api/resultadosSecciones",
   description = """ ejemplo url consulta:\n
                     /api/resultadosSecciones?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001&agrupacion=0503\n
                 """
   )(resultadosSecciones)

app.get("/api/resultadosSeccionesSublistas",
   description = """ ejemplo url consulta:\n
                     /api/resultadosSeccionesSublistas?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001&agrupacion=0503\n
                 """
   )(resultadosSeccionesSublistas)

app.get("/api/resultadosSeccionesMesas",
   description=""" ejemplo url consulta:\n
                     /api/resultadosSeccionesMesas?eleccion=e2021_generales&distrito=23\n
                     /api/resultadosSeccionesMesas?eleccion=e2021_generales&distrito=23&seccion=001\n
               """
   )(resultadosSeccionesMesas)

app.get("/api/resultadosCircuitos",
   description = """ ejemplo url consulta:\n
                     /api/resultadosCircuitos?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001&circuito=00001\n
                 """
   )(resultadosCircuitos)

app.get("/api/resultadosCircuitosSublistas",
   description = """ ejemplo url consulta:\n
                     /api/resultadosCircuitosSublistas?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001&circuito=00001\n
                 """
   )(resultadosCircuitosSublistas)

app.get("/api/resultadosCircuitosMesas",
   description=""" ejemplo url consulta:\n
                     /api/resultadosCircuitosMesas?eleccion=e2021_generales&distrito=23&seccion=001&circuito=00001\n
               """
   )(resultadosCircuitosMesas)

app.get("/api/resultadosMesas",
   description = """ ejemplo url consulta:\n
                     /api/resultadosMesas?eleccion=e2021_generales&distrito=01&seccion=001&circuito=00001&categoria=03\n
                 """
   )(resultadosMesas)
 
app.get("/api/resultadosMesasMesa",
   description = """ ejemplo url consulta:\n
                     /api/resultadosMesasMesa?eleccion=e2021_generales&distrito=01&seccion=001&circuito=00001&categoria=03&mesa=00001X\n
                 """
   )(resultadosMesasMesa)
 
app.get("/api/resultadosMesasSublistas",
   description=""" ejemplo url consulta:\n
                     /api/resultadosMesasSublistas?eleccion=e2021_generales&distrito=01&seccion=001&circuito=00001&categoria=03\n
               """
   )(resultadosMesasSublistas)

############################ API Resultados x Col ############################

app.get("/api/resultadosDistritosxCol",
   description = """ ejemplo url consulta:\n
                     /api/resultadosDistritosxCol?eleccion=e2021_generales&categoria=03&distrito=01\n
                 """
   )(resultadosDistritosxCol)

app.get("/api/resultadosSeccionesxCol",
   description = """ ejemplo url consulta:\n
                     /api/resultadosSeccionesxCol?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001\n
                 """
   )(resultadosSeccionesxCol)

app.get("/api/resultadosCircuitosxCol",
   description = """ ejemplo url consulta:\n
                     /api/resultadosCircuitosxCol?eleccion=e2021_generales&categoria=03&distrito=01&seccion=001&circuito=00001\n
                 """
   )(resultadosCircuitosxCol)


######################### Perfiles x Seccion #########################

app.get("/api/perfiles/seccionesSeccionesPerfiles",
   description= """ ejemplo url consulta: \n
                    /api/perfiles/seccionesSeccionesPerfiles?eleccion=e2021_generales \n
                """
   )(seccionesSeccionesPerfiles)

app.get("/api/perfiles/distritosSeccionesPerfiles",
   description= """ ejemplo url consulta: \n
                    /api/perfiles/distritosSeccionesPerfiles?eleccion=e2021_generales \n
                """
   )(distritosSeccionesPerfiles)

app.get("/api/perfiles/eleccionesSeccionesPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/eleccionesSeccionesPerfiles?eleccion=e2021_generales \n
                """
   )(eleccionesSeccionesPerfiles)

app.get("/api/perfiles/algoritmosSeccionesPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/algoritmosSeccionesPerfiles?eleccion=e2021_generales \n
                """
   )(algoritmosSeccionesPerfiles)

app.get("/api/perfiles/categoriasSeccionesPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/categoriasSeccionesPerfiles?eleccion=e2021_generales \n
                """
   )(categoriasSeccionesPerfiles)

app.get("/api/perfiles/perfilesSeccionesPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/perfilesSeccionesPerfiles?eleccion=e2021_generales \n
                """
   )(perfilesSeccionesPerfiles)

app.get("/api/perfiles/perfilesSecciones",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/perfilesSecciones?eleccion=e2021_generales \n
                """
   )(perfilesSecciones)

######################### Perfiles x Circuito #########################

app.get("/api/perfiles/circuitosCircuitosPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/circuitosCircuitosPerfiles?eleccion=e2021_generales \n
                """
   )(circuitosCircuitosPerfiles)

app.get("/api/perfiles/seccionesCircuitosPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/seccionesCircuitosPerfiles?eleccion=e2021_generales \n
                """
   )(seccionesCircuitosPerfiles)

app.get("/api/perfiles/distritosCircuitosPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/distritosCircuitosPerfiles?eleccion=e2021_generales \n
                """
   )(distritosCircuitosPerfiles)

app.get("/api/perfiles/algoritmosCircuitosPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/algoritmosCircuitosPerfiles?eleccion=e2021_generales \n
                """
   )(algoritmosCircuitosPerfiles)

app.get("/api/perfiles/eleccionesCircuitosPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/eleccionesCircuitosPerfiles?eleccion=e2021_generales \n
                """
   )(eleccionesCircuitosPerfiles)

app.get("/api/perfiles/categoriasCircuitosPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/categoriasCircuitosPerfiles?eleccion=e2021_generales \n
                """
   )(categoriasCircuitosPerfiles)

app.get("/api/perfiles/perfilesCircuitosPerfiles",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/perfilesCircuitosPerfiles?eleccion=e2021_generales \n
                """
   )(perfilesCircuitosPerfiles)

app.get("/api/perfiles/perfilesCircuitos",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/perfilesCircuitos?eleccion=e2021_generales \n
                """
   )(perfilesCircuitos)

######################### Info Clasificación y Perfil #########################

app.get("/api/perfiles/clasificacionPais",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/clasificacionPais?eleccion=e2021_generales \n
                """
   )(clasificacionPais)

app.get("/api/perfiles/clasificacionDistrito",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/clasificacionDistrito?eleccion=e2021_generales \n
                """
   )(clasificacionDistrito)

app.get("/api/perfiles/perfilPais",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/perfilPais?eleccion=e2021_generales \n
                """
   )(perfilPais)

app.get("/api/perfiles/perfilDistrito",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/perfilDistrito?eleccion=e2021_generales \n
                """
   )(perfilDistrito)

app.get("/api/perfiles/clasificacionPerfilPais",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/clasificacionPerfilPais?eleccion=e2021_generales \n
                """
   )(clasificacionPerfilPais)

app.get("/api/perfiles/clasificacionPerfilDistrito",
   description= """ ejemplo url consulta: \n
                     /api/perfiles/clasificacionPerfilDistrito?eleccion=e2021_generales \n
                """
   )(clasificacionPerfilDistrito)

############################ API Establecimientos ############################

app.get("/api/clae2",
   description=""" ejemplo url consulta:\n
                     /api/clae2?base=d2022_establecimientos\n
               """
   )(clae2)

app.get("/api/clae6",
   description=""" ejemplo url consulta:\n
                     /api/clae6?base=d2022_establecimientos\n
               """
   )(clae6)

app.get("/api/claeLetra",
   description=""" ejemplo url consulta:\n
                     /api/claeLetra?base=d2022_establecimientos\n
               """
   )(claeLetra)

app.get("/api/rangos",
   description=""" ejemplo url consulta:\n
                     /api/rangos?base=d2022_establecimientos\n
               """
   )(rangos)

app.get("/api/provincias",
   description=""" ejemplo url consulta:\n
                     /api/provincias?base=d2022_establecimientos\n
               """
   )(provincias)

app.get("/api/departamentos",
   description=""" ejemplo url consulta:\n
                     /api/departamentos?base=d2022_establecimientos&provincia=2\n
               """
   )(departamentos)

app.get("/api/establecimientos",
   description=""" ejemplo url consulta:\n
                     /api/establecimientos?base=d2022_establecimientos&departamento=10007&letra=A\n
               """
   )(establecimientos)

############################ API Censo ############################

app.get("/api/censop_provincias",
   description=""" ejemplo url consulta:\n
                     /api/censop_provincias?base=d2022_censo_provisorios\n
               """
   )(provincias_censo)

app.get("/api/censop_departamentos",
   description=""" ejemplo url consulta:\n
                     /api/censop_departamentos?base=d2022_censo_provisorios&provincia=2\n
               """
   )(departamentos_censo)

app.get("/api/censop_censo",
   description=""" ejemplo url consulta:\n
                     /api/censop_censo?base=d2022_censo_provisorios&provincia=2\n
               """
   )(censo)
