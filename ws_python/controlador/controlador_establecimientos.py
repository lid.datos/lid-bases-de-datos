from typing import Union
import asyncpg
import os
from modelo.establecimientos import Clae2, Clae6, ClaeLetra, Rangos, Provincias, Departamentos, Establecimientos

async def bd(base):
    conn = await asyncpg.connect(dsn=os.environ.get("DB_DSN"),database=base)
    return conn

async def clae2(base: str, clae2: Union[str, None]=None):
    conn = await bd(base)
    values = await conn.fetch(Clae2(clae2).xNom())
    await conn.close()
    return values

async def clae6(base: str, clae6: Union[str, None]=None):
    conn = await bd(base)
    values = await conn.fetch(Clae6(clae6).xNom())
    await conn.close()
    return values

async def claeLetra(base: str, clae_letra: Union[str, None]=None):
    conn = await bd(base)
    values = await conn.fetch(ClaeLetra(clae_letra).xNom())
    await conn.close()
    return values

async def rangos(base: str, rango: Union[str, None]=None):
    conn = await bd(base)
    values = await conn.fetch(Rangos(rango).xCodigo())
    await conn.close()
    return values

async def provincias(base: str, provincia: Union[str, None]=None):
    conn = await bd(base)
    values = await conn.fetch(Provincias(provincia).xNom())
    await conn.close()
    return values

async def departamentos(base: str, provincia: Union[str, None]=None):
    conn = await bd(base)
    values = await conn.fetch(Departamentos(provincia).xNom())
    await conn.close()
    return values

async def establecimientos(base: str, 
                           provincia: Union[str, None]=None,
                           departamento: Union[str, None]=None,
                           clae2: Union[str, None]=None,
                           clae6: Union[str, None]=None,
                           claeLetra: Union[str, None]=None,
                           rango: Union[str, None]=None):
    conn = await bd(base)
    if (base is None):
        raise Exception("Se requiere la base de datos a consultar")
    if (provincia is None and departamento is None and clae2 is None and clae6 is None and claeLetra is None and rango is None):
        raise Exception("Por cuestiones de performance, no se puede realizar la consulta sin parámetros")
    if (provincia is not None and departamento is None and clae2 is None and clae6 is None and claeLetra is None and rango is None):
        raise Exception("Por cuestiones de performance, no se puede realizar la consulta con un sólo parámetro")
    if (provincia is None and departamento is not None and clae2 is None and clae6 is None and claeLetra is None and rango is None):
        raise Exception("Por cuestiones de performance, no se puede realizar la consulta con un sólo parámetro")
    if (provincia is None and departamento is None and not clae2 is None and clae6 is None and claeLetra is None and rango is None):
        raise Exception("Por cuestiones de performance, no se puede realizar la consulta con un sólo parámetro")
    if (provincia is None and departamento is None and clae2 is None and not clae6 is None and claeLetra is None and rango is None):
        raise Exception("Por cuestiones de performance, no se puede realizar la consulta con un sólo parámetro")
    if (provincia is None and departamento is None and clae2 is None and clae6 is None and not claeLetra is None and rango is None):
        raise Exception("Por cuestiones de performance, no se puede realizar la consulta con un sólo parámetro")
    if (provincia is None and departamento is None and clae2 is None and clae6 is None and claeLetra is None and not rango is None):
        raise Exception("Por cuestiones de performance, no se puede realizar la consulta con un sólo parámetro")
    values = await conn.fetch(Establecimientos(provincia,departamento,clae2,clae6,claeLetra,rango).xCodProvinciaCodDepartamento())
    await conn.close()
    return values
