from typing import Union
import asyncpg
import os
from modelo.elecciones import Distritos, Categorias, Secciones, Circuitos, Mesas, Agrupaciones, AgrupacionesSublistas \
                     , ResultadosTotales, ResultadosTotalesSublistas, ResultadosDistritos, ResultadosDistritosSublistas, ResultadosSecciones, ResultadosSeccionesSublistas \
                     , ResultadosCircuitos, ResultadosCircuitosSublistas, ResultadosMesas, ResultadosMesasMesa, ResultadosMesasSublistas \
                     , ResultadosTotalesMesas, ResultadosDistritosMesas, ResultadosSeccionesMesas, ResultadosCircuitosMesas \
                     , ResultadosDistritosxCol, ResultadosSeccionesxCol, ResultadosCircuitosxCol \
                     , AgrupacionesDistritosxCol, AgrupacionesSeccionesxCol, AgrupacionesCircuitosxCol
from modelo.bd_elecciones import BD_ELECCIONES

def agregar_mismo_valor_a_columna(lista_records, **valores_a_agregar):
    result = []
    for i in lista_records:
        d = dict(i)
        for k, v in valores_a_agregar.items():
            d[k] = v 
        result.append(d)
    return result

async def validar_existe_bd(eleccion):
    found = False
    for eleccion_disponible in BD_ELECCIONES:
        if eleccion_disponible.cod_eleccion == eleccion:
            found = True
            continue
    if not found:
        raise Exception("La elección {} no se ha encontrado.".format(eleccion))

async def bd(codEleccion):
    conn = await asyncpg.connect(dsn=os.environ.get("DB_DSN"),database=codEleccion)
    return conn

async def elecciones():
    return BD_ELECCIONES

async def categorias(eleccion: str, 
                     distrito: Union[str, None]=None, 
                     seccion : Union[str, None]=None, 
                     circuito: Union[str, None]=None):
    conn = await bd(eleccion)
    values = await conn.fetch(Categorias(distrito,seccion,circuito).xCodCategoria())
    await conn.close()
    return values

async def distritos(eleccion: str, categoria: Union[str, None]=None):
    conn = await bd(eleccion)
    values = await conn.fetch(Distritos(categoria).xCodDistrito())
    await conn.close()
    return values

async def secciones(eleccion: str, distrito: str):
    conn = await bd(eleccion)
    values = await conn.fetch(Secciones(distrito).xCodSeccionProv())
    await conn.close()
    return values

async def circuitos(eleccion: str, distrito: str, seccion: str):
    conn = await bd(eleccion)
    values = await conn.fetch(Circuitos(distrito,seccion).xCodCircuito())
    await conn.close()
    return values

async def mesas(eleccion: str, distrito: str, seccion: str, circuito: str):
    conn = await bd(eleccion)
    values = await conn.fetch(Mesas(distrito,seccion,circuito).xCodMesa())
    await conn.close()
    return values

async def agrupaciones(eleccion  : str,
                       distrito  : Union[str, None]=None,
                       categoria : Union[str, None]=None,
                       agrupacion: Union[str, None]=None):
    conn = await bd(eleccion)
    values = await conn.fetch(Agrupaciones(distrito,categoria,agrupacion).xCodAgrupacion())
    await conn.close()
    return values

async def agrupacionesSublistas(eleccion  : str,
                                distrito  : Union[str, None]=None,
                                categoria : Union[str, None]=None,
                                agrupacion: Union[str, None]=None):
    conn = await bd(eleccion)
    values = await conn.fetch(AgrupacionesSublistas(distrito,categoria,agrupacion).xCodAgrupacionCodLista())
    await conn.close()
    return values

async def resultadosTotales(eleccion : str,
                            categoria: Union[str, None]=None):
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosTotales(categoria).xCategoriaVotos())
    await conn.close()
    return values

async def resultadosTotalesSublistas(eleccion : str,
                                     categoria: Union[str, None]=None):
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosTotalesSublistas(categoria).xCategoriaVotos())
    await conn.close()
    return values

async def resultadosTotalesMesas(eleccion : str,
                                 categoria: Union[str, None]=None):
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosTotalesMesas(categoria).xCategoria())
    await conn.close()
    return values

async def resultadosDistritos(eleccion  : Union[str, None]=None,
                              categoria : Union[str, None]=None,
                              distrito  : Union[str, None]=None,
                              agrupacion: Union[str, None]=None):
    
    if not eleccion and not categoria:
        raise Exception("No se ingresó ni eleccion ni categoria, debe ingresar de manera obligatoria uno de los dos parámetros.")
    
    if eleccion:
        conn = await bd(eleccion)
        values = await conn.fetch(ResultadosDistritos(categoria,distrito,agrupacion).xDistritoCategoriaVotos())
        await conn.close()

    else:
        values = []
        print("Ninguna elección especificada")
        for elec in BD_ELECCIONES:
            try:
                conn = await bd(elec.cod_eleccion)
                values += agregar_mismo_valor_a_columna(
                    await conn.fetch(ResultadosDistritos(categoria,distrito,agrupacion).xDistritoCategoriaVotos()),
                    cod_eleccion=elec.cod_eleccion, nom_eleccion=elec.nom_eleccion
                )
                await conn.close()
            except Exception as e:
                print(e.message, elec.nom_eleccion)
    return values

async def resultadosDistritosSublistas(eleccion  : str,
                                       categoria : Union[str, None]=None,
                                       distrito  : Union[str, None]=None,
                                       agrupacion: Union[str, None]=None):
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosDistritosSublistas(categoria,distrito,agrupacion).xDistritoCategoriaVotos())
    await conn.close()
    return values

async def resultadosDistritosMesas(eleccion : str,
                                   categoria: Union[str, None]=None,
                                   distrito : Union[str, None]=None):
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosDistritosMesas(categoria,distrito).xCategoriaDistritoVotos())
    await conn.close()
    return values

async def resultadosSecciones(eleccion  : str,
                              categoria : Union[str, None]=None,
                              distrito  : Union[str, None]=None,
                              seccion   : Union[str, None]=None,
                              agrupacion: Union[str, None]=None):
    if (distrito is None):
        raise Exception("Para resultados de secciones se requiere distrito.")
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosSecciones(categoria,distrito,seccion,agrupacion).xCategoriaDistritoSeccionVotos())
    await conn.close()
    return values

async def resultadosSeccionesSublistas(eleccion  : str,
                                       categoria : Union[str, None]=None,
                                       distrito  : Union[str, None]=None,
                                       seccion   : Union[str, None]=None,
                                       agrupacion: Union[str, None]=None):
    if (distrito is None):
        raise Exception("Para resultados de secciones se requiere distrito.")
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosSeccionesSublistas(categoria,distrito,seccion,agrupacion).xCategoriaDistritoSeccionVotos())
    await conn.close()
    return values

async def resultadosSeccionesMesas(eleccion : str,
                                   categoria: Union[str, None]=None,
                                   distrito : Union[str, None]=None,
                                   seccion  : Union[str, None]=None):
    if (distrito is None):
        raise Exception("Para resultados de secciones se requiere distrito.")
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosSeccionesMesas(categoria,distrito,seccion).xCategoriaDistritoSeccionesVotos())
    await conn.close()
    return values

async def resultadosCircuitos(eleccion  : str,
                              categoria : Union[str, None]=None,
                              distrito  : Union[str, None]=None,
                              seccion   : Union[str, None]=None,
                              circuito  : Union[str, None]=None,
                              agrupacion: Union[str, None]=None):
    if (distrito is None):
        raise Exception("Para resultados de circuitos se requiere distrito.")
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosCircuitos(categoria,distrito,seccion,circuito,agrupacion).xCategoriaDistritoSeccionCircuitoVotos())
    await conn.close()
    return values

async def resultadosCircuitosSublistas(eleccion  : str,
                                       categoria : Union[str, None]=None,
                                       distrito  : Union[str, None]=None,
                                       seccion   : Union[str, None]=None,
                                       circuito  : Union[str, None]=None,
                                       agrupacion: Union[str, None]=None):
    if (distrito is None):
        raise Exception("Para resultados de circuitos se requiere distrito.")
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosCircuitosSublistas(categoria,distrito,seccion,circuito,agrupacion).xCategoriaDistritoSeccionCircuitoVotos())
    await conn.close()
    return values

async def resultadosCircuitosMesas(eleccion : str,
                                   categoria: Union[str, None]=None,
                                   distrito : Union[str, None]=None,
                                   seccion  : Union[str, None]=None,
                                   circuito : Union[str, None]=None):
    if (distrito is None):
        raise Exception("Para resultados de circuitos se requiere distrito.")
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosCircuitosMesas(categoria,distrito,seccion,circuito).xCategoriaDistritoSeccionesCircuitosVotos())
    await conn.close()
    return values

async def resultadosMesas(eleccion  : str,
                          categoria : Union[str, None]=None,
                          distrito  : Union[str, None]=None,
                          seccion   : Union[str, None]=None,
                          circuito  : Union[str, None]=None,
                          mesa      : Union[str, None]=None,
                          agrupacion: Union[str, None]=None):
    if (distrito is None or seccion is None):
        raise Exception("Para resultados de mesas se requiere distrito y sección")
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosMesas(distrito,seccion,circuito,categoria,mesa,agrupacion).xMesaCategoriaVotos())
    await conn.close()
    return values

async def resultadosMesasSublistas(eleccion  : str,
                                   categoria : Union[str, None]=None,
                                   distrito  : Union[str, None]=None,
                                   seccion   : Union[str, None]=None,
                                   circuito  : Union[str, None]=None,
                                   mesa      : Union[str, None]=None,
                                   agrupacion: Union[str, None]=None):
    if (distrito is None or seccion is None):
        raise Exception("Para resultados de mesas se requiere distrito y sección")
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosMesasSublistas(distrito,seccion,circuito,categoria,mesa,agrupacion).xMesaCategoriaVotos())
    await conn.close()
    return values

async def resultadosMesasMesa(eleccion : str,
                              categoria: Union[str, None]=None,
                              distrito : Union[str, None]=None,
                              seccion  : Union[str, None]=None,
                              circuito : Union[str, None]=None,
                              mesa     : Union[str, None]=None):
    if (distrito is None or seccion is None):
        raise Exception("Para resultados de mesas se requiere distrito y sección")
    conn = await bd(eleccion)
    values = await conn.fetch(ResultadosMesasMesa(distrito,seccion,circuito,categoria,mesa).xMesaCategoriaVotos())
    await conn.close()
    return values

############################ Resultados x Col ############################

def agrupacionesToFiltros(values):
    result = []
    for record in values:
        result.append("\n, SUM(v.votos) FILTER (WHERE a.cod_agrupacion = '" + record['cod_agrupacion'] + "') AS \"" + record['nom_agrupacion'] +"\"")
    result.append("\n, SUM(v.votos) FILTER (WHERE a.cod_agrupacion = '0000' AND v.cod_tipo_voto = 'b') AS \"Blancos\"")
    result.append("\n, SUM(v.votos) FILTER (WHERE a.cod_agrupacion = '0000' AND v.cod_tipo_voto = 'n') AS \"Nulos\"")
    result.append("\n, SUM(v.votos) FILTER (WHERE a.cod_agrupacion = '0000' AND v.cod_tipo_voto NOT IN ('b','n')) AS \"Rec./Imp./Com\"")
    return ''.join(result)

async def resultadosDistritosxCol(eleccion  : Union[str, None]=None,
                              categoria : Union[str, None]=None,
                              distrito  : Union[str, None]=None):
    
    if not eleccion and not categoria:
        raise Exception("No se ingresó ni eleccion ni categoria, debe ingresar de manera obligatoria uno de los dos parámetros.")
    
    if eleccion:
        conn = await bd(eleccion)
        filtros = agrupacionesToFiltros(await conn.fetch(AgrupacionesDistritosxCol(categoria,distrito).xVotos()))
        consulta = ResultadosDistritosxCol(categoria,distrito).xDistritoCategoria().replace('-- FILTROS','-- FILTROS'+filtros)
        values = await conn.fetch(consulta)
        await conn.close()

    else:
        values = []
        print("Ninguna elección especificada")
        for elec in BD_ELECCIONES:
            try:
                conn = await bd(elec.cod_eleccion)
                values += agregar_mismo_valor_a_columna(
                    await conn.fetch(ResultadosDistritosxCol(categoria,distrito).xDistritoCategoria()),
                    cod_eleccion=elec.cod_eleccion, nom_eleccion=elec.nom_eleccion
                )
                await conn.close()
            except Exception as e:
                print(e.message, elec.nom_eleccion)
    return values

async def resultadosSeccionesxCol(eleccion  : str,
                              categoria : Union[str, None]=None,
                              distrito  : Union[str, None]=None,
                              seccion   : Union[str, None]=None):
    if (distrito is None):
        raise Exception("Para resultados de secciones se requiere distrito.")
    conn = await bd(eleccion)
    filtros = agrupacionesToFiltros(await conn.fetch(AgrupacionesSeccionesxCol(categoria,distrito,seccion).xVotos()))
    consulta = ResultadosSeccionesxCol(categoria,distrito,seccion).xCategoriaDistritoSeccion().replace('-- FILTROS','-- FILTROS'+filtros)
    values = await conn.fetch(consulta)
    await conn.close()
    return values

async def resultadosCircuitosxCol(eleccion  : str,
                              categoria : Union[str, None]=None,
                              distrito  : Union[str, None]=None,
                              seccion   : Union[str, None]=None,
                              circuito  : Union[str, None]=None):
    if (distrito is None):
        raise Exception("Para resultados de circuitos se requiere distrito.")
    conn = await bd(eleccion)
    filtros = agrupacionesToFiltros(await conn.fetch(AgrupacionesCircuitosxCol(categoria,distrito,seccion,circuito).xVotos()))
    consulta = ResultadosCircuitosxCol(categoria,distrito,seccion,circuito).xCategoriaDistritoSeccionCircuito().replace('-- FILTROS','-- FILTROS'+filtros)
    values = await conn.fetch(consulta)
    await conn.close()
    return values
