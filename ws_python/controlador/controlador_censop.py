from typing import Union
import asyncpg
import os
from modelo.censop import Provincias, Departamentos, Censo

async def bd(base):
    conn = await asyncpg.connect(dsn=os.environ.get("DB_DSN"),database=base)
    return conn

async def provincias_censo(base: str, provincia: Union[str, None]=None):
    conn = await bd(base)
    values = await conn.fetch(Provincias(provincia).xNom())
    await conn.close()
    return values

async def departamentos_censo(base: str, provincia: Union[str, None]=None, departamento: Union[str, None]=None):
    conn = await bd(base)
    values = await conn.fetch(Departamentos(provincia,departamento).xNom())
    await conn.close()
    return values

async def censo(base: str, 
                provincia: Union[str, None]=None,
                departamento: Union[str, None]=None):
    conn = await bd(base)
    if (base is None):
        raise Exception("Se requiere la base de datos a consultar")
    values = await conn.fetch(Censo(provincia,departamento).xCodigo())
    await conn.close()
    return values
