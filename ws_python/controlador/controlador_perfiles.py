from typing import Union
import asyncpg
import os
from modelo.perfiles import PerfilesSeccionesElecciones, PerfilesCircuitosElecciones, PerfilesSeccionesCategorias, PerfilesCircuitosCategorias \
                    , PerfilesSeccionesDistritos, PerfilesCircuitosDistritos, PerfilesSeccionesSecciones, PerfilesCircuitosSecciones \
                    , PerfilesSeccionesPerfiles, PerfilesCircuitosPerfiles, PerfilesCircuitosCircuitos, PerfilesSecciones, PerfilesCircuitos \
                    , PerfilesSeccionesAlgoritmos, PerfilesCircuitosAlgoritmos, ClasificacionPais, ClasificacionDistrito, PerfilPais, PerfilDistrito \
                    , ClasificacionPerfilPais, ClasificacionPerfilDistrito

async def bd():
    conn = await asyncpg.connect(dsn=os.environ.get("DB_DSN"),database="consultas")
    return conn

######################### Perfiles x Seccion #########################

async def seccionesSeccionesPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesSeccionesSecciones(eleccion,categoria,distrito,seccion,algoritmo,perfil).xCodSeccion())
    await conn.close()
    return values

async def distritosSeccionesPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesSeccionesDistritos(eleccion,categoria,distrito,algoritmo,perfil).xCodDistrito())
    await conn.close()
    return values

async def algoritmosSeccionesPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesSeccionesAlgoritmos(eleccion,categoria,distrito,seccion,algoritmo,perfil).xCodAlgoritmo())
    await conn.close()
    return values

async def eleccionesSeccionesPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesSeccionesElecciones(eleccion,categoria,distrito,seccion,algoritmo,perfil).xCodEleccion())
    await conn.close()
    return values

async def categoriasSeccionesPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesSeccionesCategorias(eleccion,categoria,distrito,seccion,algoritmo,perfil).xCodCategoria())
    await conn.close()
    return values

async def perfilesSeccionesPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    if sum([var is not None for var in [eleccion, categoria, distrito, seccion]]) < 2:
        raise Exception("Se requieren al menos dos variables")
    conn = await bd()
    values = await conn.fetch(PerfilesSeccionesPerfiles(eleccion,categoria,distrito,seccion,algoritmo,perfil).xPerfil())
    await conn.close()
    return values

async def perfilesSecciones(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    if sum([var is not None for var in [eleccion, categoria, distrito, seccion]]) < 2:
        raise Exception("Se requieren al menos dos variables")
    conn = await bd()
    values = await conn.fetch(PerfilesSecciones(eleccion,categoria,distrito,seccion,algoritmo,perfil).xCodCategoriaCodDistritoCodSeccionPerfil())
    await conn.close()
    return values

######################### Perfiles x Circuito #########################

async def circuitosCircuitosPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     circuito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesCircuitosCircuitos(eleccion,categoria,distrito,seccion,circuito,algoritmo,perfil).xCodCircuito())
    await conn.close()
    return values

async def seccionesCircuitosPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     circuito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesCircuitosSecciones(eleccion,categoria,distrito,seccion,circuito,algoritmo,perfil).xCodSeccion())
    await conn.close()
    return values

async def distritosCircuitosPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesCircuitosDistritos(eleccion,categoria,distrito,algoritmo,perfil).xCodDistrito())
    await conn.close()
    return values

async def algoritmosCircuitosPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     circuito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesCircuitosAlgoritmos(eleccion,categoria,distrito,seccion,circuito,algoritmo,perfil).xCodAlgoritmo())
    await conn.close()
    return values

async def eleccionesCircuitosPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     circuito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesCircuitosElecciones(eleccion,categoria,distrito,seccion,circuito,algoritmo,perfil).xCodEleccion())
    await conn.close()
    return values

async def categoriasCircuitosPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     circuito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    conn = await bd()
    values = await conn.fetch(PerfilesCircuitosCategorias(eleccion,categoria,distrito,seccion,circuito,algoritmo,perfil).xCodCategoria())
    await conn.close()
    return values

async def perfilesCircuitosPerfiles(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     circuito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    if sum([var is not None for var in [eleccion, categoria, distrito, seccion]]) < 2:
        raise Exception("Se requieren al menos dos variables")
    conn = await bd()
    values = await conn.fetch(PerfilesCircuitosPerfiles(eleccion,categoria,distrito,seccion,circuito,algoritmo,perfil).xPerfil())
    await conn.close()
    return values

async def perfilesCircuitos(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     seccion  : Union[str, None]=None, 
                     circuito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     perfil   : Union[str, None]=None):
    if sum([var is not None for var in [eleccion, categoria, distrito, seccion]]) < 2:
        raise Exception("Se requieren al menos dos variables")
    conn = await bd()
    values = await conn.fetch(PerfilesCircuitos(eleccion,categoria,distrito,seccion,circuito,algoritmo,perfil).xCodCategoriaCodDistritoCodSeccionCodCircuitoPerfil())
    await conn.close()
    return values

######################### Info Clasificación y Perfil #########################

async def clasificacionPais(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     elementos: Union[str, None]=None):
    if sum([var is not None for var in [eleccion, categoria, algoritmo, elementos]]) < 2:
        raise Exception("Se requieren al menos dos variables")
    conn = await bd()
    values = await conn.fetch(ClasificacionPais(eleccion,categoria,algoritmo,elementos).xEleccionCategoriaAlgoritmoElementos())
    await conn.close()
    return values

async def clasificacionDistrito(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     elementos: Union[str, None]=None):
    if sum([var is not None for var in [eleccion,categoria,distrito,algoritmo,elementos]]) < 2:
        raise Exception("Se requieren al menos dos variables")
    conn = await bd()
    values = await conn.fetch(ClasificacionDistrito(eleccion,categoria,distrito,algoritmo,elementos).xEleccionCategoriaDistritoAlgoritmoElementos())
    await conn.close()
    return values

async def perfilPais(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     elementos: Union[str, None]=None,
                     perfil   : Union[str, None]=None):
    if sum([var is not None for var in [eleccion, categoria, algoritmo, elementos, perfil]]) < 2:
        raise Exception("Se requieren al menos dos variables")
    conn = await bd()
    values = await conn.fetch(PerfilPais(eleccion,categoria,algoritmo,elementos,perfil).xEleccionCategoriaAlgoritmoElementos())
    await conn.close()
    return values

async def perfilDistrito(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     elementos: Union[str, None]=None,
                     perfil   : Union[str, None]=None):
    if sum([var is not None for var in [eleccion,categoria,distrito,algoritmo,elementos, perfil]]) < 2:
        raise Exception("Se requieren al menos dos variables")
    conn = await bd()
    values = await conn.fetch(PerfilDistrito(eleccion,categoria,distrito,algoritmo,elementos,perfil).xEleccionCategoriaDistritoAlgoritmoElementos())
    await conn.close()
    return values

async def clasificacionPerfilPais(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     elementos: Union[str, None]=None):
    if sum([var is not None for var in [eleccion, categoria, algoritmo, elementos]]) < 2:
        raise Exception("Se requieren al menos dos variables")
    conn = await bd()
    values = await conn.fetch(ClasificacionPerfilPais(eleccion,categoria,algoritmo,elementos).xEleccionCategoriaAlgoritmoElementos())
    await conn.close()
    return values

async def clasificacionPerfilDistrito(eleccion: Union[str, None]=None, 
                     categoria: Union[str, None]=None, 
                     distrito : Union[str, None]=None, 
                     algoritmo: Union[str, None]=None, 
                     elementos: Union[str, None]=None):
    if sum([var is not None for var in [eleccion,categoria,distrito,algoritmo,elementos]]) < 2:
        raise Exception("Se requieren al menos dos variables")
    conn = await bd()
    values = await conn.fetch(ClasificacionPerfilDistrito(eleccion,categoria,distrito,algoritmo,elementos).xEleccionCategoriaDistritoAlgoritmoElementos())
    await conn.close()
    return values
