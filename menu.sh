#!/bin/bash
# Bash Menu Script
while true
do
    echo "------------------------------------------------------------"
    echo "Ingresar opción: "
    echo "1) Levantar  contenedor: docker-compose up"
    echo "2) Detener   contenedor: docker-compose stop"
    echo "3) Reiniciar contenedor: docker-compose up --build"
    echo "4) Eliminar  contenedor: docker-compose down (OJO !, se eliminarán las bases de datos)"
    echo "5) Importar bases de datos desde dump (es necesario que el contenedor esté levantado)"
    echo "6) Borrar bases de datos que estén en dump (es necesario que el contenedor esté levantado)"
    echo "7) Consultas SQL Elecciones"
    echo "8) Consultas SQL Establecimientos"
    echo "9) Consultas SQL Censo Provisorio"
    echo "10) Consultas SQL Perfiles"
    echo "11) Generar perfiles (con el contenedor levantado, aprox 30 min)"
    echo "0) Salir"
    echo "------------------------------------------------------------"
    read opt
    case $opt in
        1) echo "- "$(date)" - Levantando contenedores (Ctrl+C para detener)" # Sin mostrar salida: docker-compose up -d 
           docker-compose up
           ;;
        2) # Detiene el contenedor que está corriendo sin borrarlo
           docker-compose stop
           ;;
        3) # Reconstruye imágenes antes de levantar contendores
           docker-compose up --build
           ;;
        4) # Borra el contenedor y su información asociada (redes, imágenes y volúmenes)
           docker-compose down
           ;;
        5) # Por cada sql del directorio dump crea una base de datos con el mismo nombre e importa los datos
           echo "- "$(date)" - Importando bases de datos desde dump/*.sql"
           echo "Tiempo aproximado 15 min, para las 6 bases de datos electorales"
           for i in $(ls dump/*.sql); do
             nombre_bd=$(basename -s .sql $i)
             echo "- "$(date)" - Importando" $nombre_bd "..."
             echo "CREATE DATABASE $nombre_bd;" | docker exec -i contenedor_pg psql -U postgres
             cat dump/$nombre_bd.sql | docker exec -i contenedor_pg psql -U postgres -d $nombre_bd
           done
           echo "- "$(date)" - Fin importación"
           ;;
        6) # Por cada sql del directorio dump dropea base de datos con el mismo nombre
           echo "- "$(date)" - Borrando bases de datos desde dump/*.sql"
           for i in $(ls dump/*.sql); do
             nombre_bd=$(basename -s .sql $i)
             echo "- "$(date)" - Dropeando" $nombre_bd "..."
             echo "DROP DATABASE $nombre_bd;" | docker exec -i contenedor_pg psql -U postgres
           done
           echo "- "$(date)" - Fin importación"
           ;;
        7) python3 ws_python/modelo/consultas_elecciones.py ;;  
        8) python3 ws_python/modelo/consultas_establecimientos.py ;;
        9) python3 ws_python/modelo/consultas_censop.py ;;
       10) python3 ws_python/modelo/consultas_perfiles.py ;;
       11) date
           docker exec -it web python -u generar/generar_perfiles.py | tee informe_generacion_perfiles.md
           date
           ;;
        0) break ;;
        *) echo "opción inválida $opt";;
    esac
done
